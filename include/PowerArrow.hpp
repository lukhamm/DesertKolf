/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef DESERTKOLF_POWERARROW_HPP
#define DESERTKOLF_POWERARROW_HPP

#include <lupriv/Matrix.hpp>
#include <lugame/utils/EventHandler.hpp>

#include <allegro5/bitmap.h>
#include <allegro5/allegro.h>

#include <allegro5/allegro_font.h>      // test
#include <allegro5/allegro_ttf.h>       // test

#include "FileManager.hpp"

namespace lugame {
  class Event;
}

class PowerArrow : public lugame::EventHandler {
public:
  
  PowerArrow(
  );
  virtual ~PowerArrow(
  );
  
  void 
  loadContent(
  );
  
  void 
  update(
    ALLEGRO_EVENT ev
  );
  
  void 
  draw(
  );
  
  void 
  handleEvent(
    const lugame::Event* event
  ) override;
  
private:
  lulib::math::Matrix<float> m_startPos;
  lulib::math::Matrix<float> m_currentPos;
  lulib::math::Matrix<float> m_endPos;
  lulib::math::Matrix<float> m_t1, m_t2, m_t3, m_span, m_powerArrowStart;

  const float m_arrowThickness{3};
  const float m_lengthSprite{200};
  const int m_powerArrowDelay{2};
  const int m_maxColumn{16};
  
  int m_currentDelayCounter{0};
  int m_spriteCol{0};
  float m_angle{0};
  float m_power{0};
  lulib::math::Matrix<float> m_dir;
  
  bool m_show{false};
  bool m_mouseActive{false};
  
  ALLEGRO_BITMAP* m_powerDotsBmp;
  ALLEGRO_MOUSE_STATE m_state;
  
  ALLEGRO_FONT* fonti; // test
  
  // Ghost Power Arrow
  bool m_showGhostArrow{false};
  lulib::math::Matrix<float> m_startPosGhost;
  lulib::math::Matrix<float> m_t1Ghost, m_t2Ghost, m_t3Ghost, m_spanGhost, m_powerArrowStartGhost;
  float m_angleGhost{0};
  
  FileManager m_fileManager;
  
};



#endif