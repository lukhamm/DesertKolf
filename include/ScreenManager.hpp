/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef DESERTKOLF_SCREENMANAGER_HPP
#define DESERTKOLF_SCREENMANAGER_HPP

#include <allegro5/allegro.h>

#include <iostream>
#include <unordered_map>
#include <string>

#include <lugame/gui/Gui.hpp>

#include "GameScreen.hpp"


class ScreenManager {
public:
  
  ScreenManager(
    ScreenManager const&
  ) = delete;
  
  ~ScreenManager(
  );
  
  void 
  operator=(
    ScreenManager const&
  ) = delete;
  
  static ScreenManager&
  getInstance(
  );
  
  bool 
  initialize(
    const int screenWidth,
    const int screenHeight
  );
  
  void 
  loadContent(
  );
  
  void 
  unloadContent(
  );
  
  void 
  update(
    ALLEGRO_EVENT ev
  );
  
  void 
  draw(
    ALLEGRO_DISPLAY* display
  );
  
  void 
  changeScreen(
    const std::string& screen_name              // TODO make this an integer
  );
  
  bool done;
  
  void*
  getData(
  );
  
  void
  passData(
    void* data
  );
  
  void
  demandGameExit(
  );
  
private:
  ScreenManager(
  );
  
  GameScreen* 
  createScreen(
    const std::string& screenName
  );
  
  void 
  fadeAnimationUpdate(
  );
  
  GameScreen *currentScreen, *newScreen;
  lugame::gui::Gui gui;
  
  int m_screenWidth;
  int m_screenHeight;
  
  void* m_dataPtr = nullptr;
  
  std::string m_requestChangeScreen{""};
  bool m_fadingActive{false};
  bool m_fadingDone{false};
  bool m_exitGame{false};
};

#endif