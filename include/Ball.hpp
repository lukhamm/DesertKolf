/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef DESERTKOLF_BALL_HPP
#define DESERTKOLF_BALL_HPP

#include "GameObject.hpp"
#include "Constants.hpp"
#include "SoundManager.hpp"

#include <lupriv/Matrix.hpp>

class Ball : public GameObject {
public:
  
  enum class STATE { 
    resting,
    moving,
    holed
  };
  
  Ball(
    const float screenWidth, 
    const float screenHeight,
    const float posX,
    const float posY
  );
  virtual ~Ball(
  );
  
  virtual void 
  loadContent(
  ) override;
  
  virtual void 
  update(
    ALLEGRO_EVENT ev,
    void* data = nullptr
  ) override;
  
  virtual void 
  draw(
  ) override;
  
  virtual void 
  handleEvent(
    const lugame::Event* event
  ) override;
  
  bool 
  isHoled();
  
  void
  reset(
    const float startPosX,
    const float startPosY
  );
  
private:
  SoundManager m_soundManager;
  
  lulib::math::Matrix<float> m_levelStartPos{0,0};
  lulib::math::Matrix<float> m_startPos{0,0};
  lulib::math::Matrix<float> m_v{0,0};
  lulib::math::Matrix<float> m_pos;
  lulib::math::Matrix<float> m_offset{0,0};
  
  void changeState(
    const STATE newState
  );
  
  bool inAir{false};
  bool m_isHit{false};
  
  int m_frictionCounter{0};
  
  const float m_radius{3.f};
  STATE m_state{STATE::moving};
};

#endif