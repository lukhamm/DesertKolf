/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef DESERT_KOLF_LEVELMANAGER_HPP
#define DESERT_KOLF_LEVELMANAGER_HPP

#include <string>
#include <vector>

#include <lupriv/Matrix.hpp>

#include<allegro5/allegro_font.h>
#include<allegro5/allegro_ttf.h>

#include "Level.hpp"
#include "FileManager.hpp"

class LevelManager {
public:
  LevelManager(
    const int screenWidth,
    const int screenHeight
  );
  ~LevelManager(
  );
  
  void
  createLevel(
  );
  
  bool 
  setLevel(
    const std::size_t levelNr,
    const int hits
  );
  
  void
  drawLevel(
  );
  
  bool
  checkCollisions(
    const lulib::math::Matrix<float>& pos,
    const float radius,
    lulib::math::Matrix<float>& offset
  );
  
  bool
  isBallHoled(
    const lulib::math::Matrix<float>& pos,
    const float radius
  );
  
  lulib::math::Matrix<float>
  getStartingPosition(
  );
  
  std::size_t
  nextLevel(
  );
  
  bool
  setLevel(
    const std::size_t levelNr
  );
  
  void addHit(
  );
  
  std::size_t
  nrOfLevels(
  ) const;
  
  int
  getHits(
  ) const;
  
  int
  getTotalHits(
  ) const;
  
  std::size_t
  getCurrentLevel(
  ) const;
  
private:
  FileManager m_fileManager;
  
  int m_screenWidth, m_screenHeight;
  
  int m_hitsCurrentLevel{0};
  int m_hitsOverall{0};
  std::size_t m_currentLevel{0};
  int m_nrLevels{0};
  std::vector<std::string> m_levelNames;
  std::string m_rootFolderName;
  Level* m_levelPtr{nullptr};
  ALLEGRO_FONT* m_levelFont;
};


#endif