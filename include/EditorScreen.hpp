/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef DESERTKOLF_EDITORSCREEN_HPP
#define DESERTKOLF_EDITORSCREEN_HPP

#include <lugame/gui/Box.hpp>
#include <lugame/gui/Button.hpp>
#include <lugame/gui/styles/Primitive.hpp>

#include <lugame/utils/RandomNumbers.hpp>

#include "GameScreen.hpp"
#include "LevelEditor.hpp"
#include "FileManager.hpp"
#include "Level.hpp"

class EditorScreen : public GameScreen {
public:
  
  EditorScreen(
    const int screenWidth, 
    const int screenHeight
  ) ;
  
  virtual ~EditorScreen(
  );

  void 
  loadContent(
  ) override;
  
  void 
  unloadContent(
  ) override;
  
  void 
  draw(
    ALLEGRO_DISPLAY* display
  ) override;
  
  void 
  update(
    ALLEGRO_EVENT ev
  ) override;
  
  void
  handleEvent(
    const lugame::Event* event
  ) override;
  
private:
  
  void
  createRandomLevel(
  );
  
  void
  saveLevel(
  );
  
  LevelEditor m_levelEditor;
  
  lugame::gui::Box box{"box1",50.f,20.f,lugame::gui::Box::ORIENTATION::HORIZONTAL,20};
  
  lugame::gui::style::Primitive stylePrimitive;
  lugame::gui::style::Style* stylePtr{&stylePrimitive};

  lugame::gui::Button m_randomButton{"randomLevelButton","Random Level",100,30, stylePtr};
  lugame::gui::Button m_saveButton{"saveLevelButton","Save Level",100,30, stylePtr};
  lugame::gui::Button m_returnButton{"returnButton","Return to Menu",100,30, stylePtr};
  
  FileManager m_fileManager;
  
  Level* m_levelPtr{nullptr};
};


#endif