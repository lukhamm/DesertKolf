/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef DESERTKOLF_LEVELEDITOR_HPP
#define DESERTKOLF_LEVELEDITOR_HPP

#include <iostream>
#include <vector>

#include <lugame/utils/RandomNumbers.hpp>

class LevelEditor {
public:
  
  LevelEditor(
    const int screenWidth, 
    const int screenHeight
  );
  
  void 
  createRandomLevel(
  );
  
  void writeLevelToFile(
    const int levelNr,
    const std::string& filename
  );
  
  void
  updateLevelOverview(
    const int totalNrOfLevels,
    const std::string& filename
  );

  void clearLevel(
  );
  
private:
  
  struct Point {
    int x,y;
    void 
    print(
    ) const 
    {
      std::cout << x << "  " << y << std::endl;
    };
  };
  
  lugame::RandomNumbers<int> m_rd;
  const int m_screenWdith, m_screenHeight;
  std::vector<Point> m_points;
  Point m_driveStart, m_holeStart;
};

#endif