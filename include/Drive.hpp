/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef DESERT_KOLF_DRIVE_HPP
#define DESERT_KOLF_DRIVE_HPP

#include "Hole.hpp"

class Drive {
public:
  Drive(
    const float posX, 
    const float posY,
    std::vector<LineType>& lines
  );
  
  void 
  draw();
  
  lulib::math::Matrix<float>
  getStartingPosition(
  );
  
private:
  float m_p1[2];
  float m_p2[2];
};


#endif