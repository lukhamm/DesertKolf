/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef DESERT_KOLF_HOLE_HPP
#define DESERT_KOLF_HOLE_HPP

#include <vector>

#include <lupriv/Matrix.hpp>

#include "Flag.hpp"

struct LineType {
  float x1{0},y1{0};
  float x2{0},y2{0};
};

struct PointType {
  float x{0},y{0};
};


class Hole {
public:
  
  Hole(
    const float offsetX,
    const float offsetY,
    std::vector<LineType>& lines,
    const int levelNr
  );
  
  void
  draw(
  ) const;
  
  void
  setOffset(
    const float offsetX,
    const float offsetY
  );
  
  bool
  isBallHoled(
    const lulib::math::Matrix<float>& pos,
    const float radius
  );
  
private:
  const float m_p1[2];
  const float m_p2[2];
  const float m_p3[2];
  const float m_p4[2];
  const float m_p5[2];
  
  float m_offset[2];
  
  Flag m_flag;
};


#endif