/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef DESERT_KOLF_SOUNDMANAGER_HPP
#define DESERT_KOLF_SOUNDMANAGER_HPP

#include <allegro5/allegro5.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>

#include <string>
#include <vector>
#include <map>

enum class SOUNDS {
    HIT
  };

class SoundManager {
public:
  
  void
  loadContent(
  );
  
  void
  unloadContent();
  
  void
  play(
    const SOUNDS soundToPlay
  );

  
private:
  std::vector<ALLEGRO_SAMPLE_INSTANCE*> m_instances;
  std::map<SOUNDS, ALLEGRO_SAMPLE*> m_sample_map;
};

#endif