/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef DESERT_KOLF_FINISH_SCREEN_HPP
#define DESERT_KOLF_FINISH_SCREEN_HPP

#include <lugame/gui/Box.hpp>
#include <lugame/gui/Button.hpp>
#include <lugame/gui/styles/Primitive.hpp>
#include <lugame/utils/InputManager.hpp>

#include<allegro5/allegro_font.h>
#include<allegro5/allegro_ttf.h>

#include "GameScreen.hpp"
#include "FileManager.hpp"
#include "KeyboardInput.hpp"

#include <vector>

class FinishScreen : public GameScreen {
public:
  
  struct HighscoreType {
    bool operator<(
      const HighscoreType& other
    ) const
    {
      return score < other.score;
    }
    int score;
    std::string name;
  };
  
  FinishScreen(
    const int screenWidth, 
    const int screenHeight
  );
  
  virtual ~FinishScreen(){};

  void 
  loadContent(
  ) override;
  
  void 
  unloadContent(
  ) override;
  
  void 
  draw(
    ALLEGRO_DISPLAY* display
  ) override;
  
  void 
  update(
    ALLEGRO_EVENT ev
  ) override;
  
  void
  handleEvent(
    const lugame::Event* event
  ) override;
  
  
private:
  
  bool loadHighScores(
  );
  
  bool saveHighScore(
    const std::string& name
  );
  
  bool eraseSaveGame(
  );
  
  int m_hitsOverall{0};
  int* m_hitsPtr = nullptr;
  
  lugame::gui::Box box{"box1",m_screenWidth/2.f-100.f,400.f,lugame::gui::Box::ORIENTATION::VERTICAL,20};
 
  lugame::gui::style::Primitive stylePrimitive;
  lugame::gui::style::Style* stylePtr{&stylePrimitive};

  lugame::gui::Button backButton{"backButton","Back to Menu",200,70, stylePtr};
  lugame::gui::Button submitButton{"submitButton","Submit",200,70, stylePtr};

  ALLEGRO_FONT* m_hitsFont;
  ALLEGRO_FONT* m_highscoreFont;
  
  FileManager m_fileManager;
  std::vector<HighscoreType> m_highscores;
  
  KeyboardInput m_keyboardInput;
  bool m_readHighscoreName{false};
  std::string m_name{""};
  bool m_nameEntered{false};
  lugame::InputManager m_inputManager;
};

#endif