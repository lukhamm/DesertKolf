/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef DESERT_KOLF_LEVEL_HPP
#define DESERT_KOLF_LEVEL_HPP

#include <string>
#include <vector>
#include <memory>

#include <lupriv/Matrix.hpp>

#include "Hole.hpp"
#include "Drive.hpp"

class Level {
public:
  
  Level(
    const std::string& filename,
    const int levelNr,
    const int levelWidth,
    const int levelHeight
  );
  
  ~Level(
  );
  
  void draw();
  
  bool
  checkCollisions(
    const lulib::math::Matrix<float>& pos,
    const float radius,
    lulib::math::Matrix<float>& offset
  );
  
  bool
  isBallHoled(
    const lulib::math::Matrix<float>& pos,
    const float radius
  );
  
  lulib::math::Matrix<float>
  getStartingPosition(
  );
  
private:
  
  struct triangleType {
    float x1{0}, y1{0};
    float x2{0}, y2{0};
    float x3{0}, y3{0};
  };
  
  std::vector<LineType> m_lines;
  std::vector<PointType> m_points;
  
  std::size_t m_verticesArrayEntries;
  float* m_verticesPtr{nullptr};
  
  int m_pointNr{0};
  int m_levelNr{0};
  
  int m_levelWidth;
  int m_levelHeight;
  
  Hole* m_holePtr{nullptr};
  Drive* m_drivePtr{nullptr};
};


#endif