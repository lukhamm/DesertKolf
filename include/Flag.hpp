/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef DESERT_KOLF_FLAG_HPP
#define DESERT_KOLF_FLAG_HPP

#include <lupriv/Matrix.hpp>

#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>

#include "FileManager.hpp"

class Flag {
public:
  
  Flag(
    const float x,
    const float y,
    const int holeNr
  );
  
  void draw(
  ) const;
  
private:
  
  lulib::math::Matrix<float> m_pos{0,0};
  int m_holeNr{0};
  
  ALLEGRO_FONT* m_font;
  
  FileManager m_fileManager;
};

#endif