/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef DESERTKOLF_GAMEOBJECT_HPP
#define DESERTKOLF_GAMEOBJECT_HPP

#include <lugame/utils/EventHandler.hpp>

#include <allegro5/allegro.h>

namespace lugame {
  class Event;
}

class GameObject : public lugame::EventHandler {
public:
  
  GameObject(
  ){};
  virtual ~GameObject(
  ){};
  
  virtual void 
  loadContent(
  ){};
  
  virtual void 
  update(
    ALLEGRO_EVENT /*ev*/,
    void* /*data*/
  ){};
  
  virtual void 
  draw(
  ){};
  
  virtual void 
  handleEvent(
    const lugame::Event* /*event*/
  ){};
  
protected:
  float m_posX, m_posY;
  float m_mass;
  
private:
  
};


#endif