/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef DESERTKOLF_TITLESCREEN_HPP
#define DESERTKOLF_TITLESCREEN_HPP

#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_font.h>

#include <lugame/gui/Box.hpp>
#include <lugame/gui/Button.hpp>
#include <lugame/gui/styles/Primitive.hpp>

#include "GameScreen.hpp"
#include "FileManager.hpp"

namespace lugame {
  class Event;
}


class TitleScreen : public GameScreen {
public:

  TitleScreen(
    const int screenWidth, 
    const int screenHeight
  );
  
  virtual ~TitleScreen();

  void 
  loadContent(
  ) override;
  
  void 
  unloadContent(
  ) override;
  
  void 
  draw(
    ALLEGRO_DISPLAY* display
  ) override;
  
  void 
  update(
    ALLEGRO_EVENT ev
  ) override;
  
  void
  handleEvent(
    const lugame::Event* event
  ) override;
  
private:
  lugame::gui::Box box{"box1",m_screenWidth/2.f-100.f,150.f,lugame::gui::Box::ORIENTATION::VERTICAL,15};
  
  lugame::gui::style::Primitive stylePrimitive;
  lugame::gui::style::Style* stylePtr{&stylePrimitive};

  lugame::gui::Button resumeButton{"resumeButton","Resume Game",200, 60, stylePtr};
  lugame::gui::Button editorButton{"editorButton","Editor",200, 60, stylePtr};
  lugame::gui::Button startButton{"startButton","New Game",200, 60, stylePtr};
  lugame::gui::Button quitButton{"quitButton","Quit",200, 60, stylePtr};
  
  ALLEGRO_FONT* m_titleFont;
  
  bool m_resumeGame{false};
  FileManager m_fileManager;
  std::pair<std::size_t, int>* m_resumeParametersPtr{nullptr};
};


#endif