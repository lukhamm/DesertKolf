/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef DESERT_KOLF_KEYBOARD_INPUT_HPP
#define DESERT_KOLF_KEYBOARD_INPUT_HPP

#include <allegro5/allegro.h>

#include <string>
#include <iostream>

class KeyboardInput {
public:
  void startRecording(
    const std::size_t stringLength
  )
  {
    m_stringLength = stringLength;
    m_isRecording = true;
    m_recString = "";
  }
  
  void stopRecording(
  )
  {
    m_isRecording = false;
  }
  
  const std::string& getString(
  )
  {
    return m_recString;
  }
  
  void update(
    ALLEGRO_EVENT ev
  )
  {
    if (m_isRecording)
    { 
      if (ev.type == ALLEGRO_EVENT_KEY_DOWN and m_recString.size() < m_stringLength)
      {
        switch (ev.keyboard.keycode)
        {
          case ALLEGRO_KEY_Q:
            m_recString += "Q";
            break;
          case ALLEGRO_KEY_W:
            m_recString += "W";
            break;
          case ALLEGRO_KEY_E:
            m_recString += "E";
            break;
          case ALLEGRO_KEY_R:
            m_recString += "R";
            break;
          case ALLEGRO_KEY_T:
            m_recString += "T";
            break;
          case ALLEGRO_KEY_Z:
            m_recString += "Z";
            break;
          case ALLEGRO_KEY_U:
            m_recString += "U";
            break;
          case ALLEGRO_KEY_I:
            m_recString += "I";
            break;
          case ALLEGRO_KEY_O:
            m_recString += "O";
            break;
          case ALLEGRO_KEY_P:
            m_recString += "P";
            break;
          case ALLEGRO_KEY_A:
            m_recString += "A";
            break;
          case ALLEGRO_KEY_S:
            m_recString += "S";
            break;
          case ALLEGRO_KEY_D:
            m_recString += "D";
            break;
          case ALLEGRO_KEY_F:
            m_recString += "F";
            break;
          case ALLEGRO_KEY_G:
            m_recString += "G";
            break;
          case ALLEGRO_KEY_H:
            m_recString += "H";
            break;
          case ALLEGRO_KEY_J:
            m_recString += "J";
            break;
          case ALLEGRO_KEY_K:
            m_recString += "K";
            break;
          case ALLEGRO_KEY_L:
            m_recString += "L";
            break;
          case ALLEGRO_KEY_Y:
            m_recString += "Y";
            break;
          case ALLEGRO_KEY_X:
            m_recString += "X";
            break;
          case ALLEGRO_KEY_C:
            m_recString += "C";
            break;
          case ALLEGRO_KEY_V:
            m_recString += "V";
            break;
          case ALLEGRO_KEY_B:
            m_recString += "B";
            break;
          case ALLEGRO_KEY_N:
            m_recString += "N";
            break;
          case ALLEGRO_KEY_M:
            m_recString += "M";
            break;
          case ALLEGRO_KEY_1:
            m_recString += "1";
            break;
          case ALLEGRO_KEY_2:
            m_recString += "2";
            break;
          case ALLEGRO_KEY_3:
            m_recString += "3";
            break;
          case ALLEGRO_KEY_4:
            m_recString += "4";
            break;
          case ALLEGRO_KEY_5:
            m_recString += "5";
            break;
          case ALLEGRO_KEY_6:
            m_recString += "6";
            break;
          case ALLEGRO_KEY_7:
            m_recString += "7";
            break;
          case ALLEGRO_KEY_8:
            m_recString += "8";
            break;
          case ALLEGRO_KEY_9:
            m_recString += "9";
            break;
          case ALLEGRO_KEY_0:
            m_recString += "0";
            break;
          case ALLEGRO_KEY_BACKSPACE:
            m_recString = m_recString.substr(0,m_recString.size()-1);
            break;
        }
      }
      else if (ev.type == ALLEGRO_EVENT_KEY_DOWN)
      {
        if (ev.keyboard.keycode == ALLEGRO_KEY_BACKSPACE)
        {
          m_recString = m_recString.substr(0,m_recString.size()-1);
        }
      }
    }
  }
  
private:
  bool m_isRecording{false};
  std::string m_recString;
  std::size_t m_stringLength{0};
};


#endif