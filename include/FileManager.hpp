/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef DESERTKOLF_FILEMANAGER_HPP
#define DESERTKOLF_FILEMANAGER_HPP

#include <allegro5/allegro.h>
#include <string>
#include <map>

class FileManager {
public:

  enum class 
  FOLDER { 
    BUILD,
    RESOURCES, 
    SPRITES, 
    SOUNDS, 
    LEVELS, 
    STYLE_PRIMITIVE,
    SAVEGAMES
  };
  
  FileManager(
  );
  
  ~FileManager(
  );
  
  bool
  checkFolderExistance(
  );
  
  void 
  printBuildPath(
  );
  
//   bool
//   registerNewPath(
//     const std::string& pathName
//   );
  
  ALLEGRO_PATH*
  getPath(
    const FOLDER pathType
  );
  
  std::string
  getPathString(
    const FOLDER pathType
  );
  
private:
  std::map<FOLDER, ALLEGRO_PATH*> m_paths;
  std::string m_resources{"resources"};
};

#endif