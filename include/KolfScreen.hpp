/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef DESERTKOLF_KOLFSCREEN_HPP
#define DESERTKOLF_KOLFSCREEN_HPP

#include <lugame/gui/Box.hpp>
#include <lugame/gui/Button.hpp>
#include <lugame/gui/styles/Primitive.hpp>

#include "GameScreen.hpp"
#include "PowerArrow.hpp"
#include "Ball.hpp"
#include "LevelManager.hpp"
#include "FileManager.hpp"

class KolfScreen : public GameScreen {
public:
  
  KolfScreen(
    const int screenWidth, 
    const int screenHeight
  ) ;
  
  virtual ~KolfScreen(
  );

  void 
  loadContent(
  ) override;
  
  void 
  unloadContent(
  ) override;
  
  void 
  draw(
    ALLEGRO_DISPLAY* display
  ) override;
  
  void 
  update(
    ALLEGRO_EVENT ev
  ) override;
  
  void
  handleEvent(
    const lugame::Event* event
  ) override;
  
  void
  nextLevel(
  );
  
  void
  actionOnExit(
  ) override;
  
private:
  PowerArrow m_powerArrow;
  LevelManager m_levels{m_screenWidth, m_screenHeight};
  FileManager m_fileManager;
  
  lugame::gui::Box m_box{"box1",20.f,20.f,lugame::gui::Box::ORIENTATION::HORIZONTAL,20};
  
  lugame::gui::style::Primitive stylePrimitive;
  lugame::gui::style::Style* stylePtr{&stylePrimitive};

  lugame::gui::Button m_nextLevelButton{"nextLevelButton","Next Level",100,30, stylePtr};
  lugame::gui::Button m_returnButton{"returnButton","Return to Menu",100,30, stylePtr};
  lugame::gui::Button m_resetButton{"resetButton","Reset Ball",100,30, stylePtr};
  
  bool m_debugMode{false};
  bool m_mouseActive{false};
  Ball* m_ballPtr;
};


#endif