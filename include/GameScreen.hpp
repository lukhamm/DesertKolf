/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef DESERTKOLF_GAMESCREEN_HPP
#define DESERTKOLF_GAMESCREEN_HPP

#include <allegro5/allegro.h>

#include <iostream>

#include <lugame/utils/EventHandler.hpp>

namespace lugame {
  class Event;
}


class GameScreen : public lugame::EventHandler {
public:
  
  GameScreen(
    const int screenWidth, 
    const int screenHeight
  ) : m_screenWidth(screenWidth)
    , m_screenHeight(screenHeight)
  {
  }
  
  virtual ~GameScreen(){};
  
  virtual void 
  loadContent(
  ){};

  virtual void 
  unloadContent(
  ){};
  
  virtual void 
  draw(
    ALLEGRO_DISPLAY* /*display*/
  ){};
  
  virtual void 
  update(
    ALLEGRO_EVENT /*ev*/
  ){};
  
  virtual void
  handleEvent(
    const lugame::Event* /*event*/
  ){};
  
  virtual void
  actionOnExit(
  ){};

protected:
  const int m_screenWidth;
  const int m_screenHeight;
  
private:

};

#endif