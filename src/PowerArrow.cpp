/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#include "../include/PowerArrow.hpp"

#include <cmath>

#include <lugame/utils/EventManager.hpp>
#include <lugame/utils/EventCodes.hpp>
#include <lugame/engines2d/Collision.hpp>

#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>

constexpr uint32_t ballShotId = lugame::SDBMCalculator<9>::CalculateValue("ball_shot");
constexpr uint32_t levelChangedId = lugame::SDBMCalculator<9>::CalculateValue("level_changed");

PowerArrow::PowerArrow(
)
: m_startPos(2)
, m_currentPos(2)
, m_endPos(2)
, m_t1(2)
, m_t2(2)
, m_t3(2)
, m_span(2)
, m_powerArrowStart(2)
{
  al_get_mouse_state(&m_state);
  m_currentPos[0] = m_state.x;
  m_currentPos[1] = m_state.y;
  lugame::EventManager::getInstance().registerEvent(ballShotId);
  std::string fontPath = m_fileManager.getPathString(FileManager::FOLDER::RESOURCES)
                       + "Style_Primitive/fonts/bitstream_vera_sans_font_6016/Vera.ttf";
  fonti = al_load_ttf_font(fontPath.c_str(), 20, 0);
  
  m_name = "PowerArrow";
}

PowerArrow::~PowerArrow(
)
{
  lugame::EventManager::getInstance().detachEvent(lugame::eventid::mouseClickLeft, 
                                                  *static_cast<EventHandler*>(this));
  lugame::EventManager::getInstance().detachEvent(lugame::eventid::mouseReleaseLeft, 
                                                  *static_cast<EventHandler*>(this));
  lugame::EventManager::getInstance().detachEvent(lugame::eventid::mouseMove, 
                                                  *static_cast<EventHandler*>(this));
  lugame::EventManager::getInstance().detachEvent(levelChangedId, 
                                                  *static_cast<EventHandler*>(this));
  al_destroy_font(fonti);
}

void PowerArrow::loadContent()
{
  lugame::EventManager::getInstance().attachEvent(lugame::eventid::mouseClickLeft, 
                                                  *static_cast<EventHandler*>(this));
  lugame::EventManager::getInstance().attachEvent(lugame::eventid::mouseReleaseLeft, 
                                                  *static_cast<EventHandler*>(this));
  lugame::EventManager::getInstance().attachEvent(lugame::eventid::mouseMove, 
                                                  *static_cast<EventHandler*>(this));
  lugame::EventManager::getInstance().attachEvent(levelChangedId,
                                                  *static_cast<EventHandler*>(this));
  
  std::string bitmapPath = m_fileManager.getPathString(FileManager::FOLDER::SPRITES) 
                         + "PowerBalken.png";
  m_powerDotsBmp = al_load_bitmap(bitmapPath.c_str());
}


void PowerArrow::update(
  ALLEGRO_EVENT ev
)
{
  if (ev.type == ALLEGRO_EVENT_TIMER)
  {
    if (m_mouseActive)
    {
      if (++m_currentDelayCounter >= m_powerArrowDelay)
      {
        m_spriteCol--;
        if (m_spriteCol < 0)
          m_spriteCol = m_maxColumn-1;
        m_currentDelayCounter = 0;
      }
      
      m_span = (m_startPos-m_currentPos)*0.5 + m_startPos;
      m_t1 = (m_startPos-m_currentPos).norm()*6.f;
      m_t2 = m_t1;
      m_t3 = m_t1*2.f;
      
      m_t1.rotate90_2D_insitu();
      m_t2.rotate270_2D_insitu();
      
      // Power Bar
      m_powerArrowStart = (m_currentPos-m_startPos).norm()*50.f + m_startPos;
      m_angle = lugame::collision::angle_axis(m_startPos[0]-m_currentPos[0], m_startPos[1]-m_currentPos[1]);
      m_angle -= -90./180.*M_PI;
      
      m_power = (m_startPos-m_currentPos).length();
      
      m_show = true;
    }
  }
}

void 
PowerArrow::draw(
)
{
  /*
   * Draw a ghost arrow of last shot
   */
  if (m_showGhostArrow)
  {
    // Arrow Tail
    int rgb = 0;
    int alpha = 50;
    al_draw_line(m_startPosGhost[0], 
                 m_startPosGhost[1], 
                 m_t1Ghost[0]+m_startPosGhost[0], 
                 m_t1Ghost[1]+m_startPosGhost[1], 
                 al_map_rgba(rgb,rgb,rgb,alpha),
                 m_arrowThickness
                );
    al_draw_line(m_startPosGhost[0], 
                 m_startPosGhost[1], 
                 m_t2Ghost[0]+m_startPosGhost[0], 
                 m_t2Ghost[1]+m_startPosGhost[1], 
                 al_map_rgba(rgb,rgb,rgb,alpha),
                 m_arrowThickness
                );
    
    // Arrow Length
    al_draw_line(m_startPosGhost[0], 
                 m_startPosGhost[1], 
                 m_spanGhost[0], 
                 m_spanGhost[1], 
                 al_map_rgba(rgb,rgb,rgb,alpha),
                 m_arrowThickness
                );

    // Arrow Head Triangle
    al_draw_filled_triangle(m_spanGhost[0]+m_t1Ghost[0], 
                            m_spanGhost[1]+m_t1Ghost[1], 
                            m_spanGhost[0]+m_t2Ghost[0], 
                            m_spanGhost[1]+m_t2Ghost[1], 
                            m_spanGhost[0]+m_t3Ghost[0], 
                            m_spanGhost[1]+m_t3Ghost[1], 
                            al_map_rgba(rgb,rgb,rgb,alpha)
                           );
  }
  
  /*
   * Draw a power arrow
   */
  if (m_show && m_mouseActive)
  {
    // Arrow Tail
    al_draw_line(m_startPos[0], 
                 m_startPos[1], 
                 m_t1[0]+m_startPos[0], 
                 m_t1[1]+m_startPos[1], 
                 al_map_rgb(255,255,255),
                 m_arrowThickness
                );
    al_draw_line(m_startPos[0], 
                 m_startPos[1], 
                 m_t2[0]+m_startPos[0], 
                 m_t2[1]+m_startPos[1], 
                 al_map_rgb(255,255,255),
                 m_arrowThickness
                );
    
    // Arrow Length
    al_draw_line(m_startPos[0], 
                 m_startPos[1], 
                 m_span[0], 
                 m_span[1], 
                 al_map_rgb(255,255,255),
                 m_arrowThickness
                );

    // Arrow Head Triangle
    al_draw_filled_triangle(m_span[0]+m_t1[0], 
                            m_span[1]+m_t1[1], 
                            m_span[0]+m_t2[0], 
                            m_span[1]+m_t2[1], 
                            m_span[0]+m_t3[0], 
                            m_span[1]+m_t3[1], 
                            al_map_rgb(255,255,255)
                           );

    float lenDrawPower = m_power - 50;
    if (lenDrawPower > m_lengthSprite)
      lenDrawPower = m_lengthSprite;
    
    if (lenDrawPower > 0 )
      al_draw_tinted_scaled_rotated_bitmap_region(m_powerDotsBmp, 0, m_spriteCol*7, lenDrawPower, 7, 
                                                  al_map_rgb(255,255,255),
                                                  0, 3.5,
                                                  m_powerArrowStart[0], m_powerArrowStart[1],
                                                  1, 1, 
                                                  m_angle, 0);
  }
  
  
}

void 
PowerArrow::handleEvent(
  const lugame::Event* event
)
{
  /*
   * Mouse Click Left
   */
  if (event->getId() == lugame::eventid::mouseClickLeft)
  {
    m_mouseActive = true;
    m_startPos[0] = m_state.x;
    m_startPos[1] = m_state.y;
  }
  
  /*
   * Mouse Released Left
   */
  if (event->getId() == lugame::eventid::mouseReleaseLeft)
  {
    if (m_mouseActive && m_show)
    {
      m_dir = m_startPos - m_currentPos;
      lulib::math::Matrix<float>* fptr = &m_dir;
      lugame::EventManager::getInstance().sendEvent(ballShotId, fptr);
    }
    m_mouseActive = false;
    m_show = false;
    m_showGhostArrow = true;
    {
      m_startPosGhost = m_startPos;
      m_t1Ghost = m_t1;
      m_t2Ghost = m_t2;
      m_t3Ghost = m_t3;
      m_spanGhost = m_span;
      m_angleGhost = m_angle;
    }
  }
  
  /*
   * Mouse Moved
   */
  if (event->getId() == lugame::eventid::mouseMove)
  {
    al_get_mouse_state(&m_state);
    m_currentPos[0] = m_state.x;
    m_currentPos[1] = m_state.y;
  }
  
  /*
   * Level Changed
   */
  if (event->getId() == levelChangedId)
  {
    m_showGhostArrow = false;
  }
}

