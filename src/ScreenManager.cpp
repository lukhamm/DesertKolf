/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#include "../include/ScreenManager.hpp"

#include <string>
#include <cassert>
#include <lugame/utils/SDBMHash.hpp>


#include "../include/KolfScreen.hpp"
#include "../include/TitleScreen.hpp"
#include "../include/FinishScreen.hpp"
#include "../include/EditorScreen.hpp"


ScreenManager::ScreenManager(
)
{
}


ScreenManager::~ScreenManager(
)
{
  if (m_dataPtr != nullptr)
  {
    std::cout << "Deleting void* data from ScreenManager not happend!" << std::endl;
  }
}


ScreenManager&
ScreenManager::getInstance(
)
{
  static ScreenManager instance;
  return instance;
}


bool 
ScreenManager::initialize(
  const int screenWidth,
  const int screenHeight
)
{
  m_screenWidth = screenWidth;
  m_screenHeight = screenHeight;
  
  currentScreen = createScreen("TitleScreen");
  done = false;
  
  return true;
}


void 
ScreenManager::loadContent(
)
{
  currentScreen->loadContent();
}


void 
ScreenManager::unloadContent(
)
{
  currentScreen->unloadContent();
}


void 
ScreenManager::update(
  ALLEGRO_EVENT ev
)
{
//   if (not fadingActive)
  currentScreen->update(ev);
  gui.update(ev);
  
  // change screen on request
  if (m_requestChangeScreen != "")
  {
    newScreen = createScreen(m_requestChangeScreen);
    currentScreen->unloadContent();
    delete currentScreen;

    currentScreen = newScreen;
    currentScreen->loadContent();
    newScreen = nullptr;
    
    m_requestChangeScreen = "";
  }
  
  if (m_exitGame)
  {
    currentScreen->actionOnExit();
    done = true;
  }
}


void 
ScreenManager::draw(
  ALLEGRO_DISPLAY* display
)
{
//   if (not fadingActive)
  currentScreen->draw(display);
}

void 
ScreenManager::changeScreen(
  const std::string& screen_name
)
{
  m_requestChangeScreen = screen_name;
}

GameScreen* 
ScreenManager::createScreen(
  const std::string& screenName
)
{
  if (screenName == "KolfScreen")
    return new KolfScreen(m_screenWidth, m_screenHeight);
  else if (screenName == "TitleScreen")
    return new TitleScreen(m_screenWidth, m_screenHeight);
  else if (screenName == "EditorScreen")
    return new EditorScreen(m_screenWidth, m_screenHeight);
  else if (screenName == "FinishScreen")
    return new FinishScreen(m_screenWidth, m_screenHeight);
  else
  {
    assert(0 && "Screen not registered");
    return nullptr;
  }
}


void* 
ScreenManager::getData(
)
{
  return m_dataPtr;
}

void 
ScreenManager::passData(
  void* data
)
{
  m_dataPtr = data;
  data = nullptr;
}


void
ScreenManager::demandGameExit(
)
{
  m_exitGame = true;
}