/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#include "../include/LevelEditor.hpp"

#include <fstream>

LevelEditor::LevelEditor(
  const int screenWidth, 
  const int screenHeight
)
: m_screenWdith(screenWidth)
, m_screenHeight(screenHeight)
{
  
}
  
  
void 
LevelEditor::createRandomLevel(
)
{
  /*
   * Free memory and recreate a new level
   */
  clearLevel();
  
  /* 
   * Create startpoint
   */
  Point start = {0, m_rd(100,m_screenHeight-100)};
  m_points.push_back(start);
  
  /*
   * Create drive point
   */
  m_driveStart = {m_rd(10,80), m_rd(100,400)};
  Point drive_end = {m_driveStart.x+20, m_driveStart.y};
  m_points.push_back(m_driveStart);
  m_points.push_back(drive_end);
  
  /*
   * Create hole points
   */
  m_holeStart = {m_rd(m_screenWdith-300,m_screenWdith-50), m_rd(50,m_screenHeight-50)};
  
  /*
   * Determine number of basepoints
   */
  int pointsBetween = m_rd(1,7);
  for (int i=0; i<pointsBetween; i++)
  {
    if (i>0)
    {
      if (m_rd(0,2) > 0)
      {
        Point addPoint{m_rd(m_points.back().x,m_holeStart.x), m_points.back().y};
        m_points.push_back(addPoint);
      }
      else
      {
        Point addPoint{m_rd(m_points.back().x,m_holeStart.x), m_rd(50,m_screenHeight-50)};
        m_points.push_back(addPoint);
      }
    }
    else
    {
      Point addPoint{m_rd(m_points.back().x+20,m_holeStart.x), m_rd(50,m_screenHeight-50)};
      m_points.push_back(addPoint);
    }
  }
  
  m_points.push_back(m_holeStart);
  m_points.push_back(Point{m_holeStart.x+20, m_holeStart.y});
  m_points.push_back(Point{m_holeStart.x+30, m_holeStart.y});
  
  /*
   * Set last point right side of screen
   */
  if (m_rd(0,3) > 0)
  {
    m_points.push_back(Point{m_screenWdith, m_points.back().y});  // last point; plain
  }
  else
  {
    m_points.push_back(Point{m_screenWdith, m_rd(100,m_screenHeight-100)});  // last point; right border
  }
}


void
LevelEditor::writeLevelToFile(
  const int levelNr,
  const std::string& filename
)
{
  std::ofstream outStream(filename);
  /*
   *  Print out level
   */
  outStream << "# lines with # are ignored" << std::endl << std::endl;
  outStream << "# Instructions: \n# The hole needs 20 horizontal space \n# \n#" << std::endl << std::endl;
  
  outStream << "level_nr = " << std::to_string(levelNr) << std::endl << std::endl << std::endl;
  
  outStream << "# nr_of_lines" << std::endl;
  outStream << "# x1 y1  x2 y2" << std::endl;
  outStream << "#" << std::endl;
  outStream << "points = {" << std::endl;
  outStream << m_points.size() << std::endl;
  for (const Point& point : m_points)
  {
    outStream << point.x << " " << point.y << std::endl;
  }
  outStream << "}" << std::endl << std::endl << std::endl;
  
  outStream << "# \n# start position \n#" << std::endl;
  outStream << "drive = {" << std::endl;
  outStream << m_driveStart.x << "  " << m_driveStart.y << std::endl;
  outStream << "}" << std::endl << std::endl << std::endl;
  outStream << "# \n# hole (offset for left upper corner of hole) \n#" << std::endl;
  outStream << "hole = {" << std::endl;
  outStream << m_holeStart.x << "  " << m_holeStart.y << std::endl;
  outStream << "}" << std::endl;
}


void
LevelEditor::updateLevelOverview(
  const int totalNrOfLevels,
  const std::string& filename
)
{
  std::ofstream outStream(filename);
  outStream << "# This is the overview about levels. This needs to be adjusted" << std::endl;
  outStream << "# in order for levels to be recognized." << std::endl << std::endl;
  
  outStream << "nr_of_levels = " << std::to_string(totalNrOfLevels) << std::endl << std::endl;
  outStream << "levels = {" << std::endl;
  for (int i=1; i< totalNrOfLevels+1; i++)
  {
    outStream << "level" << std::to_string(i) << ".dkl" << std::endl;
  }
  outStream << "}" << std::endl;
}


void
LevelEditor::clearLevel(
)
{
  m_points.clear();
}