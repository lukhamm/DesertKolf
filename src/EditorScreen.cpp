/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#include "../include/EditorScreen.hpp"

#include <fstream>

#include <lugame/utils/EventManager.hpp>

#include "../include/ScreenManager.hpp"

constexpr uint32_t eventId_randomLevelButton_released{
    lugame::SDBMCalculator<30>::CalculateValue("randomLevelButton_releasedLeft")
  };
constexpr uint32_t eventId_saveLevelButton_released{
    lugame::SDBMCalculator<28>::CalculateValue("saveLevelButton_releasedLeft")
  };
constexpr uint32_t eventId_returnButton_released{
    lugame::SDBMCalculator<25>::CalculateValue("returnButton_releasedLeft")
  };

EditorScreen::EditorScreen(
  const int screenWidth, 
  const int screenHeight
) 
: GameScreen(screenWidth, screenHeight)
, m_levelEditor(screenWidth, screenHeight)
, stylePrimitive(15)
{
  
}


EditorScreen::~EditorScreen(
)
{
  if (m_levelPtr != nullptr)
  {
    delete m_levelPtr;
  }
};


void 
EditorScreen::loadContent(
)
{
  box.insert(&m_randomButton);
  box.insert(&m_saveButton);
  box.insert(&m_returnButton);
  
  lugame::EventManager::getInstance().attachEvent(eventId_randomLevelButton_released, 
                                                  *static_cast<EventHandler*>(this));
  lugame::EventManager::getInstance().attachEvent(eventId_saveLevelButton_released, 
                                                  *static_cast<EventHandler*>(this));
  lugame::EventManager::getInstance().attachEvent(eventId_returnButton_released, 
                                                  *static_cast<EventHandler*>(this));
}


void 
EditorScreen::unloadContent(
)
{
  lugame::EventManager::getInstance().detachEvent(eventId_randomLevelButton_released, 
                                                  *static_cast<EventHandler*>(this));
  lugame::EventManager::getInstance().detachEvent(eventId_saveLevelButton_released, 
                                                  *static_cast<EventHandler*>(this));
  lugame::EventManager::getInstance().detachEvent(eventId_returnButton_released, 
                                                  *static_cast<EventHandler*>(this));
}


void 
EditorScreen::draw(
  ALLEGRO_DISPLAY* /*display*/
)
{
  box.draw();
  if (m_levelPtr != nullptr)
    m_levelPtr->draw();
}


void 
EditorScreen::update(
  ALLEGRO_EVENT /*ev*/
)
{
}


void
EditorScreen::handleEvent(
  const lugame::Event* event
)
{
  if (event->getId() == eventId_randomLevelButton_released)
  {
    createRandomLevel();
  }
  else if (event->getId() == eventId_returnButton_released)
  {
    ScreenManager::getInstance().changeScreen("TitleScreen");
  }
  else if (event->getId() == eventId_saveLevelButton_released)
  {
    saveLevel();
  }
}


void
EditorScreen::createRandomLevel(
)
{
  m_levelEditor.createRandomLevel();
  
  std::string levelFolder = m_fileManager.getPathString(FileManager::FOLDER::LEVELS);
  std::string tmpFile = levelFolder + "tmp.dkl";
  m_levelEditor.writeLevelToFile(0, tmpFile);
  
  if (m_levelPtr != nullptr)
    delete m_levelPtr;
  m_levelPtr = new Level(tmpFile, 0, m_screenWidth, m_screenHeight);
}


void
EditorScreen::saveLevel(
)
{
  std::string levelsFolder = m_fileManager.getPathString(FileManager::FOLDER::LEVELS);

  int levelCounter = 1;
  std::string levelName = levelsFolder + "level" + std::to_string(levelCounter) + ".dkl";
  
  while (al_filename_exists(levelName.c_str()))
  {
    levelCounter++;
    levelName = levelsFolder + "level" + std::to_string(levelCounter) + ".dkl";
  }
  int nrOfLevels = levelCounter;
  
  levelName = levelsFolder + "level" + std::to_string(nrOfLevels) + ".dkl";
  
  std::cout << "Saved level to: " << levelName << std::endl;
  m_levelEditor.writeLevelToFile(nrOfLevels, levelName);
  
  m_levelEditor.updateLevelOverview(nrOfLevels, levelsFolder+"Overview.dkl");
}
