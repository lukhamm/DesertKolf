/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#include "../include/Ball.hpp"

#include <algorithm>
#include <cmath>

#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>

#include <lugame/utils/SDBMHash.hpp>
#include <lugame/utils/EventManager.hpp>
#include <lugame/engines2d/Collision.hpp>

#include "../include/LevelManager.hpp"

constexpr uint32_t ballShotId = lugame::SDBMCalculator<9>::CalculateValue("ball_shot");


Ball::Ball(
  const float /*screenWidth*/, 
  const float /*screenHeight*/,
  const float posX,
  const float posY
)
: m_pos(2)
{ 
  m_startPos[0] = posX;
  m_startPos[1] = posY;
  
  m_pos = m_startPos;
  
  m_soundManager.loadContent();
  
  m_name = "Ball";
}



Ball::~Ball(
)
{
  lugame::EventManager::getInstance().detachEvent(ballShotId, *static_cast<EventHandler*>(this));
  m_soundManager.unloadContent();
};



void 
Ball::loadContent(
)
{
  lugame::EventManager::getInstance().attachEvent(ballShotId, *static_cast<EventHandler*>(this));
}



void 
Ball::update(
  ALLEGRO_EVENT ev,
  void* data
)
{
  lulib::math::Matrix<float> old_offset = m_offset;
  if (ev.type == ALLEGRO_EVENT_TIMER)
  {
    // get level pointer
    LevelManager* levelManager = static_cast<LevelManager*>(data);
    if (m_isHit)
    {
      levelManager->addHit();
      m_isHit = false;
    }
    
    lulib::math::Matrix<float> old_pos = m_pos;
    m_pos += m_v;
    
    /*
     * This is for continuous collision detection
     */
    m_frictionCounter++;
    constexpr float friction{0.7};
    constexpr int frictionCounterMax{10};
    if (m_v.length() > m_radius)
    {
      int m = m_v.length() / m_radius + 1;
      
      lulib::math::Matrix<float> in_between_pos(2);
      for (int i = 0; i < m; i++)
      {
        in_between_pos = old_pos + (m_v.norm() * m_radius) * (i+1);
        
        if (levelManager->checkCollisions(in_between_pos, m_radius, m_offset))
        {
          lulib::math::Matrix<float> n(2);
          n = (in_between_pos - m_offset).norm();      // normal vector according to hitting direction
          float scalarp = (n.transpose()*m_v)[0];
          m_v = (n*scalarp*2.f - m_v) * -1.f;
          
          if (m_frictionCounter > frictionCounterMax)
          {
            m_v *= friction;
            m_frictionCounter = 0;
          }
          m_pos = m_offset;
          break;
        }
      }
    }
    /*
     * This is the normal collision detection
     */
    else
    {
      if (levelManager->checkCollisions(m_pos, m_radius, m_offset))
      {
        lulib::math::Matrix<float> n(2);
        n = (m_pos - m_offset).norm();      // normal vector according to hitting direction
        float scalarp = (n.transpose()*m_v)[0];
        m_v = (n*scalarp*2.f - m_v) * -1.f;
        
        if (m_frictionCounter > frictionCounterMax)
        {
          m_v[1] *= friction;
          if (fabs(m_v[0]) > 0.15 and fabs(m_v[1]) > 0.0001)
            m_v[0] -= 0.15 * m_v[0];///fabs(m_v[0]);
          else
            m_v[0] *= friction;
          m_frictionCounter = 0;
        }
        m_pos = m_offset;
      }
    }
    
    /*
     * Add (gravitational) forces
     */
    m_v += gravity*0.3f;
    
    /*
     * If ball doesnt move anymore its state is set to resting
     */
    if ( (m_pos-old_pos).length() < 0.05 and m_state != STATE::holed)
    {
      m_state = STATE::resting;
    }
    
    /*
     * If the ball's state is resting; check if it is in the hole
     */
    if (m_state == STATE::resting)
    {
      if (levelManager->isBallHoled(m_pos, m_radius))
      {
        m_state = STATE::holed;
      }
    }
    
    /*
     * Check screen boundary collisions
     */
    if (lugame::collision::circle_lineSegment(0,0,0,500,m_pos[0],m_pos[1],m_radius))
    {
      m_pos = m_startPos;
      m_v = 0;
    }
    if (lugame::collision::circle_lineSegment(900,0,900,500,m_pos[0],m_pos[1],m_radius))
    {
      m_pos = m_startPos;
      m_v = 0;
    }
    if (lugame::collision::circle_lineSegment(900,500,900,500,m_pos[0],m_pos[1],m_radius))
    {
      m_pos = m_startPos;
      m_v = 0;
    }
 
//     if (lugame::collision::circle_border_rectangle(0,0,900,500,m_pos[0],m_pos[1],m_radius))
//     {
//       m_pos = m_startPos;
//       m_v = 0;
//     }
    
  }
}



void 
Ball::draw(
)
{
  al_draw_filled_circle(m_pos[0], m_pos[1], m_radius, al_map_rgb(255,255,255));
}



void 
Ball::changeState(
  const STATE newState
)
{
  if (newState == STATE::moving)
  {
  }
  m_state = newState;
}



void 
Ball::handleEvent(
  const lugame::Event* event
)
{
  if (event->getId() == ballShotId and m_state == STATE::resting)
  {
    void* dataPtr;
    dataPtr = event->getDataPtr();
    
    lulib::math::Matrix<float>* fptr;
    if (dataPtr != nullptr)
    {
      fptr = static_cast<lulib::math::Matrix<float>*>(dataPtr);
      if (fptr->length() < 0.001) return;
      m_v = *fptr/50.f;
    }
    
    changeState(STATE::moving);
    inAir = true;
    m_soundManager.play(SOUNDS::HIT);
    m_isHit = true;
  }
}



bool
Ball::isHoled(
)
{
  if (m_state == STATE::holed)
    return true;
  else
    return false;
}



void
Ball::reset(
  const float startPosX,
  const float startPosY
)
{
  m_startPos[0] = startPosX;
  m_startPos[1] = startPosY;
  
  m_pos = m_startPos;
  
  m_state = STATE::resting;
  m_v = 0;
  m_offset = 0;
}