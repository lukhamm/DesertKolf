/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#include "../include/Flag.hpp"

#include <allegro5/allegro_primitives.h>


Flag::Flag(
  const float x,
  const float y,
  const int holeNr
)
{
  m_pos[0] = x + 25.f;
  m_pos[1] = y;
  m_holeNr = holeNr;
  
  std::string fontPath = m_fileManager.getPathString(FileManager::FOLDER::RESOURCES)
                       + "Style_Primitive/fonts/bitstream_vera_sans_font_6016/VeraBd.ttf";
  m_font = al_load_ttf_font(fontPath.c_str(), 14, 0);
}


void Flag::draw(
) const
{
  al_draw_line(m_pos[0], m_pos[1], m_pos[0], m_pos[1]-35, al_map_rgb(116, 117, 121), 4); 
  al_draw_filled_rectangle(m_pos[0]+2, m_pos[1]-32, m_pos[0]+22, m_pos[1]-14, al_map_rgb(251,250,150));
  al_draw_filled_triangle(m_pos[0]+22, m_pos[1]-32,
                          m_pos[0]+22, m_pos[1]-14,
                          m_pos[0]+26, m_pos[1]-23, al_map_rgb(251,250,150));
  al_draw_textf(m_font, al_map_rgb(116,117,121), m_pos[0]+4, m_pos[1]-30, 0, "%i", m_holeNr);
}