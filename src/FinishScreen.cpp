/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#include "../include/FinishScreen.hpp"

#include <fstream>
#include <algorithm>
#include <cstdio>

#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_primitives.h>

#include <lupriv/ReadWriteFiles.hpp>

#include <lugame/utils/EventManager.hpp>

#include "../include/ScreenManager.hpp"

FinishScreen::FinishScreen(
  const int screenWidth, 
  const int screenHeight
) : GameScreen(screenWidth, screenHeight)
{
  std::string fontPath = m_fileManager.getPathString(FileManager::FOLDER::RESOURCES)
                       + "Style_Primitive/fonts/bitstream_vera_sans_font_6016/VeraBd.ttf";
  m_hitsFont = al_load_ttf_font(fontPath.c_str(), 30, 0);
  m_highscoreFont = al_load_ttf_font(fontPath.c_str(), 20, 0);
  
  m_keyboardInput.startRecording(10);
}


void 
FinishScreen::loadContent(
)
{
  lugame::EventManager::getInstance().attachEvent(lugame::SDBMHash("backButton_clickedLeft"), 
                                                  *static_cast<EventHandler*>(this));
  lugame::EventManager::getInstance().attachEvent(lugame::SDBMHash("backButton_releasedLeft"), 
                                                  *static_cast<EventHandler*>(this));
  lugame::EventManager::getInstance().attachEvent(lugame::SDBMHash("submitButton_releasedLeft"), 
                                                  *static_cast<EventHandler*>(this));
  
  m_hitsPtr = static_cast<int*>(ScreenManager::getInstance().getData());
  ScreenManager::getInstance().passData(nullptr);
  
  m_hitsOverall = *m_hitsPtr;
  delete m_hitsPtr;
  
  eraseSaveGame();
  m_readHighscoreName = loadHighScores();
  
  if (m_readHighscoreName)
    box.insert(&submitButton);
  else
    box.insert(&backButton);
}

void 
FinishScreen::unloadContent(
)
{
  lugame::EventManager::getInstance().detachEvent(lugame::SDBMHash("backButton_clickedLeft"), 
                                                  *static_cast<EventHandler*>(this));
  lugame::EventManager::getInstance().detachEvent(lugame::SDBMHash("backButton_releasedLeft"), 
                                                  *static_cast<EventHandler*>(this));
  lugame::EventManager::getInstance().detachEvent(lugame::SDBMHash("submitButton_releasedLeft"), 
                                                  *static_cast<EventHandler*>(this));
  al_destroy_font(m_hitsFont);
}

void 
FinishScreen::draw(
  ALLEGRO_DISPLAY* /*display*/
)
{
  box.draw();
  
  if (not m_readHighscoreName)
  {
    al_draw_textf(m_hitsFont, al_map_rgb(255,255,255), m_screenWidth/2.f, 35, 
                  ALLEGRO_ALIGN_CENTRE, "Total Number of Hits: %i", m_hitsOverall);

    al_draw_line(50,90,850,90,al_map_rgb(255,255,255), 3);  
    al_draw_text(m_hitsFont, al_map_rgb(255,255,255), m_screenWidth/2.f, 100, 
                  ALLEGRO_ALIGN_CENTRE, "Highscores:");
    al_draw_text(m_highscoreFont, al_map_rgb(255,255,255), m_screenWidth/2.f-150, 135, 
                  ALLEGRO_ALIGN_LEFT, "Position");
    al_draw_text(m_highscoreFont, al_map_rgb(255,255,255), m_screenWidth/2.f, 135, 
                  ALLEGRO_ALIGN_LEFT, "Hits");
    al_draw_text(m_highscoreFont, al_map_rgb(255,255,255), m_screenWidth/2.f+100, 135, 
                  ALLEGRO_ALIGN_LEFT, "Name");
    
    int yOffset = 155;
    int counter = 1;
    for (auto p : m_highscores)
    {
      al_draw_textf(m_highscoreFont, al_map_rgb(255,255,255), m_screenWidth/2.f-150, yOffset, 
                  ALLEGRO_ALIGN_LEFT, "%2i.", counter);
      al_draw_textf(m_highscoreFont, al_map_rgb(255,255,255), m_screenWidth/2.f, yOffset, 
                  ALLEGRO_ALIGN_LEFT, "%4i", p.score);
      al_draw_textf(m_highscoreFont, al_map_rgb(255,255,255), m_screenWidth/2.f+100, yOffset, 
                  ALLEGRO_ALIGN_LEFT, "%s", p.name.c_str());
      yOffset += 22;
      counter++;
    }
    al_draw_line(50,380,850,380,al_map_rgb(255,255,255), 3);
  }
  
  if (m_readHighscoreName)
  {
    al_draw_text(m_hitsFont, al_map_rgb(255,255,255), m_screenWidth/2.f, m_screenHeight/2.f-100,
                 ALLEGRO_ALIGN_CENTRE, "New Highscore!!!");
    al_draw_text(m_highscoreFont, al_map_rgb(255,255,255), m_screenWidth/2.f, m_screenHeight/2.f-50,
                 ALLEGRO_ALIGN_CENTRE, "Please Enter your name:");
    
    std::string tmpString = m_keyboardInput.getString();
    while (tmpString.size() < 10)
    {
      tmpString += "-";
    }
    al_draw_textf(m_highscoreFont, al_map_rgb(255,255,255), m_screenWidth/2.f, m_screenHeight/2.f,
                  ALLEGRO_ALIGN_CENTRE, "%s", tmpString.c_str());
  }
}


void 
FinishScreen::update(
  ALLEGRO_EVENT ev
)
{
  if (m_readHighscoreName and m_nameEntered)
  {
    m_keyboardInput.stopRecording();
    m_readHighscoreName = false;
  }

  m_keyboardInput.update(ev);
  
  if (m_inputManager.isKeyPressed(ev, ALLEGRO_KEY_ENTER) and m_readHighscoreName)
  {
    m_readHighscoreName = false;
    saveHighScore(m_keyboardInput.getString());
    m_nameEntered = true;
  }
  
  if (m_nameEntered)
  {
    box.erase("submitButton");
    box.insert(&backButton);
    m_nameEntered = false;
  }
}

void
FinishScreen::handleEvent(
  const lugame::Event* event
)
{
  if (event->getId() == lugame::SDBMHash("backButton_clickedLeft"))
  {
  }
  
  if (event->getId() == lugame::SDBMHash("backButton_releasedLeft"))
  {
    ScreenManager::getInstance().changeScreen("TitleScreen");
  }
  
  if (event->getId() == lugame::SDBMHash("submitButton_releasedLeft") and m_readHighscoreName)
  {
    m_readHighscoreName = false;
    saveHighScore(m_keyboardInput.getString());
    m_nameEntered = true;
  }
}


bool 
FinishScreen::loadHighScores(
)
{
  std::string highscoreFileName = m_fileManager.getPathString(FileManager::FOLDER::SAVEGAMES)
                                + "highscores.dks";
  try
  {
    lulib::ReadWrite rw;
    lulib::io::BinaryFileIO binData = rw.readData(highscoreFileName);
    if (binData.size() > 0)
    {
      std::vector<HighscoreType> highscores;
      HighscoreType hscore;
      std::size_t nrOfScores = binData.read<std::size_t>();
      
      for (std::size_t i=0; i<nrOfScores; i++)
      {
        hscore.score = binData.read<int>();
        std::size_t lenName = binData.read<std::size_t>();
        hscore.name = binData.readString(lenName);
        
        highscores.push_back(hscore);
      }
      m_highscores = highscores;
    }
    else
    {
      m_highscores = std::vector<HighscoreType>{};
    }
    
    if (m_hitsOverall < m_highscores.back().score or m_highscores.size() < 10) 
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  catch (...)
  {
    return true;
  }
}

bool FinishScreen::saveHighScore(
  const std::string& name
)
{
  if (m_highscores.size() == 0 or m_highscores.size() < 10)
  {
    m_highscores.push_back(HighscoreType{m_hitsOverall, name});
    std::sort(m_highscores.begin(), m_highscores.end());
  }
  else
  {
    m_highscores.push_back(HighscoreType{m_hitsOverall, name});
    std::sort(m_highscores.begin(), m_highscores.end());
    m_highscores = std::vector<HighscoreType>(m_highscores.begin(), m_highscores.begin()+10);
  }
  
  // Save vector
  lulib::ReadWrite rw;
  lulib::io::BinaryFileIO data;
  std::size_t nrOfScores = m_highscores.size();
  data.write(nrOfScores);
  for (const auto& p : m_highscores)
  {
    data.write(p.score);
    std::size_t lenName = p.name.size();
    data.write(lenName);
    data.writeString(p.name);
  }
  
  std::string highscoreFileName = m_fileManager.getPathString(FileManager::FOLDER::SAVEGAMES)
                                + "highscores.dks";
  rw.writeData(highscoreFileName, data);
  return true;
}


bool FinishScreen::eraseSaveGame(
)
{
  std::string saveFilename = m_fileManager.getPathString(FileManager::FOLDER::SAVEGAMES)
                                + "save.dks";
  std::remove(saveFilename.c_str());
  return true;
}