/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#include "../include/Hole.hpp"

#include <allegro5/allegro_primitives.h>

#include <lugame/engines2d/Collision.hpp>

Hole::Hole(
  const float offsetX,
  const float offsetY,
  std::vector<LineType>& lines,
  const int levelNr
)
: m_p1 { 0, 0},
  m_p2 { 0,19},
  m_p3 {10,22},
  m_p4 {20,19},
  m_p5 {20, 0},
  m_offset {offsetX, offsetY},
  m_flag(offsetX, offsetY, levelNr)
{
  lines.push_back(LineType{m_offset[0]+m_p1[0], m_offset[1]+m_p1[1], 
                           m_offset[0]+m_p2[0], m_offset[1]+m_p2[1]});
  
  lines.push_back(LineType{m_offset[0]+m_p2[0], m_offset[1]+m_p2[1], 
                           m_offset[0]+m_p3[0], m_offset[1]+m_p3[1]});
  
  lines.push_back(LineType{m_offset[0]+m_p3[0], m_offset[1]+m_p3[1], 
                           m_offset[0]+m_p4[0], m_offset[1]+m_p4[1]});
  
  lines.push_back(LineType{m_offset[0]+m_p4[0], m_offset[1]+m_p4[1], 
                           m_offset[0]+m_p5[0], m_offset[1]+m_p5[1]});
}

void
Hole::draw(
) const
{
  al_draw_filled_rectangle(m_offset[0]+m_p1[0], m_offset[1]+m_p1[1], 
                           m_offset[0]+m_p4[0], m_offset[1]+m_p4[1],
                           al_map_rgb(201,146,108));
  
  al_draw_filled_triangle(m_offset[0]+m_p2[0], m_offset[1]+m_p2[1], 
                          m_offset[0]+m_p3[0], m_offset[1]+m_p3[1], 
                          m_offset[0]+m_p4[0], m_offset[1]+m_p4[1],
                          al_map_rgb(201,146,108));
  
  m_flag.draw();
}

void
Hole::setOffset(
  const float offsetX,
  const float offsetY
)
{
  m_offset[0] = offsetX;
  m_offset[1] = offsetY;
}



bool
Hole::isBallHoled(
  const lulib::math::Matrix<float>& pos,
  const float radius
)
{
  return lugame::collision::point_circle(m_offset[0]+m_p3[0], m_offset[1]+m_p3[1]-1,
                                         pos[0], pos[1], radius);
}