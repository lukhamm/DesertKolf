/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#include "../include/Drive.hpp"

#include <allegro5/allegro_primitives.h>


Drive::Drive(
  const float posX, 
  const float posY,
  std::vector<LineType>& lines
)
{
  m_p1[0] = posX;
  m_p1[1] = posY+2.5f;
  m_p2[0] = posX+20;
  m_p2[1] = posY+2.5f;
  
  lines.push_back(LineType{m_p1[0], m_p1[1], m_p2[0], m_p2[1]});
}


void 
Drive::draw(
)
{
  al_draw_line(m_p1[0], m_p1[1], m_p2[0], m_p2[1], al_map_rgb(99,99,99), 5);
}


lulib::math::Matrix<float>
Drive::getStartingPosition(
)
{
  lulib::math::Matrix<float> startPos = {(m_p1[0]+m_p2[0])/2.f, m_p1[1]-3.f}; // 3 = radius
  return startPos;
}