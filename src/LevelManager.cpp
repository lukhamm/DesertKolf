/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#include "../include/LevelManager.hpp"

#include <iostream>
#include <sstream>

#include <lupriv/InputFileParser.hpp>
#include <lupriv/StringManipulation.hpp>

#include <lugame/utils/SDBMHash.hpp>
#include <lugame/utils/EventManager.hpp>

#include <allegro5/allegro_primitives.h>

constexpr int32_t levelChangedId = lugame::SDBMCalculator<9>::CalculateValue("level_changed");


LevelManager::LevelManager(
//   const std::string& initLevelFoldername,
  const int screenWidth,
  const int screenHeight
)
: m_screenWidth(screenWidth)
, m_screenHeight(screenHeight)
{
  lugame::EventManager::getInstance().registerEvent(levelChangedId);
  
  m_rootFolderName = m_fileManager.getPathString(FileManager::FOLDER::LEVELS);
  
  lulib::InputFileParser parser;
  
  parser.registerKeyword("nr_of_levels");
  parser.registerKeyword("levels");
  
  parser.readFile(m_rootFolderName+"Overview.dkl");

  std::stringstream ss = parser.getValuesStream("nr_of_levels");
  ss >> m_nrLevels;
  
  std::string levelString = parser.getValues("levels");
  m_levelNames = lulib::string::split(levelString);
  
  std::string fontPath = m_fileManager.getPathString(FileManager::FOLDER::RESOURCES)
                       + "/Style_Primitive/fonts/bitstream_vera_sans_font_6016/VeraBd.ttf";
  m_levelFont = al_load_ttf_font(fontPath.c_str(), 20, 0);
  
  createLevel();
}


LevelManager::~LevelManager(
)
{
  if (m_levelPtr != nullptr)
    delete m_levelPtr;
}

void
LevelManager::createLevel(
)
{
  m_levelPtr = new Level(m_rootFolderName+m_levelNames[m_currentLevel],
                         static_cast<int>(m_currentLevel),
                         m_screenWidth,
                         m_screenHeight);
}


bool 
LevelManager::setLevel(
  const std::size_t levelNr,
  const int hits
)
{
  if (m_levelPtr != nullptr)
    delete m_levelPtr;
   
  m_hitsOverall = hits;
  m_hitsCurrentLevel = 0;
  m_currentLevel = levelNr;
  
  if (m_currentLevel < m_levelNames.size())
  {
    createLevel();
    return true;
  }
  else
  {
    std::cout << "Fatal Error: Could not find level nr: " << m_currentLevel << std::endl;
    m_levelPtr = nullptr;
    return false;
  }
}


void
LevelManager::drawLevel(
)
{
  if (m_levelPtr != nullptr)
  {
    m_levelPtr->draw();
  }
  
  al_draw_textf(m_levelFont, al_map_rgb(255,255,255), 430, 40, 0, "%i", m_hitsOverall);
  al_draw_textf(m_levelFont, al_map_rgb(255,255,255), 470, 40, 0, "+%i", m_hitsCurrentLevel);
}



bool
LevelManager::checkCollisions(
  const lulib::math::Matrix<float>& pos,
  const float radius,
  lulib::math::Matrix<float>& offset
)
{
  return m_levelPtr->checkCollisions(pos, radius, offset);
}



bool
LevelManager::isBallHoled(
  const lulib::math::Matrix<float>& pos,
  const float radius
)
{
  return m_levelPtr->isBallHoled(pos, radius);
}



lulib::math::Matrix<float>
LevelManager::getStartingPosition(
)
{
  return m_levelPtr->getStartingPosition();
}



std::size_t
LevelManager::nextLevel(
)
{
  delete m_levelPtr;
   
  m_hitsOverall += m_hitsCurrentLevel;
  m_hitsCurrentLevel = 0;
  lugame::EventManager::getInstance().sendEvent(levelChangedId);
  m_currentLevel++;
  
  if (m_currentLevel < m_levelNames.size())
  {
    createLevel();
  }
  else
  {
    m_levelPtr = nullptr;
  }
  
  return m_currentLevel;
}


void
LevelManager::addHit(
)
{
  m_hitsCurrentLevel++;
}


std::size_t
LevelManager::nrOfLevels(
) const
{
  return m_levelNames.size();
}


int
LevelManager::getHits(
) const
{
  return m_hitsOverall;
}

int
LevelManager::getTotalHits(
) const
{
  return m_hitsOverall+m_hitsCurrentLevel;
}

std::size_t
LevelManager::getCurrentLevel(
) const
{
  return m_currentLevel;
}