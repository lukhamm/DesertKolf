/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#include <allegro5/allegro.h>
#include <allegro5/allegro_native_dialog.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_acodec.h>
#include <allegro5/allegro_primitives.h>

#include "../include/ScreenManager.hpp"
#include "../include/TitleScreen.hpp"
#include "../include/KolfScreen.hpp"
#include "../include/FileManager.hpp"

#include <lugame/gui/Button.hpp>
#include <lugame/gui/Box.hpp>
#include <lugame/gui/styles/Primitive.hpp>
#include <lugame/utils/EventManager.hpp>

static constexpr int screenWidth  = 900;
static constexpr int screenHeight = 500;


int main (
  int argc, 
  char **argv
)
{
  const float FPS = 60.0f;
  
  ALLEGRO_DISPLAY *display;
  
  //--------------------------
  // Allegro inizialize
  //--------------------------
  if(!al_init())
  {
    al_show_native_message_box(NULL, "Error", "Error", "Cannot initialize Allegro.", NULL, 0);
  }
  
  display = al_create_display(screenWidth, screenHeight);
  
  if(!display)
  {
    al_show_native_message_box(NULL, "Error", "Error", "Cannot initialize Display.", NULL, 0);
  }
  
  al_install_keyboard();
  al_install_mouse();
  al_install_audio();
  al_init_image_addon();
  al_init_acodec_addon();
  al_init_font_addon();
  al_init_ttf_addon();
  al_init_primitives_addon();
  
  //--------------------------
  // : essential variables
  //--------------------------
  ALLEGRO_TIMER *timer = al_create_timer(1.0f / FPS);
  ALLEGRO_EVENT_QUEUE *event_queue = al_create_event_queue();
//   ALLEGRO_KEYBOARD_STATE keyState;
  
  //--------------------------
  // : Register event sources
  //--------------------------
  al_register_event_source(event_queue, al_get_keyboard_event_source());
  al_register_event_source(event_queue, al_get_timer_event_source(timer));
  al_register_event_source(event_queue, al_get_mouse_event_source());
  al_register_event_source(event_queue, al_get_display_event_source(display));
  
  //--------------------------
  // : Initialize Variables
  //--------------------------
  bool done = false;
  bool redraw = true;
  
  //--------------------------
  // : Init and load screens
  //--------------------------
  assert(ScreenManager::getInstance().initialize(screenWidth, screenHeight));
  ScreenManager::getInstance().loadContent();
  
  //--------------------------
  // : Define necessary variables
  //--------------------------
  FileManager fm;
  fm.printBuildPath();
  
  //--------------------------
  // : Game loop starts here
  //--------------------------
  al_start_timer(timer);
  while (!done)
  {
    ALLEGRO_EVENT ev;
    al_wait_for_event(event_queue, &ev);
    
    if (ScreenManager::getInstance().done)
      done = true;
    
    switch (ev.type)
    {
      case ALLEGRO_EVENT_DISPLAY_CLOSE:
        ScreenManager::getInstance().demandGameExit();
        break;
      case ALLEGRO_EVENT_TIMER:
        redraw = true;
        break;
    }
    
    ScreenManager::getInstance().update(ev);
    
    if (redraw && al_is_event_queue_empty(event_queue))
    {
      ScreenManager::getInstance().draw(display);
      
      al_flip_display();
      al_clear_to_color(al_map_rgb(201,146,108));
    }
  }
  
  ScreenManager::getInstance().unloadContent();
  
  al_destroy_display(display);
  al_destroy_event_queue(event_queue);
  al_destroy_timer(timer);
  
  return 0;
  
}