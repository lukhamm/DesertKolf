/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#include "../include/FileManager.hpp"

#include <iostream>

FileManager::FileManager(
)
{
  m_paths[FOLDER::BUILD] = al_get_standard_path(ALLEGRO_RESOURCES_PATH);
  
  m_paths[FOLDER::RESOURCES] = al_get_standard_path(ALLEGRO_RESOURCES_PATH);
  al_append_path_component(m_paths[FOLDER::RESOURCES], m_resources.c_str());
  
  m_paths[FOLDER::SPRITES] = al_get_standard_path(ALLEGRO_RESOURCES_PATH);
  al_append_path_component(m_paths[FOLDER::SPRITES], m_resources.c_str());
  al_append_path_component(m_paths[FOLDER::SPRITES], "Sprites");
  
  m_paths[FOLDER::LEVELS] = al_get_standard_path(ALLEGRO_RESOURCES_PATH);
  al_append_path_component(m_paths[FOLDER::LEVELS], m_resources.c_str());
  al_append_path_component(m_paths[FOLDER::LEVELS], "Levels");
  
  m_paths[FOLDER::SOUNDS] = al_get_standard_path(ALLEGRO_RESOURCES_PATH);
  al_append_path_component(m_paths[FOLDER::SOUNDS], m_resources.c_str());
  al_append_path_component(m_paths[FOLDER::SOUNDS], "Sounds");
  
  m_paths[FOLDER::SAVEGAMES] = al_get_standard_path(ALLEGRO_RESOURCES_PATH);
  al_append_path_component(m_paths[FOLDER::SAVEGAMES], m_resources.c_str());
  al_append_path_component(m_paths[FOLDER::SAVEGAMES], "SaveGames");
  
  checkFolderExistance();
}


bool
FileManager::checkFolderExistance(
)
{
  for (const std::pair<FOLDER, ALLEGRO_PATH*>& pathPair : m_paths)
  {
    if (not al_filename_exists(al_path_cstr(pathPair.second, ALLEGRO_NATIVE_PATH_SEP)))
    {
      std::cout << "ERROR: " 
                << al_path_cstr(pathPair.second, ALLEGRO_NATIVE_PATH_SEP) 
                << " does not exist!" << std::endl;
      return false;
    }
  }
  return true;
}


FileManager::~FileManager(
)
{
  for (const std::pair<FOLDER, ALLEGRO_PATH*>& pathPair : m_paths)
  {
    al_destroy_path(pathPair.second);
  }
}


ALLEGRO_PATH*
FileManager::getPath(
  const FOLDER pathType
)
{
  if (m_paths.find(pathType) == m_paths.end())
  {
    std::cout << "ERROR: Could not find pathType" << __func__ << std::endl;
    return nullptr;
  }
  else
  {
    return m_paths[pathType];
  }
}

std::string
FileManager::getPathString(
  const FOLDER pathType
)
{
  return std::string(al_path_cstr(getPath(pathType), ALLEGRO_NATIVE_PATH_SEP));
}

  
void
FileManager::printBuildPath(
)
{
  
}