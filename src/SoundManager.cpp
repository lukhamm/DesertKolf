/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#include "../include/SoundManager.hpp"

#include "../include/FileManager.hpp"

void
SoundManager::loadContent(
)
{
  FileManager fm;
  std::string soundPath = fm.getPathString(FileManager::FOLDER::SOUNDS) + "ball_hit_short.ogg";
  al_reserve_samples(10);
  m_sample_map[SOUNDS::HIT] = al_load_sample(soundPath.c_str());
  
  if (m_sample_map[SOUNDS::HIT] != nullptr)
  {
    ALLEGRO_SAMPLE_INSTANCE* tmp_inst = al_create_sample_instance(m_sample_map[SOUNDS::HIT]);
    m_instances.push_back(tmp_inst);
  }
  
  for (std::vector<ALLEGRO_SAMPLE_INSTANCE*>::size_type i=0; i<m_instances.size(); i++)
  {
    al_attach_sample_instance_to_mixer(m_instances[i], al_get_default_mixer());
  }
}

void
SoundManager::unloadContent(
)
{
  std::map<SOUNDS, ALLEGRO_SAMPLE*>::iterator m_it;
  for (m_it = m_sample_map.begin(); m_it != m_sample_map.end(); m_it++)
  {
    al_destroy_sample(m_it->second);
  }
  
  for (std::vector<ALLEGRO_SAMPLE_INSTANCE*>::size_type i=0; i<m_instances.size();i++)
  {
    al_destroy_sample_instance(m_instances[i]);
  }
}
  
void
SoundManager::play(
  const SOUNDS soundToPlay
)
{
  switch (soundToPlay)
  {
    case SOUNDS::HIT:
      al_play_sample_instance(m_instances[0]);
      break;
  }
}