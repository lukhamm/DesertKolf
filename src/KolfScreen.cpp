/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#include "../include/KolfScreen.hpp"

#include <allegro5/allegro_primitives.h>

#include <lugame/utils/EventManager.hpp>
#include <lugame/utils/EventCodes.hpp>
#include <lugame/utils/InputManager.hpp>

#include <lupriv/ReadWriteFiles.hpp>

#include "../include/LevelManager.hpp"
#include "../include/ScreenManager.hpp"


constexpr uint32_t eventId_nextLevelButton_released{
  lugame::SDBMCalculator<28>::CalculateValue("nextLevelButton_releasedLeft")
};
constexpr uint32_t eventId_returnButton_released{
  lugame::SDBMCalculator<25>::CalculateValue("returnButton_releasedLeft")
};
constexpr uint32_t eventId_resetButton_released{
  lugame::SDBMCalculator<24>::CalculateValue("resetButton_releasedLeft")
};

KolfScreen::KolfScreen(
  const int screenWidth, 
  const int screenHeight
) 
: GameScreen(screenWidth, screenHeight)
, m_powerArrow()
, stylePrimitive(15)
{
  m_name = "KolfScreen";
  
  std::pair<std::size_t,int>* pairPtr = 
    static_cast<std::pair<std::size_t,int>*>(ScreenManager::getInstance().getData());
  ScreenManager::getInstance().passData(nullptr);
  
  if (pairPtr != nullptr)
  {
    std::size_t levelNr = pairPtr->first;
    int hits = pairPtr->second;
    m_levels.setLevel(levelNr, hits);
    delete pairPtr;
  }
}


KolfScreen::~KolfScreen(
)
{
};


void 
KolfScreen::loadContent(
)
{
  lugame::EventManager::getInstance().attachEvent(lugame::eventid::mouseClickLeft, 
                                                  *static_cast<EventHandler*>(this));
  lugame::EventManager::getInstance().attachEvent(lugame::eventid::mouseReleaseLeft, 
                                                  *static_cast<EventHandler*>(this));
  m_powerArrow.loadContent();
  lulib::math::Matrix<float> ballStartPos = m_levels.getStartingPosition();
  m_ballPtr = new Ball(m_screenWidth, m_screenHeight, ballStartPos[0], ballStartPos[1]);
  m_ballPtr->loadContent();
  
  /*
   * Create Menu
   */
  m_box.insert(&m_returnButton);
  m_box.insert(&m_resetButton);
//   m_box.insert(&m_nextLevelButton);
  
  lugame::EventManager::getInstance().attachEvent(eventId_nextLevelButton_released, 
                                                  *static_cast<EventHandler*>(this));
  lugame::EventManager::getInstance().attachEvent(eventId_returnButton_released, 
                                                  *static_cast<EventHandler*>(this));
  lugame::EventManager::getInstance().attachEvent(eventId_resetButton_released, 
                                                  *static_cast<EventHandler*>(this));
}

void 
KolfScreen::unloadContent(
)
{
  lugame::EventManager::getInstance().detachEvent(eventId_nextLevelButton_released, 
                                                  *static_cast<EventHandler*>(this));
  lugame::EventManager::getInstance().detachEvent(eventId_returnButton_released, 
                                                  *static_cast<EventHandler*>(this));
  lugame::EventManager::getInstance().detachEvent(eventId_resetButton_released, 
                                                  *static_cast<EventHandler*>(this));
  
  lugame::EventManager::getInstance().detachEvent(lugame::eventid::mouseClickLeft, 
                                                  *static_cast<EventHandler*>(this));
  lugame::EventManager::getInstance().detachEvent(lugame::eventid::mouseReleaseLeft, 
                                                  *static_cast<EventHandler*>(this));
  
  if (m_ballPtr != nullptr)
    delete m_ballPtr;
}

void 
KolfScreen::draw(
  ALLEGRO_DISPLAY* /*display*/
)
{
  m_levels.drawLevel();
  
  m_powerArrow.draw();
  m_ballPtr->draw();
  
  m_box.draw();
}


void 
KolfScreen::update(
  ALLEGRO_EVENT ev
)
{
  m_powerArrow.update(ev);
  m_ballPtr->update(ev, &m_levels);
  if (m_ballPtr->isHoled())
    nextLevel();
  
  lugame::InputManager inputManager;
  if (inputManager.isKeyPressed(ev, ALLEGRO_KEY_I))
  {
    m_debugMode = not m_debugMode;
    if (m_debugMode)
    {
      m_box.insert(&m_nextLevelButton);
    }
    else
    {
      m_box.erase("nextLevelButton");
    }
  }
  
}

void
KolfScreen::handleEvent(
  const lugame::Event* event
)
{
  if (event->getId() == lugame::eventid::mouseClickLeft)
  {
    m_mouseActive = true;
  }
  else if (event->getId() == lugame::eventid::mouseReleaseLeft)
  {
    m_mouseActive = false;
  }
  else if (event->getId() == eventId_nextLevelButton_released and m_debugMode)
  {
    nextLevel();
  }
  else if (event->getId() == eventId_returnButton_released)
  {
    actionOnExit();
    ScreenManager::getInstance().changeScreen("TitleScreen");
  }
  else if (event->getId() == eventId_resetButton_released)
  {
    lulib::math::Matrix<float> startingPos = m_levels.getStartingPosition();
    m_ballPtr->reset(startingPos[0], startingPos[1]);
  }
}


void
KolfScreen::nextLevel(
)
{
  if (m_levels.nextLevel() == m_levels.nrOfLevels())
  {
    ScreenManager::getInstance().passData(new int(m_levels.getHits()));
    ScreenManager::getInstance().changeScreen("FinishScreen");
  }
  else
  {
    lulib::math::Matrix<float> startingPos = m_levels.getStartingPosition();
    m_ballPtr->reset(startingPos[0], startingPos[1]);
  }
}


void
KolfScreen::actionOnExit(
)
{
  // Save vector
  lulib::ReadWrite rw;
  lulib::io::BinaryFileIO data;
  
  data.write(m_levels.getCurrentLevel());
  data.write(m_levels.getTotalHits());
  
  std::string saveFilename = m_fileManager.getPathString(FileManager::FOLDER::SAVEGAMES)
                             + "save.dks";
  rw.writeData(saveFilename, data);
}