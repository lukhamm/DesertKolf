/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#include "../include/Level.hpp"

#include <allegro5/allegro_primitives.h>

#include <iostream>
#include <sstream>
#include <algorithm>

#include <lupriv/InputFileParser.hpp>

#include <lugame/engines2d/Collision.hpp>

Level::Level(
  const std::string& filename,
  const int levelNr,
  const int levelWidth,
  const int levelHeight
)
: m_levelNr(levelNr+1)
, m_levelWidth(levelWidth)
, m_levelHeight(levelHeight)
{
  /*
   * Read Level File
   */
  lulib::InputFileParser fileParser;
  fileParser.registerKeyword("level_nr");
//   fileParser.registerKeyword("lines");
  fileParser.registerKeyword("hole");
  fileParser.registerKeyword("drive");
  fileParser.registerKeyword("points");
  
  fileParser.readFile(filename);
  
//   m_levelNr = std::stoi(fileParser.getValues("level_nr"));

  
  /*
   * Read Lines and Create Vertices for Elevation Drawing
   */
  std::stringstream ss = fileParser.getValuesStream("points");
  ss >> m_pointNr;
  std::size_t pointsSize = static_cast<std::size_t>(m_pointNr);
  m_points.resize(pointsSize);
  std::vector<float> verticesX;
  std::vector<float> verticesY;
  for (std::size_t i=0; i<pointsSize; i++)
  {
    ss >> m_points[i].x >> m_points[i].y;
    if (ss.fail())
      throw std::runtime_error("Error while reading Level: " + filename);
    
    verticesX.push_back(m_points[i].x);
    verticesY.push_back(m_points[i].y);
  }
  
  /* 
   * Create Vertices for Drawing Background
   * They need to be arranged counter clockwise!
   * So we have to reorder them accordingly
   */
  std::reverse(verticesX.begin(), verticesX.end());
  std::reverse(verticesY.begin(), verticesY.end());
  
  m_verticesArrayEntries = verticesX.size()+verticesY.size()+4;
  m_verticesPtr = new float[m_verticesArrayEntries];  
  
  for (std::size_t i=0; i<verticesX.size(); i++)
  {
    m_verticesPtr[i*2] = verticesX[i];
    m_verticesPtr[i*2+1] = verticesY[i];
  }
  m_verticesPtr[m_verticesArrayEntries-4] = 0;
  m_verticesPtr[m_verticesArrayEntries-3] = m_levelHeight;
  m_verticesPtr[m_verticesArrayEntries-2] = m_levelWidth;
  m_verticesPtr[m_verticesArrayEntries-1] = m_levelHeight;
  
  
  /*
   * Load Hole
   */
  ss.str("");
  ss.clear();
  ss = fileParser.getValuesStream("hole");
  float offset[2];
  ss >> offset[0] >> offset[1];
  if (ss.fail())
  {
    throw std::runtime_error("Couldn't read offset from level input file.");
  }
  
  /*
   * Load Drive
   */
  ss.str("");
  ss.clear();
  ss = fileParser.getValuesStream("drive");
  float drivePos[2];
  ss >> drivePos[0] >> drivePos[1];
  if (ss.fail())
  {
    throw std::runtime_error("Couldn't read drive position from level input file.");
  }
  
  /*
   * Create Lines From Points
   */
  m_lines.clear();
  for (std::size_t i=0; i<m_points.size()-1; i++)
  {
    if (m_points[i].x != offset[0] or m_points[i].y != offset[1])
    {
      m_lines.push_back(LineType{m_points[i].x, m_points[i].y, m_points[i+1].x, m_points[i+1].y});
    }
  }
  
  /*
   * adds the hole, the drive and their boundary lines
   */
  m_holePtr  = new Hole(offset[0], offset[1], m_lines, m_levelNr);
  m_drivePtr = new Drive(drivePos[0], drivePos[1], m_lines);
  
}



Level::~Level(
)
{
  if (m_verticesPtr != nullptr)
    delete[] m_verticesPtr;
  
  if (m_holePtr != nullptr)
    delete m_holePtr;
}


void
Level::draw(
)
{
  al_draw_filled_polygon(m_verticesPtr, 
                         m_verticesArrayEntries/2,
                         al_map_rgb(202,108,70));
  
  // needs to be at the end to drawn in front
  m_holePtr->draw();
  m_drivePtr->draw();
}




bool
Level::checkCollisions(
  const lulib::math::Matrix<float>& pos,
  const float radius,
  lulib::math::Matrix<float>& offset
)
{
  bool isCollision{false};
  lulib::math::Matrix<float> tmpOffset(2);
  lulib::math::Matrix<float> shift(2);
  
  // Collision is checked here. Since it can happen that there are more than one
  //   collisions active, all of them have to be regarded. That is why the offset
  //   (put object to non collision position) has to be subtracted by the position
  //   to get the shift due to the one collision. All shifts together added to the
  //   original position yield the new position.
  for (const auto& line : m_lines)
  {
    if(lugame::collision::lineSegCircleOffset(line.x1, line.y1, line.x2, line.y2, 
                                              pos[0], pos[1], radius, tmpOffset))
    {
      isCollision = true;
      shift = shift + (tmpOffset - pos);
    }
  }
  
  if (isCollision)
    offset = pos + shift;
  
  return isCollision;
}



bool
Level::isBallHoled(
  const lulib::math::Matrix<float>& pos,
  const float radius
)
{
  return m_holePtr->isBallHoled(pos, radius);
}



lulib::math::Matrix<float>
Level::getStartingPosition(
)
{
  return m_drivePtr->getStartingPosition();
}