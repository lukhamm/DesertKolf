#include "../include/TitleScreen.hpp"

#include <lugame/utils/EventManager.hpp>
/* 
 * This file is part of DesertKolf ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#include <lupriv/ReadWriteFiles.hpp>
#include "../include/ScreenManager.hpp"
#include "../include/EditorScreen.hpp"

#include <iostream>
#include <utility>

TitleScreen::TitleScreen(
  const int screenWidth, 
    const int screenHeight) 
: GameScreen(screenWidth, screenHeight)
{
  m_name = "TitleScreen";
  
  /*
   * Read SaveGame in case it exists
   */
  try
  {
    m_resumeGame = true;
    std::string saveFilename = m_fileManager.getPathString(FileManager::FOLDER::SAVEGAMES)
                             + "save.dks";
    lulib::ReadWrite rw;
    lulib::io::BinaryFileIO binData;
    binData = rw.readData(saveFilename);
    if (binData.size() > 0)
    {
      std::size_t levelNr = binData.read<std::size_t>();
      int nrTotalHits = binData.read<int>();
      m_resumeParametersPtr = new std::pair<std::size_t, int>{levelNr, nrTotalHits};
    }
  }
  catch (...)
  {
    m_resumeGame = false;
  }
  
  const std::string fontPath = m_fileManager.getPathString(FileManager::FOLDER::RESOURCES)
                             + "/Style_Primitive/fonts/bitstream_vera_sans_font_6016/VeraBd.ttf";
  m_titleFont = al_load_ttf_font(fontPath.c_str(), 50, 0);
}

TitleScreen::~TitleScreen(
)
{
}

void 
TitleScreen::loadContent(
)
{
  if (m_resumeGame)
  {
    box.insert(&resumeButton);
    lugame::EventManager::getInstance().attachEvent(lugame::SDBMHash("resumeButton_releasedLeft"), 
                                                  *static_cast<EventHandler*>(this));
  }
  
  box.insert(&startButton);
  box.insert(&editorButton);
  box.insert(&quitButton);
  lugame::EventManager::getInstance().attachEvent(lugame::SDBMHash("quitButton_clickedLeft"), 
                                                  *static_cast<EventHandler*>(this));
  lugame::EventManager::getInstance().attachEvent(lugame::SDBMHash("startButton_clickedLeft"), 
                                                  *static_cast<EventHandler*>(this));
  lugame::EventManager::getInstance().attachEvent(lugame::SDBMHash("startButton_releasedLeft"), 
                                                  *static_cast<EventHandler*>(this));
  lugame::EventManager::getInstance().attachEvent(lugame::SDBMHash("editorButton_releasedLeft"), 
                                                  *static_cast<EventHandler*>(this));
  
}

void 
TitleScreen::unloadContent(
)
{
  lugame::EventManager::getInstance().detachEvent(lugame::SDBMHash("quitButton_clickedLeft"), 
                                                  *static_cast<EventHandler*>(this));
  lugame::EventManager::getInstance().detachEvent(lugame::SDBMHash("startButton_clickedLeft"), 
                                                  *static_cast<EventHandler*>(this));
  lugame::EventManager::getInstance().detachEvent(lugame::SDBMHash("startButton_releasedLeft"), 
                                                  *static_cast<EventHandler*>(this));
  lugame::EventManager::getInstance().detachEvent(lugame::SDBMHash("editorButton_releasedLeft"), 
                                                  *static_cast<EventHandler*>(this));
  if (m_resumeGame)
  {
    lugame::EventManager::getInstance().detachEvent(lugame::SDBMHash("resumeButton_releasedLeft"), 
                                                  *static_cast<EventHandler*>(this));
  }
}

void 
TitleScreen::draw(
  ALLEGRO_DISPLAY* /*display*/
)
{
  al_draw_text(m_titleFont, al_map_rgb(255,255,255), m_screenWidth/2.f, 40, ALLEGRO_ALIGN_CENTRE, "DesertKolf");
  box.draw();
}

void 
TitleScreen::update(
  ALLEGRO_EVENT /*ev*/
)
{
}

void
TitleScreen::handleEvent(
  const lugame::Event* event
)
{
  if (event->getId() == lugame::SDBMHash("quitButton_clickedLeft"))
  {
    ScreenManager::getInstance().done = true;
  }
  
  if (event->getId() == lugame::SDBMHash("startButton_clickedLeft"))
  {
  }
  
  if (event->getId() == lugame::SDBMHash("resumeButton_releasedLeft"))
  {
    ScreenManager::getInstance().passData(m_resumeParametersPtr);
    m_resumeParametersPtr = nullptr;
    ScreenManager::getInstance().changeScreen("KolfScreen");
  }
  
  if (event->getId() == lugame::SDBMHash("startButton_releasedLeft"))
  {
    ScreenManager::getInstance().changeScreen("KolfScreen");
  }
  
  if (event->getId() == lugame::SDBMHash("editorButton_releasedLeft"))
  {
    ScreenManager::getInstance().changeScreen("EditorScreen");
  }
}