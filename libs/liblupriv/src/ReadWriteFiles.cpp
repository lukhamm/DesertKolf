/* 
 * This file is part of liblupriv ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#include "lupriv/ReadWriteFiles.hpp"

#include <fstream>
#include "lupriv/BinaryFileIO.hpp"

std::vector<std::string> 
lulib::ReadWrite::readAllLines(
  const std::string& filename
) const
{
  std::vector<std::string> line_list;
  
  std::ifstream inFile;
  inFile.open(filename.c_str());
  if (inFile.fail())
  {
    throw std::runtime_error(
      "Couldn't load file: " 
      + filename 
      + ". Is it there? Did you provide the correct Name?"
    );
  }
  else
  {
    std::string line;
    
    while(std::getline(inFile, line))
    {
      line_list.push_back(line);
    }
  }

  if (inFile.is_open())
    inFile.close();
  
  return line_list;
}


lulib::io::BinaryFileIO
lulib::ReadWrite::readData(
  const std::string& filename
) const
{
  std::vector<char> dataAsChars;
  
  std::ifstream inFile;
  inFile.open(filename.c_str());
  if (inFile.fail())
  {
    throw std::runtime_error(
      "Couldn't load file: " 
      + filename
      + ". Does it exist?"
    );
  }
  else
  {
    char c = inFile.get();
    while (inFile.good())
    {
      dataAsChars.push_back(c);
      c = inFile.get();
    }
  }
  
  lulib::io::BinaryFileIO binData;
  binData.setData(dataAsChars);
  
  return binData;
}


void
lulib::ReadWrite::writeData(
  const std::string& filename,
  const std::vector<char>& data
) const
{
  std::ofstream filestream;
  filestream.open(filename, std::ios::out | std::ios::trunc );
  if (!filestream)
  {
    throw std::runtime_error("Trying to open " + filename + " for writing, failed.");
  }

  for (std::size_t i=0; i<data.size(); i++)
  {
    filestream << data[i];
    if (filestream.fail()) throw std::runtime_error("Error while writing to file: " + filename);
  }
}


void
lulib::ReadWrite::writeData(
  const std::string& filename,
  const io::BinaryFileIO& binFile
) const
{
  writeData(filename, binFile.getData());
}