/* 
 * This file is part of liblupriv ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

/*
 * lulib::string::
 * 
 * -- contains functions for std::string manipulation
 * 
 * Requirements:
 * -- std=c++11
 * 
 * For more details, see header-file
 * 
 */

#include "lupriv/StringManipulation.hpp"

#include <sstream>
#include <algorithm>


std::string 
lulib::string::trim(
  const std::string& inString,
  const std::string& replaceCharacter
)
{
  // find_first_... accepts a list of characters as a std::string
  auto stringBegin = inString.find_first_not_of(replaceCharacter);
  if (stringBegin == std::string::npos) 
    stringBegin = 0;

  auto stringEnd = inString.find_last_not_of(replaceCharacter);
  if (stringEnd == std::string::npos) 
    stringEnd = inString.length();

  return inString.substr(stringBegin, stringEnd - stringBegin + 1);
}



std::string& 
lulib::string::trimThis(
  std::string& inString,
  const std::string& replaceCharacter
)
{
  // find_first_... accepts a list of characters as a std::string
  auto stringBegin = inString.find_first_not_of(replaceCharacter);
  if (stringBegin == std::string::npos) 
    stringBegin = 0;

  auto stringEnd = inString.find_last_not_of(replaceCharacter);
  if (stringEnd == std::string::npos) 
    stringEnd = inString.length();

  inString = inString.substr(stringBegin, stringEnd - stringBegin + 1);
  return inString;
}




std::string 
lulib::string::reduce(
  const std::string& inString, 
  const std::string& fillString, 
  const std::string& replaceCharacter
)
{
  std::string tmpString = trim(inString, replaceCharacter);
  
  auto stringBegin = tmpString.find_first_of(replaceCharacter);
  
  while (stringBegin != std::string::npos){
    const auto stringEnd = tmpString.find_first_not_of(replaceCharacter, stringBegin);
    const auto range = stringEnd - stringBegin;
    
    tmpString.replace(stringBegin, range, fillString);
    
    const auto newStart = stringBegin + fillString.length();
    stringBegin = tmpString.find_first_of(replaceCharacter, newStart);
  }
  return tmpString;
}



std::string& 
lulib::string::reduceThis(
  std::string& inString, 
  const std::string& fillString, 
  const std::string& replaceCharacter
)
{
  inString = trim(inString, replaceCharacter);
  
  auto stringBegin = inString.find_first_of(replaceCharacter);
  
  while (stringBegin != std::string::npos){
    const auto stringEnd = inString.find_first_not_of(replaceCharacter, stringBegin);
    const auto range = stringEnd - stringBegin;
    
    inString.replace(stringBegin, range, fillString);
    
    const auto newStart = stringBegin + fillString.length();
    stringBegin = inString.find_first_of(replaceCharacter, newStart);
  }
  return inString;
}




std::string 
lulib::string::replace(
  const std::string& inString, 
  const std::string& oldSubstring, 
  const std::string& newSubstring
)
{
  std::string tmpString = inString;
  auto substringPos = tmpString.find(oldSubstring);
  while (substringPos != std::string::npos)
  {
    tmpString.replace(substringPos, oldSubstring.length(), newSubstring);
    // searches again from the end where substring was inserted.
    substringPos = tmpString.find(oldSubstring, substringPos+oldSubstring.length());
  }
  return tmpString;
}
std::string 
lulib::string::replace(
  std::string&& inString, 
  const std::string& oldSubstring, 
  const std::string& newSubstring
)
{
  std::string tmpString;
  std::swap(tmpString,inString);
  auto substringPos = tmpString.find(oldSubstring);
  while (substringPos != std::string::npos)
  {
    tmpString.replace(substringPos, oldSubstring.length(), newSubstring);
    // searches again from the end where substring was inserted.
    substringPos = tmpString.find(oldSubstring, substringPos+oldSubstring.length());
  }
  return tmpString;
}
std::string&
lulib::string::replaceThis(
  std::string& inString, 
  const std::string& oldSubstring, 
  const std::string& newSubstring
)
{
  auto substringPos = inString.find(oldSubstring);
  while (substringPos != std::string::npos)
  {
    inString.replace(substringPos, oldSubstring.length(), newSubstring);
    // searches again from the end where substring was inserted.
    substringPos = inString.find(oldSubstring, substringPos+oldSubstring.length());
  }
  return inString;
}



std::vector<std::string> 
lulib::string::split(
  const std::string& inString,
  const std::string& delimiter
)
{
  std::stringstream ss;
  ss << reduce(trim(inString, delimiter), delimiter);
  std::vector<std::string> returnStrings;
  std::string tmpLine;
  while (std::getline(ss, tmpLine, delimiter[0]))
  {
    returnStrings.push_back(tmpLine);
  }
  return returnStrings;
}



std::vector<std::string> 
lulib::string::splitEsc(
  const std::string& inString,
  const char delimiter,
  const char escChar
)
{
  std::vector<std::string> returnStrings;
  std::string::size_type argPos    = 0
                       , printPos  = 0
                       , searchPos = 0;
  
  while (searchPos != std::string::npos)
  {
    // ignore find if escape sequence was found
    argPos = inString.find_first_of(delimiter, searchPos);
    if (inString[argPos-1] == escChar)
    {
      searchPos = argPos+1;
      continue;
    }
    
    // add to list of splitted strings
    returnStrings.push_back(trim(inString.substr(printPos,argPos-printPos)));

    // update searchPosition
    if (argPos == std::string::npos)
      searchPos = std::string::npos;
    else
      searchPos = argPos+1;
    
    // update print position
    printPos = searchPos;
  }
  
  return returnStrings;
}



char 
lulib::string::charToLower(
  char in
)
{
  if(in<='Z' && in>='A')
    return in-('Z'-'z');
  return in;
}



std::string 
lulib::string::toLower(
  const std::string& inString
)
{
  std::string returnString = inString;
  std::transform(returnString.begin(), returnString.end(), returnString.begin(), ::tolower);
  return returnString;
}
std::string 
lulib::string::toLower(
  std::string&& inString
)
{
  std::string returnString;
  std::swap(returnString, inString);
  std::transform(returnString.begin(), returnString.end(), returnString.begin(), ::tolower);
  return returnString;
}
std::string& 
lulib::string::toLowerThis(
  std::string& inString
)
{
  std::transform(inString.begin(), inString.end(), inString.begin(), ::tolower);
  return inString;
}