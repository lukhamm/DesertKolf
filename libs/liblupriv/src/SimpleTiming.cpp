/* 
 * This file is part of liblupriv ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#include <lupriv/SimpleTiming.hpp>


namespace lulib {
  namespace utils {
    
    
    std::ostream& 
    operator<<(
      std::ostream& os, const SimpleTiming& timing
    )
    {
      os << std::chrono::duration<double> (timing.m_end-timing.m_start).count();
      return os;
    };
    
    
  }
}
// operator<< overload for string streaming
