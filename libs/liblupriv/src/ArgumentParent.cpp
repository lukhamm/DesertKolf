/* 
 * This file is part of liblupriv ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#include "lupriv/argutil/ArgumentParent.hpp"

#include <stdexcept>

void 
lulib::argutil::ArgumentParent::setArgumentId(
  std::string argumentId
)
{
  m_id = argumentId;
}



const std::string& 
lulib::argutil::ArgumentParent::getArgumentId(
) const
{
  return m_id;
}



void 
lulib::argutil::ArgumentParent::setArgname(
  std::string argname1, 
  std::string argname2
)
{
  // checks which of the 2 provided arguments starts with '-' and which with '--'
  // sets the argnameLong and argnameShort accordingly
  
  if (argname1 == argname2) 
  {
    throw std::invalid_argument(
      "Strings submitted to 'setArgname' are equal.");
  }
  
  
  if (
    argname1[0] == '-' and argname1[1] == '-' and
    (argname2 == "" or (argname2[0] == '-' and argname2[1] != '-') )
  )
  {
    m_argnameLong  = argname1;
    m_argnameShort = argname2;
  }
  else if (
    argname2[0] == '-' and argname2[1] == '-' and
    (argname1 == "" or (argname1[0] == '-' and argname1[1] != '-') )
  )
  {
    m_argnameLong  = argname2;
    m_argnameShort = argname1;
  }
  else if (
    argname1[0] == '-' and argname1[1] != '-' and
    (argname2 == "" or (argname2[0] == '-' and argname2[1] == '-') )
  )
  {
    m_argnameLong  = argname2;
    m_argnameShort = argname1;
  }
  else if (
    argname2[0] == '-' and argname2[1] != '-' and
    (argname1 == "" or (argname1[0] == '-' and argname1[1] == '-') )
  )
  {
    m_argnameLong  = argname1;
    m_argnameShort = argname2;
  }
  else if ( argname2 == "" and argname1[0] != '-' )
  {
    m_argname = argname1;
  }
  else
  {
    throw std::invalid_argument(
      "Strings '" + argname1 + "' and '" + argname2 + "' submitted to 'setArgname' are invalid.");
  }
  
  setArgumentId(m_argnameShort+m_argnameLong+m_argname);
  
}



void 
lulib::argutil::ArgumentParent::setHelptext(
  std::string helpTxt
)
{
  m_helptxt = helpTxt;
}


void
lulib::argutil::ArgumentParent::setPositional(
  const bool positional
)
{
  m_positional = positional;
}


const std::string& 
lulib::argutil::ArgumentParent::getArgnameLong(
) const
{
  return m_argnameLong;
}



const std::string& 
lulib::argutil::ArgumentParent::getArgnameShort(
) const
{
  return m_argnameShort;
}


const std::string& 
lulib::argutil::ArgumentParent::getArgname(
) const
{
  return m_argname;
}

const std::string& 
lulib::argutil::ArgumentParent::getAName(
) const
{
  if (m_argname != "")
    return m_argname;
  else if (m_argnameShort != "")
    return m_argnameShort;
  else 
    return m_argnameLong;
}


const std::string& 
lulib::argutil::ArgumentParent::getHelpText(
) const
{
  return m_helptxt;
}



const lulib::argutil::ArgumentParent::TYPE&
lulib::argutil::ArgumentParent::getType(
) const
{
  return m_type;
}


const bool&
  lulib::argutil::ArgumentParent::isUserInput(
) const
{
  return m_on;
}

const bool&
  lulib::argutil::ArgumentParent::isPositional(
) const
{
  return m_positional;
}

void 
lulib::argutil::ArgumentParent::turnOn(
)
{
  m_on = true;
}

void 
lulib::argutil::ArgumentParent::turnOff(
)
{
  m_on = false;
}


unsigned int 
lulib::argutil::ArgumentParent::getNPar(
)
{
  return m_nPar;
}
void 
lulib::argutil::ArgumentParent::setNPar(
  const unsigned int npar
)
{
  m_nPar = npar;
}


void
lulib::argutil::ArgumentParent::setType(
  const lulib::argutil::ArgumentParent::TYPE type
)
{
  m_type = type;
}


unsigned int 
lulib::argutil::ArgumentParent::getPosNo(
)
{
  return m_posNo;
}

void 
lulib::argutil::ArgumentParent::setPosNo(
  const unsigned int n
)
{
  m_posNo = n;
}
