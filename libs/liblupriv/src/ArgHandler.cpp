/* 
 * This file is part of liblupriv ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#include "lupriv/ArgHandler.hpp"
#include "lupriv/StringManipulation.hpp"

#include <iostream>
#include <sstream>
#include <iomanip>


lulib::ArgHandler::ArgHandler()
{
  registerArg("short=h; long=help; type=bool; help=show this help message and exit");
}


void 
lulib::ArgHandler::registerArg(
  const std::string& argumentInputString
)
{
  ArgumentParentPtr argPtr = deduceArg(argumentInputString);
  
  // check if argument exists in the maps 
  //   (would be a double registration)
  if (m_args.find(argPtr->getArgumentId()) == m_args.end())
  {
    m_args[argPtr->getArgumentId()] = argPtr;
  }
  else
  {
    throw std::invalid_argument("Found the same argument twice! Check your code!");
  }
  
  if (m_longArgs.find(argPtr->getArgumentId()) == m_longArgs.end())
  {
    m_longArgs[argPtr->getArgnameLong()] = argPtr;
  }
  else
  {
    throw std::invalid_argument("Found the same argument (long_name) twice! Check your code!");
  }
  
  if (m_shortArgs.find(argPtr->getArgumentId()) == m_shortArgs.end())
  {
    m_shortArgs[argPtr->getArgnameShort()] = argPtr;
  }
  else
  {
    throw std::invalid_argument("Found the same argument (short_name) twice! Check your code!");
  }
  
  if (m_nameArgs.find(argPtr->getArgumentId()) == m_nameArgs.end() and argPtr )
  {
    m_nameArgs[argPtr->getArgname()] = argPtr;
  }
  else
  {
    throw std::invalid_argument("Found the same positional argument twice! Check your code!");
  }
  
}


namespace lulib {
namespace argutil {

template <typename T>
static std::vector<T> readValuesFromString(
  const std::string& parValueStr,
  unsigned int nPar
)
{
  std::stringstream ss;
  ss << parValueStr;
  std::vector<T> values;
  for (unsigned int i=0; i<nPar; i++)
  {
    T tmpValue;
    ss >> tmpValue;
    if (ss.fail())
      throw std::invalid_argument(
        "Not enough or invalid values as default parameters given.");
    values.push_back(tmpValue);
  }
  return (values);
//   rawArgPtr->setValues(values);
}

}
}


std::shared_ptr<lulib::argutil::ArgumentParent>
lulib::ArgHandler::deduceArg(
  const std::string& argString
)
{
  std::string escapeCharacter = "$";                             // could be changed easily
  const char delimiter = ';';
  
  
  // some default values, if nothing else is given:
  std::string argName_long  = "";
  std::string argName_short = "";
  std::string argName       = "";
  std::string helpTxt       = "";
  std::string parValueStr   = "";
  argutil::ArgumentParent::TYPE parameterType = argutil::ArgumentParent::TYPE::BOOL;
  unsigned int nPar = 0;
  bool defaultValuesGiven = false;
  
  
  // register keywords
  std::vector<std::string> keywords;
  keywords.push_back("short");                          // signals '-' argument
  keywords.push_back("long");                           // signals '--' argument
  keywords.push_back("type");
  keywords.push_back("help");
  keywords.push_back("n_par");
  keywords.push_back("name");
  
  
  // split argString into all arguments
  std::vector<std::string> argStrs = string::splitEsc(argString, 
                                                      delimiter, 
                                                      escapeCharacter[0]);
  
  
  // check for keywords
  for (const std::string& keyword : argStrs)
  {
    std::string keyword_lower = string::toLower(keyword);
    
    // keyword: short
    if (keyword_lower.substr(0,keywords[0].length()) == keywords[0])
    {
      argName_short = "-" + string::trim(
        string::replace(keyword.substr(keywords[0].length()), "=", ""));
    }
    
    // keyword: long
    if (keyword_lower.substr(0,keywords[1].length()) == keywords[1])
    {
      argName_long = "--" + string::trim(
        string::replace(keyword.substr(keywords[1].length()), "=", ""));
    }
    
    // keyword: type
    if (keyword_lower.substr(0,keywords[2].length()) == keywords[2])
    {
      std::string typeStr = string::trim(
        string::replace(keyword.substr(keywords[2].length()), "=", ""));

      if (typeStr == "bool")
      {
        parameterType = argutil::ArgumentParent::TYPE::BOOL;
      }
      else if (typeStr == "string")
      {
        parameterType = argutil::ArgumentParent::TYPE::STR;
      }
      else if (typeStr == "double")
      {
        parameterType = argutil::ArgumentParent::TYPE::DOUBLE;
      }
      else if (typeStr == "int")
      {
        parameterType = argutil::ArgumentParent::TYPE::INT;
      }
      else if (typeStr == "float")
      {
        parameterType = argutil::ArgumentParent::TYPE::FLOAT;
      }
      else if (typeStr == "positional")
      {
        parameterType = argutil::ArgumentParent::TYPE::POSITIONAL;
      }
      else
      {
        throw std::invalid_argument("Wrong 'type=' on creating argument. Check your src-code");
      }
    }
    
    // keyword: help
    if (keyword_lower.substr(0,keywords[3].length()) == keywords[3])
    {
      helpTxt = string::trim(
        string::replace(keyword.substr(keywords[3].length()), "=", ""));
    }
    
    // keyword: n_par
    if (keyword_lower.substr(0,keywords[4].length()) == keywords[4])
    {
      std::stringstream ss;
      if (keyword.find(':') != std::string::npos)
      {
        std::string tmpstr = string::replace(keyword.substr(keywords[4].length()), "=", "");
        ss << string::replace(tmpstr, ":", " ");
        defaultValuesGiven = true;
      }
      else
      {
        ss << string::replace(keyword.substr(keywords[4].length()), "=", "");
      }
      ss >> nPar;
      if (defaultValuesGiven)
      {
        for (unsigned int i = 0; i<nPar; i++)
        {
          std::string tmp;
          ss >> tmp;
          parValueStr = parValueStr + tmp + " ";
        }
      }
      if (ss.fail())
      {
        throw std::invalid_argument("Wrong 'n_par=' parameter. Please check your src-code.");
      }
    }
    
    // keyword: name
    if (keyword_lower.substr(0,keywords[5].length()) == keywords[5])
    {
      argName = string::trim(string::replace(keyword.substr(keywords[5].length()), "=", ""));
    }
  }
  if (argName == "" and argName_short == "" and argName_long == "")
    throw std::invalid_argument("You have to provide at least one argument name for registering"
                                "an argument. You'll have to check your source code!");
  
    
    
  // actual argument and type
  std::shared_ptr<argutil::ArgumentParent> argPtr = nullptr;
  switch (parameterType)
  {
    case(argutil::ArgumentParent::TYPE::BOOL):
    {
      auto rawArgPtr = std::shared_ptr<argutil::Argument<bool>>(new argutil::Argument<bool>);
      if (defaultValuesGiven)
      {
        std::vector<bool> values = argutil::readValuesFromString<bool>(parValueStr, nPar);
        rawArgPtr->setValues(values);
      }
      argPtr = std::static_pointer_cast<argutil::ArgumentParent>(rawArgPtr);
      break;
    }
    case(argutil::ArgumentParent::TYPE::INT):
    {
      auto rawArgPtr = std::shared_ptr<argutil::Argument<int>>(new argutil::Argument<int>);
      if (defaultValuesGiven)
      {
        std::vector<int> values = argutil::readValuesFromString<int>(parValueStr, nPar);
        rawArgPtr->setValues(values);
      }
      argPtr = std::static_pointer_cast<argutil::ArgumentParent>(rawArgPtr);
      break;
    }
    case(argutil::ArgumentParent::TYPE::FLOAT):
    {
      auto rawArgPtr = std::shared_ptr<argutil::Argument<float>>(new argutil::Argument<float>);
      if (defaultValuesGiven)
      {
        std::vector<float> values = argutil::readValuesFromString<float>(parValueStr, nPar);;
        rawArgPtr->setValues(values);
      }
      argPtr = std::static_pointer_cast<argutil::ArgumentParent>(rawArgPtr);
      break;
    }
    case(argutil::ArgumentParent::TYPE::DOUBLE):
    {
      auto rawArgPtr = std::shared_ptr<argutil::Argument<double>>(new argutil::Argument<double>);
      if (defaultValuesGiven)
      {
        std::vector<double> values = argutil::readValuesFromString<double>(parValueStr, nPar);;
        rawArgPtr->setValues(values);
      }
      argPtr = std::static_pointer_cast<argutil::ArgumentParent>(rawArgPtr);
      break;
    }
    case(argutil::ArgumentParent::TYPE::STR):
    {
      auto rawArgPtr = std::shared_ptr<argutil::Argument<std::string>>(
        new argutil::Argument<std::string>);
      if (defaultValuesGiven)
      {
        std::vector<std::string> values = argutil::readValuesFromString<std::string>(parValueStr, nPar);;
        rawArgPtr->setValues(values);
      }
      argPtr = std::static_pointer_cast<argutil::ArgumentParent>(rawArgPtr);
      break;
    }
    case(argutil::ArgumentParent::TYPE::POSITIONAL):
    {
      if (argName_short != "" and argName_long != "")
      {
        throw std::invalid_argument("A positional type can't be combined with long and short names!"
                                    "You'll have to check your source code!");
      }
      auto rawArgPtr = std::shared_ptr<argutil::Argument<std::string>>(
        new argutil::Argument<std::string>);
      argPtr = std::static_pointer_cast<argutil::ArgumentParent>(rawArgPtr);
      break;
    }
    default:
    {
      throw std::invalid_argument("parameterType is not a valid type... Check source code.");
      break;
    }
    
  }
  
  // setting values to argument
  argPtr->setType(parameterType);
  argPtr->setNPar(nPar);
  argPtr->setHelptext(string::replace(helpTxt, escapeCharacter, ""));
  
  if (parameterType == argutil::ArgumentParent::TYPE::POSITIONAL)
  {
    argPtr->setArgname(argName);
    argPtr->setPosNo(m_nPosArgCounter);
    m_nPosArgCounter++;
  }
  else 
  {
    argPtr->setArgname(argName_short, argName_long);
  }

  // checks if type is not BOOL 
  // and there aren't any default parameters provided
  // --> turns it into a positional argument
  if (parameterType != argutil::ArgumentParent::TYPE::BOOL and not defaultValuesGiven)
  {
    argPtr->setPositional(true);
    m_nPositionalArg++;
  }
  
  // +=== TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST ===
//   std::cout << "THIS IS FOR TEST PURPOSES:: " << std::endl;
//   std::cout << "argName       = " << argPtr->getArgname() << std::endl;
//   std::cout << "argName_short = " << argPtr->getArgnameShort() << std::endl;
//   std::cout << "argName_long  = " << argPtr->getArgnameLong() << std::endl;
//   std::cout << "argument ID   = " << argPtr->getArgumentId() << std::endl;
//   std::cout << "type          = " << static_cast<int>(parameterType) << std::endl;
//   std::cout << "help          = " << string::replace(argPtr->getHelpText(), "$", "")  << std::endl;
//   std::cout << "no of paramet = " << argPtr->getNPar() << std::endl;
//   std::cout << "values        = " << parValueStr << std::endl;
//   if (argPtr->isPositional())
//     std::cout << "positional     = true" << std::endl;
//   else
//     std::cout << "positional     = false" << std::endl;
  // +=== TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST ===
 
  return argPtr;
}

std::vector<std::string>
lulib::ArgHandler::readUserArgs(
  const int user_argc, char* user_argv[]
)
{
  std::vector<std::string> commandLineArgs;
  for (int i=0; i<user_argc; i++)
  {
    commandLineArgs.push_back(string::trim(std::string(user_argv[i])));
  }
  
  // +=== TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST ===
//   std::cout << "Command Line Args: " << std::endl;
//   for (auto bla : commandLineArgs)
//   {
//     std::cout << bla << std::endl;
//   }
  // +=== TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST ===
  
  return commandLineArgs;
}



void 
lulib::ArgHandler::parseArgs(
  const int user_argc,
  char* user_argv[]
)
{
  std::vector<std::string> userArgs = readUserArgs(user_argc, user_argv);
  
  
  // check for help user argument
  if ( isUsrArgPresent("-h", "--help", userArgs) )
  {
    printHelp();
  }
  if ( isUsrArgPresent("-v", "--version", userArgs) and 
       m_version != "" )
  {
    printVersion();
  }
  if ( isUsrArgPresent("-l", "--license", userArgs) and 
       m_license != "" )
  {
    printLicense();
  }
  
  // check if there are no arguments but positional arguments
  if (userArgs.size()-1 < m_nPositionalArg)
  {
    std::stringstream ss;
    ss << m_nPositionalArg;
    throw argutil::ArgError("Error! Not enough arguments. Expecting " 
                            + ss.str() 
                            + " positional arguments! \n"
                            "See -h,--help for help.");
  }

  
  // check if user arguments are unique
  std::vector<std::string> rawArgs;
  for (const auto& s : userArgs)
  {
    if (s[0] == '-')
    {
      rawArgs.push_back(s);
    }
  }
  if (not hasUniqueElements<std::string>(rawArgs))
    throw argutil::ArgError("Error! Found argument duplicate on command line!");
  
  
  // check if user arguments are registered
  // if they are, turn them on
  // if they are on already, throw error
  for (const auto& s : rawArgs)
  {
    ArgumentParentPtr argPtr = isArgRegistered(s);
    if (argPtr == nullptr)
    {
      throw argutil::ArgError("Error! Invalid command line argument: '" 
                              + s 
                              +  "'! \nPlease refer to -h,--help."
                             );
    }
    else
    {
      if (argPtr->isUserInput())
      {
        throw argutil::ArgError("Error! Invalid use of the same arguments! Please see -h,--help.");
      }
      else
      {
        argPtr->turnOn();
      }
    }
  }
  
  
  // check if user arguments have the right number of
  // parameters and throw if not
  updateParameter(userArgs);
  
  
  // check if all '-' and '--' arguments that are also
  //   positional were presented by the user
  for (const auto& p : m_args)
  {
    if ( p.second->isPositional()
        and (p.second->getType() != argutil::ArgumentParent::TYPE::POSITIONAL)
        and (not p.second->isUserInput())
       )
      throw argutil::ArgError("Argument '" 
                             + p.second->getAName()
                             +"' is positional and needs to be given parameter values.");
  }
  
  
  // delete all arguments that have been resolved
  // arguments that are left over should be positional
  for ( auto it = userArgs.cbegin(); it != userArgs.cend();)
  {
    if ( (*it)[0] == '-')
    {
      ArgumentParentPtr argPtr = getArgument(*it);
      userArgs.erase( it, it+argPtr->getNPar()+1 );
    }
    else
    {
      ++it;
    }
  }
  userArgs.erase(userArgs.begin());

  
  // check if there are enough arguments left over to assing to positional arguments
  if (userArgs.size() < m_nPosArgCounter)
  {
    std::stringstream ss;
    ss << m_nPosArgCounter-userArgs.size();
    throw argutil::ArgError("Error! Insufficient number of positional parameter. \n"
                            " Missing "
                            + ss.str() 
                            + " positional parameter.");
  }
  
  
  // assign remaining arguments to registered positional arguments
  for (auto p : m_args)
  {
    if (p.second->getType() == argutil::ArgumentParent::TYPE::POSITIONAL)
    {
      unsigned int element = p.second->getPosNo();
      auto rawArgPtr = std::static_pointer_cast<argutil::Argument<std::string>>(p.second);
      rawArgPtr->setPosValue(userArgs[element]);
    }
  }
  
  
  // +=== TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST ===
//   for (auto p : m_args)
//   {
//     if (p.second->getType() == argutil::ArgumentParent::TYPE::POSITIONAL)
//     {
//       auto rawArgPtr = std::static_pointer_cast<argutil::Argument<std::string>>(p.second);
//       std::cout << "POS VALUE: " << rawArgPtr->getPosValue() << std::endl; 
//     }
//   }
  // +=== TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST ===
  
}



namespace lulib {
namespace argutil {

template <typename T>
static std::vector<T> readValues(
  std::vector<std::string> usrArgs,
  unsigned int start,
  unsigned int stop
)
{
  std::string clArg = usrArgs[start-1];
  std::vector<T> vec;
  for (std::size_t j=start; j<stop; ++j)
  {
    T tmpv;
    std::stringstream ss;
    try
    {
      ss << usrArgs.at(j);
    }
    catch (const std::exception& e)
    {
      throw lulib::argutil::ArgError("Error! Wrong number of parameters for argument \n  '" 
                                    + clArg 
                                    + "' on command line input. See -h,--help.");
    }
    ss >> tmpv;
    if (ss.fail())
      throw lulib::argutil::ArgError("Error! Wrong parameter type for argument '"
                                     + clArg 
                                     + "' \n  on command line input. See -h,--help.");
    else
      vec.push_back(tmpv);
    
    ss.clear();
    ss.str(std::string(""));
  }
  return vec;
}

} // namespace: lulib
} // namespace: argutil


void 
lulib::ArgHandler::updateParameter(
  const std::vector<std::string>& usrArgs
)
{
  std::size_t i=0;
  while ( i < usrArgs.size() )
  {
    if (usrArgs[i][0] != '-')
      i++;
    else
    {
      ArgumentParentPtr argPtr = getArgument(usrArgs[i]);
      unsigned int nPar = argPtr->getNPar();
      
      switch (argPtr->getType())
      {
        case (argutil::ArgumentParent::TYPE::BOOL):
        {
          auto rawArgPtr = std::static_pointer_cast<argutil::Argument<bool>>(argPtr);
          rawArgPtr->setValues(lulib::argutil::readValues<bool>(usrArgs, i+1, i+nPar+1));
          break;
        }
        case (argutil::ArgumentParent::TYPE::DOUBLE):
        {
          auto rawArgPtr = std::static_pointer_cast<argutil::Argument<double>>(argPtr);
          rawArgPtr->setValues(lulib::argutil::readValues<double>(usrArgs, i+1, i+nPar+1));
          break;
        }
        case (argutil::ArgumentParent::TYPE::FLOAT):
        {
          auto rawArgPtr = std::static_pointer_cast<argutil::Argument<float>>(argPtr);
          rawArgPtr->setValues(lulib::argutil::readValues<float>(usrArgs, i+1, i+nPar+1));
          break;
        }
        case (argutil::ArgumentParent::TYPE::INT):
        {
          auto rawArgPtr = std::static_pointer_cast<argutil::Argument<int>>(argPtr);
          rawArgPtr->setValues(lulib::argutil::readValues<int>(usrArgs, i+1, i+nPar+1));
          break;
        }
        case (argutil::ArgumentParent::TYPE::STR):
        {
          auto rawArgPtr = std::static_pointer_cast<argutil::Argument<std::string>>(argPtr);
          rawArgPtr->setValues(lulib::argutil::readValues<std::string>(usrArgs, i+1, i+nPar+1));
          break;
        }
        default:
        {
          break;
        }
      }
      i+=1+nPar;
    }
  }
  
}



lulib::ArgHandler::ArgumentParentPtr
lulib::ArgHandler::getArgument(
  const std::string& arg
) const
{
  return isArgRegistered(arg);
}



lulib::ArgHandler::ArgumentParentPtr
lulib::ArgHandler::isArgRegistered(
  const std::string& arg
) const
{
  auto it = m_longArgs.find(arg);
  if ( it  != m_longArgs.cend() )
    return it->second;
  
  it = m_shortArgs.find(arg);
  if ( it != m_shortArgs.cend() )
    return it->second;
  
  it = m_nameArgs.find(arg);
  if ( it != m_nameArgs.cend())
    return it->second;
  
  return nullptr;
}


bool 
lulib::ArgHandler::isUsrArgPresent(
  const std::string& shortName, 
  const std::string& longName, 
  const std::vector< std::string >& userArgs)
{
  for (auto str : userArgs)
  {
    if (str == shortName or str == longName)
    {
      return true;
    }
  }
  return false;
}
bool 
lulib::ArgHandler::isUsrArgPresent(
  const std::string& argName, 
  const std::vector< std::string >& userArgs)
{
  return std::find(userArgs.begin(), userArgs.end(), argName) != userArgs.end();
}



void 
lulib::ArgHandler::printHelp(
)
{
  std::cout << getUsageLine() << std::endl;
//   std::cout << std::endl;
  
  // description
  if (m_programDescription != "")
  {
    std::cout << getProgDescription() << std::endl;
    std::cout << std::endl;
  }
  
  std::cout << "positional arguments:" << std::endl;
  std::stringstream ssPositional;
  std::stringstream ssOptional;
  for (auto aPair : m_args)
  {
    if (aPair.second->isPositional())
    {
      ssPositional << getHelpItem(aPair.second);
      ssPositional << std::endl;
    }
    else if (aPair.second->isPositional() == false
         and aPair.second->getAName() != "-h")
    {
      if (m_license != "" and aPair.second->getAName() == "-l")
        continue;
      if (m_version != "" and aPair.second->getAName() == "-v")
        continue;
      ssOptional << getHelpItem(aPair.second);
      ssOptional << std::endl;
    }
  }
  std::cout << ssPositional.str() << std::endl;

  std::cout << "optional arguments:" << std::endl;
  std::cout << getHelpItem(getArgument("-h")) << std::endl;
  if (m_license != "")
    std::cout <<getHelpItem(getArgument("-l")) << std::endl;
  if (m_version != "")
    std::cout << getHelpItem(getArgument("-v")) << std::endl;
  std::cout << ssOptional.str() << std::endl;
  
  throw argutil::ArgError("stop:help");
}


static std::string
lineBreakTxt(
  const std::string& txt,
  std::size_t cutoff
)
{
  std::string returnStr = txt;
  if (returnStr.size() > cutoff)
  {
    std::stringstream ss;
    int teiler = returnStr.size()/cutoff;
    
    for (int i =0; i<teiler; i++)
    {
      std::size_t pos;                                          // this section finds
      std::size_t prior = returnStr.rfind(" ", (i+1)*cutoff);   //   the nearest space
      std::size_t next  = returnStr.find(" ", (i+1)*cutoff);    //   around a cutoff-character  
      if (next  == std::string::npos and 
          prior != std::string::npos)                            //   proximity
      {
          pos = prior;
      }
      else if (prior == std::string::npos and 
               next  != std::string::npos)
      {
        pos = next;
      }
      else if (prior == std::string::npos and
               next  == std::string::npos)
      {
        continue;
      }
      else 
      {
        cutoff-prior < next-cutoff ? pos = prior : pos = next;
      }
      returnStr.replace(pos, 1, "\n");
    }
    return returnStr;
  }
  else
    return returnStr;
}


std::string
lulib::ArgHandler::getProgDescription(
) const
{
  return lineBreakTxt(m_programDescription, 77);
}



std::string 
lulib::ArgHandler::getUsageLine(
) const
{
  // usage + program name
  std::stringstream argLineStrStr;
  argLineStrStr << std::setw(6+m_programName.size()+1) 
                << std::left 
                << std::setfill(' ') 
                << "usage: " + m_programName;                   // prints: 'usage: programName '
  
  // offset for the arguments
  std::size_t offset_size = 6+m_programName.size()+1;
  std::stringstream optArgStrStr;
  optArgStrStr << std::setw(offset_size) 
          << std::left 
          << std::setfill(' ') 
          << " ";
  std::string offset = optArgStrStr.str();                      // string of spaces for indenting
                                                                //   arguments
  optArgStrStr.str(std::string());
  std::stringstream posArgStrStr;
  
  
  // argument list
  optArgStrStr << " [-h]";
  if (m_license != "")
    optArgStrStr << " [-l]";
  if (m_version != "")
    optArgStrStr << " [-v] ";
    
  // Optional Parameters
  for (auto aPair : m_args)
  {
    if (aPair.second->getType() != argutil::ArgumentParent::TYPE::POSITIONAL 
    and aPair.second->getAName() != "-h")
    {
      if (aPair.second->getAName() == "-l" and m_license != "")         // -l and -v are only printed
        continue;                                                       //   if m_version and m_license
      if (aPair.second->getAName() == "-v" and m_version != "")         //   are not set
        continue;
      
      optArgStrStr << "[" << aPair.second->getAName();
      if (aPair.second->getNPar() > 0)
      {
        for (unsigned int i = 0; i<aPair.second->getNPar(); ++i)
        {
          optArgStrStr << " p" << i+1;
        }
      }
      optArgStrStr << "] ";
    }
    else if (aPair.second->getType() == argutil::ArgumentParent::TYPE::POSITIONAL)
    {
      posArgStrStr << "[" << aPair.second->getAName() << "] ";
    }
  }
  
  // add the two argument lines together to a single string
  std::string argLine = optArgStrStr.str() + posArgStrStr.str();
  
  // Make b an indented block if it is longer than 77-offset_size letters
  if (argLine.size() > 77-offset_size)
  {
    std::stringstream ss;
    int teiler = argLine.size()/(77-offset_size);
    
    for (int i =0; i<teiler; i++)
    {
      std::size_t pos = argLine.find(" ", 77-offset_size);
      ss << argLine.substr(0,pos) << "\n";
      if (i<teiler-1 or argLine.size() > 1)
        ss << offset;
      
      argLine.erase(0,pos);
    }
    ss << argLine;
    argLine = ss.str();
  }
  
  return argLineStrStr.str()+argLine;
}


bool
lulib::ArgHandler::getArgState(
  const std::string& identifier
)
{
  ArgumentParentPtr argPtr = getArgument(identifier);
  if (argPtr == nullptr)
    throw std::invalid_argument("Invalid argument identifier (short-, long-, argname). '" 
                                + identifier + "' \n"
                                "getArgState can't be performed. Check your code!");
    
  return argPtr->isUserInput();                         // returns true if argument was selected on
}                                                       //   the command line by user 


std::string 
lulib::ArgHandler::getHelpItem(
  lulib::ArgHandler::ArgumentParentPtr argPtr
)
{
  std::string helpTxt = argPtr->getHelpText();
  
  if (helpTxt.size() > 52)
  {
    std::string offset = "                       ";
    std::stringstream ss;
    int teiler = helpTxt.size()/52;
    
    for (int i =0; i<teiler; i++)
    {
      std::size_t pos = helpTxt.find(" ", 52);           // find next " " after 52th position
      if (pos != std::string::npos){                     // check if end of string reached
        ss << helpTxt.substr(0,pos) << "\n";             // if enough string is left over
        
        if (i<teiler-1 or helpTxt.size() > 1)            // in case string is left over for new line
          ss << offset;                                  // add some offset
      }
      else
        ss << helpTxt.substr(0,pos);                     // in case string is longer than 52 but 
                                                         //   there is no " " left in string
      
      helpTxt.erase(0,pos);
    }
    ss << helpTxt;
    helpTxt = ss.str();
  }
  
  // get the argument names
  std::string argName;
  if (argPtr->getType() == argutil::ArgumentParent::TYPE::POSITIONAL)
  {
    argName = argPtr->getAName();
  }
  else
  {
    // add the parameters!!
    if (argPtr->getArgnameShort() == "")
      argName = argPtr->getArgnameLong();
    else if (argPtr->getArgnameLong() == "")
      argName = argPtr->getArgnameShort();
    else
      argName = argPtr->getArgnameShort() + ", " + argPtr->getArgnameLong();
  }
  
  // Give a-string a new line if longer than 21 letters
  std::stringstream strstr;
  if ( argName.size() < 21 )
  {
    strstr << "  " 
           << std::left 
           << std::setw(22) 
           << argName;
    strstr << helpTxt;
    return strstr.str();
  }
  else
  {
    strstr << "  " 
    << argName << "\n" 
    << std::internal 
    << std::setfill(' ') 
    << std::setw(24+helpTxt.size()) 
    << helpTxt;
    return strstr.str();
  }
  
}



static std::string
gnuLicenseTxt(
  std::string license,
  std::string version
)
{
  std::stringstream ss;
  std::string txt;
  std::size_t cutoff = 56;
  txt = " is free software: you can redistribute it and/or modify "
        "it under the terms of the " + license + " as "
        "published by the Free Software Foundation, either version " + version + " of "
        "the License, or any later version.";
  ss << lineBreakTxt(txt,cutoff) << "\n\n";
  txt = "This software is distributed in the hope that it will be useful, "
        "but WITHOUT ANY WARRANTY; without even the implied warranty of "
        "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the "
        "GNU Lesser General Public License for more details.";
  ss << lineBreakTxt(txt,cutoff) << "\n\n";
  txt = "You should have received a copy of the " + license +
        " along with this code. If not, see <http://www.gnu.org/licenses/>.";
  ss << lineBreakTxt(txt,cutoff);
  
  return ss.str();
}




void 
lulib::ArgHandler::setLicense(
  const lulib::ArgHandler::LICENSE licenseType,
  const std::string& licenseTxt
)
{
  std::string license, version;
  std::stringstream licenseSS;
  
  licenseSS << m_programName << "\n";
  licenseSS << m_author << "\n\n";
  licenseSS << m_programName;
  
  switch (licenseType)
  {
    case (LICENSE::GNU_GENERAL_PUBLIC_LICENSE_V3):
    {
      license = "GNU General Public License";
      version = "3";
      licenseSS << gnuLicenseTxt(license, version);
      break;
    }
    case (LICENSE::GNU_LESSER_GENERAL_PUBLIC_LICENSE_V3):
    {
      license = "GNU Lesser General Public License";
      version = "3";
      licenseSS << gnuLicenseTxt(license, version);
      break;
    }
    case (LICENSE::CUSTOMIZED):
    {
      if (licenseTxt == "")
        throw std::invalid_argument("No version text has been given.");
      licenseSS << licenseTxt;
      break;
    }
  }
  
  m_license = licenseSS.str();
  
  registerArg("short=l;"
              "long=license;"
              "type=bool;"
              "help=information about the license"
             );
}




void
lulib::ArgHandler::setVersion(
  const std::string& version
)
{
  m_version = version;
  registerArg("short=v;"
              "long=version;"
              "type=bool;"
              "help=show program's version number and exit"
             );
}

void
lulib::ArgHandler::setProgDescription(
  const std::string& description
)
{
  m_programDescription = description;
}


void 
lulib::ArgHandler::setAuthors(
  const std::string& authors
)
{
  m_author = authors;
}


void 
lulib::ArgHandler::printVersion(
)
{
  std::cout << m_version << std::endl;
  throw argutil::ArgError("stop:version");
}

void 
lulib::ArgHandler::printLicense(
)
{
  std::cout << m_license << std::endl;
  throw argutil::ArgError("stop:license");
}


void 
lulib::ArgHandler::setProgramName(
  const std::string& programName
)
{
  m_programName = programName;
}
