/* 
 * This file is part of liblupriv ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#include "lupriv/InputFileParser.hpp"


#include <iostream>
#include <sstream>
#include "lupriv/StringManipulation.hpp"

void 
lulib::InputFileParser::registerKeyword(
  const std::string& keyword
)
{
  m_keywordMap[lulib::string::toLower(keyword)] = "";              // no value, needs to be read from file!
}



void 
lulib::InputFileParser::registerKeyword(
  const std::string& keyword, 
  const std::string& value
)
{
  m_keywordMap[lulib::string::toLower(keyword)] = lulib::string::toLower(value);  // sets a default value
}



void 
lulib::InputFileParser::readFile(
  const std::string& filename
)
{
  //--------------------------------------------------
  // check if keywords have been registered initially
  //--------------------------------------------------
  if (m_keywordMap.size() == 0)
    throw std::runtime_error("No keywords have been registered. "
                             "Don't know what to read from input!");
  
  //-------------------------------
  // reads all lines from input file
  //-------------------------------
  std::vector<std::string> rawFileLines;
  rawFileLines = m_fileReader.readAllLines(filename);
  
  //---------------------------------------
  // only keeps the essential lines from 
  //   the raw file lines (and lowers them)
  //---------------------------------------
  std::vector<std::string> fileLines;
  for (std::string &s : rawFileLines)
  {
    s = lulib::string::trim(s);
    if (s[0] == '#')
      continue;
    
    auto hashPos = s.find("#");
    
    if (hashPos == std::string::npos)
    {
      s = lulib::string::toLower(s);
      fileLines.push_back(lulib::string::trim(s));
    }
    else
    {
      s = s.erase(hashPos);
      s = lulib::string::toLower(s);
      fileLines.push_back(lulib::string::trim(s));
    }
  }
  
  //---------------------------------------
  // checks if all essential keywords can
  //   be found in input file
  //---------------------------------------
  checkEssentialKeywords(fileLines);
  readParameters(fileLines);
}



void 
lulib::InputFileParser::checkEssentialKeywords(
  const std::vector<std::string>& lines
)
{
  //---------------------------------------
  // Set essentialKeywords according to
  //   their value in the parameter map
  //---------------------------------------
  std::vector<std::string> essentialKeywords;
  for (auto& keywordPair : m_keywordMap)
  {
    if (keywordPair.second == "")                               // if there is no value set for the
      essentialKeywords.push_back(keywordPair.first);           //   keyword it is an essential one
  }
 

  //---------------------------------------
  // Check if essentialKeywords exist
  //---------------------------------------
  // go through the input and check if all necessaryKeywords are provided by user
  
  int lineCounter = 0;
  
  for (auto keyword : essentialKeywords)
  {
    bool isKeywordFound = false;
    
    for (auto line : lines)
    {
      lineCounter++;
      
      // Check if keyword is provided by user
      if (isKeywordInLine(keyword, line))
      {
        if (not isKeywordInLine("=", line))
        {
          raiseInputError("Error in input! Expected '=' couldn't be found in line: "
                          , lineCounter);
        }
        else
        {
          isKeywordFound = true;
          break;
        }
      }
    }
    
    // in case an essential keyword wasn't provided, the program stops
    if (not isKeywordFound)
      raiseInputError("Error in input! Missing '" + keyword + "' keyword in input");
  }
}



void 
lulib::InputFileParser::readParameters(
  const std::vector<std::string>& lines
)
{
  for (auto const& keyword : m_keywordMap)
  {
    std::string parameterString;
    bool isBracketingStarting = false;
    
    for (auto const& line : lines)
    {
      //----------------------------------------
      // checks if there is a "{" if that is
      //   the case it reads all parameters inside
      //----------------------------------------
      if (isKeywordInLine(keyword.first, line) 
          and isBracketingStarting == false)
      {
        // if "{" is in the same line, several values are read until "}" is found
        if ( line.find("{") != std::string::npos )
        {
          // all values are concatenated to one string
          isBracketingStarting = true;
        }
        else
        {
          parameterString = line;
          lulib::string::replaceThis(parameterString, keyword.first, " ");
          lulib::string::replaceThis(parameterString, "=", " ");
          lulib::string::reduceThis(parameterString);
          m_keywordMap.at(keyword.first) = parameterString;
          break;                                     // stop searching file, when keyword was found
        }
      }
      
      //----------------------------------------
      // if a "{" was found, it looks for a "}"
      //----------------------------------------
      if (isBracketingStarting)
      {
        auto bracketPos = line.find("}");
        if (bracketPos != std::string::npos)
        {
          // add everything till "}" to the parameter-one-line-string
          parameterString += " ";
          parameterString += line.substr(0, bracketPos);
          lulib::string::replaceThis(parameterString, keyword.first, " ");
          lulib::string::replaceThis(parameterString, "=", " ");
          lulib::string::replaceThis(parameterString, "{", " ");
          lulib::string::replaceThis(parameterString, "}", " ");
          lulib::string::trimThis(parameterString);
          lulib::string::reduceThis(parameterString);
          m_keywordMap.at(keyword.first) = parameterString;
          isBracketingStarting = false;
          break;
        }
        else
        {
          parameterString += " ";
          parameterString += line;
        }
      }
    }
  }
}



bool 
lulib::InputFileParser::isKeywordInLine(
  const std::string& keyword, 
  const std::string& line
) const
{
  // find exact match
  std::vector<std::string> lineParts = lulib::string::split(line);
  bool found = false;
  
  for (auto s : lineParts)
  {
    lulib::string::trimThis(s);
    if (s.length() != keyword.length())
      continue;
    else if (s.find(keyword) == 0) // std::string::npos and s.find(keyword) == 0) 
    {
      found = true;
      break;
    }
  }
  return found;
}



inline void 
lulib::InputFileParser::raiseInputError(
  const std::string& errorMessage, 
  const int lineNumber
) const
{
  std::stringstream ss;
  ss << lineNumber;
  throw std::runtime_error(errorMessage + ss.str());
}



inline void 
lulib::InputFileParser::raiseInputError(
  const std::string& errorMessage
) const
{
  throw std::runtime_error(errorMessage);
}



std::string 
lulib::InputFileParser::getValues(
  const std::string& keyword
) const
{
  std::string sw = keyword;
  lulib::string::toLowerThis(sw);
  std::string ss;
  try
  {
    ss = m_keywordMap.at(sw);
  }
  catch (const std::exception& e)       // TODO that's slow and should be done differently
  {
    std::cout << e.what() << std::endl;
    std::cout << "ERROR: '" << keyword << "' does not exist in keyword list!" << std::endl;
    std::cout << "Error occurred in 'getValues()' method." << std::endl;
    throw;
  }
  return ss;
}



std::stringstream 
lulib::InputFileParser::getValuesStream(
  const std::string& keyword
) const
{
  std::string sw = keyword;
//   lulib::StringWrapper sw = keyword;
  lulib::string::toLowerThis(sw);
  std::stringstream ss;
  try
  {
    ss << m_keywordMap.at(sw);
  }
  catch (const std::exception& e)
  {
    std::cout << e.what() << std::endl;
    std::cout << "ERROR: '" << keyword << "' does not exist in keyword list!" << std::endl;
    std::cout << "Error occurred in 'getValuesStream()' method." << std::endl;
    throw;
  }
  return ss;
}
