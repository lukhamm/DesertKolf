/* 
 * This file is part of liblupriv ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#include "lupriv/BinaryFileIO.hpp"


lulib::io::BinaryFileIO::BinaryFileIO(
)
: m_readPos(0)
{
  
}


lulib::io::BinaryFileIO::BinaryFileIO(
  const std::size_t initialCapacity
)
: m_readPos(0)
{
  m_dataFile.reserve(initialCapacity);
}


lulib::io::BinaryFileIO::~BinaryFileIO(
)
{
  
}


void
lulib::io::BinaryFileIO::resetReadPos(
)
{
  m_readPos = 0;
}


std::size_t
lulib::io::BinaryFileIO::size(
) const
{
  return m_dataFile.size();
}


std::string 
lulib::io::BinaryFileIO::readString(
  std::size_t stringSize
)
{
  std::size_t size = stringSize*sizeof(char);
  // adjust size to fit the rest of the data size that is read
  if (m_readPos + size > m_dataFile.size())
  {
    size = m_dataFile.size() - m_readPos;
  }
  std::string readString((char*)(&m_dataFile[0] + m_readPos), size);
  m_readPos += size;
  return readString;
}


std::size_t
lulib::io::BinaryFileIO::writeString(
  const std::string& data
)
{
  for (std::size_t i=0; i<data.size(); i++)
  {
    *this << data[i];
  }
  return data.size()*sizeof(char);
}
