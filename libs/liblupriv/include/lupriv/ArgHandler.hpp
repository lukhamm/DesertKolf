/* 
 * This file is part of liblupriv ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

/*
 * TODO:
 * -- add groups
 * -- add more default license types
 */


#ifndef ARGHANDLER_HPP
#define ARGHANDLER_HPP

#include <string>
#include <unordered_map>
#include <memory>
#include <algorithm>

#include "argutil/Argument.hpp"
#include "argutil/ArgError.hpp"

namespace lulib {

  /*
   * ArgHandler 
   * 
   * -- an easy to use argument handler
   * 
   */
  
class ArgHandler {
public:
  enum class LICENSE { 
    CUSTOMIZED, 
    GNU_GENERAL_PUBLIC_LICENSE_V3,
    GNU_LESSER_GENERAL_PUBLIC_LICENSE_V3
  };
  /*
   * Constructor
   * 
   * -- creates first argument, which is -h, --help default argument
   * 
   */
  ArgHandler();
  
  
  
  /*
   * setVersion()
   * 
   * Parameter
   * -- const std::string&      | anything goes
   * 
   */
  void 
  setVersion(
    const std::string& version
  );


  
  /*
   * setLicense()
   * 
   * -- sets license
   * 
   * Parameter
   * -- const std::string&      | anything goes
   * 
   */
  void
  setLicense(
    const LICENSE licenseType, 
    const std::string& licenseTxt = ""
  );
  
  
  
  /*
   * setAuthors()
   * 
   * -- lets you set the contributing authors
   * 
   * Parameter
   * -- const std::string&      | anything goes
   * 
   */
  void
  setAuthors(
    const std::string& authors
  );
  
  
  
  /*
   * setProgramName()
   * 
   * -- lets you set the name of your program
   * 
   * Parameter
   * -- const std::string&      | anything goes
   * 
   */
  void
  setProgramName(
    const std::string& programName
  );
  

  
  /*
   * setProgramDescription()
   * 
   * -- lets the user define the program description shown in the help (-h) menu
   * 
   * Parameter
   * -- const std::string&      | anything goes
   * 
   */
  void
  setProgDescription(
    const std::string& description
  );
  
  
  
  /*
   * registerArg()
   * 
   * -- to use an argument it needs to be registered
   * 
   * Parameter
   * -- const std::string&      | this string needs to be formatted properly 
   *                              (see documentation)
   * Throws
   * -- std::invalid_argument
   */
  void
  registerArg(
    const std::string& argumentInputString
  );
  
  
  
  /*
   * parse()
   * 
   * -- processes command line arguments and checks them agains registered arguments
   * 
   * Parameter
   * -- const int user_argc     | argc from main
   * -- char* user_argv[]       | argv from main
   * 
   * Throws
   * -- argutil::ArgError
   */
  void
  parseArgs(
    const int user_argc,
    char* user_argv[]
  );
  
  
  
  /*
   * getArgValues()
   * 
   * -- returns an argument's parameter as an std::vector
   * 
   * Parameter
   * -- const std::string&      | is the short-, long-, or argname of the argument as
   *                                        given upon registration of argument
   * 
   * Returns
   * -- bool                    | --> true if argument was user input
   * 
   * Throws
   * -- std::invalid_argument   | in case identifier is not registered
   * 
   */
  template <typename T>
  const std::vector<T>&
  getArgValues(
    const std::string& identifier
  );
  
  
  /*
   * getArgState()
   * 
   * -- returns true if the argument was given by the user on the command line
   * 
   * Parameter
   * -- const std::string&      | is the short-, long-, or argname of the argument as
   *                                        given upon registration of argument
   * 
   * Returns
   * -- bool                    | --> true if argument was user input
   * 
   * Throws
   * -- std::invalid_argument   | in case identifier is not registered
   */
  bool
  getArgState(
    const std::string& identifier
  ); 
  
  
private:
  using ArgumentParentPtr = std::shared_ptr<argutil::ArgumentParent>;
  using ArgPtrMap = std::unordered_map<std::string, ArgumentParentPtr>;
  
  ArgPtrMap m_args;                                     // holds pointers to the reg. arguments
  ArgPtrMap m_shortArgs, m_longArgs, m_nameArgs;        // same as m_args, but look for different
                                                        //   identifiers
  unsigned int m_nPositionalArg{0};                     // number of positional arguments
  unsigned int m_nPosArgCounter{0};                     // positional argument counter according
                                                        //   to the order they have been registered
  std::string m_programName{"a.out"};
  std::string m_license{""};                            // treated as not set if ""
  std::string m_version{""};                            //         "
  std::string m_programDescription{""};
  std::string m_author{""};                             // set the author(s) name
  
  /*
   * readUserArgs()
   * 
   */
  std::vector<std::string>
  readUserArgs(
    const int user_argc,
    char* user_argv[]
  );
  
  /*
   * isUsrArgPresent()
   * 
   */
  bool
  isUsrArgPresent(
    const std::string& short_name,
    const std::string& long_name,
    const std::vector<std::string>& userArgs
  );
  bool
  isUsrArgPresent(
    const std::string& arg_name,
    const std::vector<std::string>& userArgs
  );
  
  /*
   * deduceArg()
   * 
   * -- checks user arguments
   * 
   */
  std::shared_ptr<argutil::ArgumentParent>
  deduceArg(
    const std::string& argString
  );
  
  /*
   * updateParameter()
   * 
   * -- changes parameters of registered arguments 
   * 
   */
  void 
  updateParameter(
    const std::vector<std::string>& usrArgs
  );
  
  void 
  printHelp(
  );
  
  void
  printVersion(
  );
  void
  printLicense(
  );
  
  ArgumentParentPtr
  isArgRegistered(
    const std::string& arg
  ) const;
  ArgumentParentPtr
  getArgument(
    const std::string& arg
  ) const;
  
  template <typename T>
  bool
  hasUniqueElements(
    std::vector<T> v
  ) const;
  
  std::string
  getUsageLine(
  ) const;
  
  std::string
  getProgDescription(
  ) const;
  
  std::string
  getHelpItem(
    ArgumentParentPtr argPtr
  );
  
}; // class ArgHandler




template <typename T>
bool
ArgHandler::hasUniqueElements(
  std::vector<T> v
) const
{
  std::sort(v.begin(), v.end());
  for (std::size_t i = 1; i < v.size(); ++i)
  {
    if (v[i] == v[i-1])
      return false;
  }
  return true;
}



template <typename T>
const std::vector<T>&
ArgHandler::getArgValues(
  const std::string& identifier
)
{
  ArgumentParentPtr argPtr = getArgument(identifier);
  if (argPtr == nullptr)
    throw std::invalid_argument("Invalid argument identifier (short-, long-, argname).\n"
                                "getArgValues can't be performed. Check your code!");
    
  auto rawArgPtr = std::static_pointer_cast<argutil::Argument<T>>(argPtr);
  return rawArgPtr->getValues();
}


} // namespace lulib

#endif