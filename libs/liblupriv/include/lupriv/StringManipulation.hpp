/* 
 * This file is part of liblupriv ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

/*
 * lulib::string::
 * 
 * -- contains functions for std::string manipulation
 * 
 * Requirements:
 * -- std=c++11
 * 
 * Version
 * -- 1.1
 * 
 */

#ifndef LULIB_STRING_MANIPULATION_HPP
#define LULIB_STRING_MANIPULATION_HPP

#include <string>
#include <vector>

namespace lulib {
  namespace string {
    
    /*
     * trim()
     * 
     * -- trims a string (deletes trailing spaces)
     * 
     */
    std::string 
    trim(
      const std::string& inString,
      const std::string& replaceCharacter = " \t"
    );
    
    /*
     * trimThis()
     * 
     * -- directly trims inString (deletes trailing spaces)
     * 
     */
    std::string&
    trimThis(
      std::string& inString,
      const std::string& replaceCharacter = " \t"
    );
    
    
    
    /*
     * reduce()
     * 
     * -- deletes additional "replaceCharacter"-characters from a string and fills with
     *    "fillString"
     * 
     */
    std::string
    reduce(
      const std::string& inString,
      const std::string& fillString = " ", 
      const std::string& replaceCharacter = " \t"
    );
    
    
    
    /*
     * reduceThis()
     * 
     * -- deletes additional "replaceCharacter"-characters from a string and fills with
     *    "fillString"
     * 
     */
    std::string&
    reduceThis(
      std::string& inString,
      const std::string& fillString = " ", 
      const std::string& replaceCharacter = " \t"
    );
    
    
    
    /*
     * replace()
     * 
     * -- finds substring and replaces it with another string 'newSubstring'
     * 
     */
    std::string 
    replace(
      const std::string& inString, 
      const std::string& oldSubstring, 
      const std::string& newSubstring
    );
    std::string 
    replace(
      std::string&& inString, 
      const std::string& oldSubstring, 
      const std::string& newSubstring
    );
    std::string&
    replaceThis(
      std::string& inString, 
      const std::string& oldSubstring, 
      const std::string& newSubstring
    );
    
    /*
     * split
     * 
     * -- splits a string at a delimiter and returns a vector of substrings
     * 
     */
    std::vector<std::string> 
    split(
      const std::string& inString,
      const std::string& delimiter = " "
    );
    
    /*
     * split
     * 
     * -- splits a string but ignores delimiters that have an escape character in front
     * 
     */
    std::vector<std::string> 
    splitEsc(
      const std::string& inString,
      const char delimiter = ' ',
      const char escChar = '&'
    );
    
    
    /*
     * charToLower
     * 
     * -- changes capital ASCII Letters to lower letters
     * 
     */
    char charToLower(
      char in
    );
    
    
    /*
     * toLower()
     * 
     * -- turns all ASCII characters of a string to lower case letters
     * 
     */
    std::string toLower(
      const std::string& inString
    );
    std::string toLower(
      std::string&& inString
    );
    std::string& toLowerThis(
      std::string& inString
    );
    
  }
}



#endif