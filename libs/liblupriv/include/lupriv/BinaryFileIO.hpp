/* 
 * This file is part of liblupriv ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

/*
 * lulib::io::
 * 
 * -- contains functions for io manipulations
 * 
 * Requirements:
 * -- std=c++11
 * 
 * Version
 * -- 1.0
 * 
 */

#ifndef LULIB_IO_BINARYFILE_HPP
#define LULIB_IO_BINARYFILE_HPP

#include <vector>
#include <string>

namespace lulib {
  namespace io {
    
    class BinaryFileIO {
    public:
      BinaryFileIO(
      );
      
      BinaryFileIO(
        const std::size_t initialCapacity
      );
      
      ~BinaryFileIO(
      );
      
      void 
      resetReadPos(
      );
      
      std::size_t 
      size(
      ) const;
      
      std::string 
      readString(
        std::size_t stringSize
      );
      
      template<typename T>
      T
      read(
      );
      
      template<typename T>
      std::size_t 
      write(
        const T& data
      );

      std::size_t
      writeString(
        const std::string& data
      );
      
      const std::vector<char>& 
      getData(
      ) const
      {
        return m_dataFile;
      }
      
      void
      setData(
        const std::vector<char> data
      )
      {
        m_dataFile = data;
      }
      
      
      /*
       * read Position is not proceeded
       */
      template<typename T>
      std::size_t 
      readOnly(
        T& data
      );
      
    private:
      
      template<typename T>
      BinaryFileIO& 
      operator>>(
        T& data
      );
      
      template<typename T>
      BinaryFileIO& operator<<(
        const T& data
      );
      
      std::size_t m_readPos;
      
      std::vector<char> m_dataFile;
      
    };
  }
}    
    
    
template<typename T>
T
lulib::io::BinaryFileIO::read(
)
{
  T outData = T();
  *this >> outData;
  return outData;
}


template<typename T>
std::size_t
lulib::io::BinaryFileIO::write(
  const T& data
)
{
  *this << data;
  return sizeof(T);
}


template<typename T>
std::size_t
lulib::io::BinaryFileIO::readOnly(
  T& data
)
{
  *this >> data;
  m_readPos -= sizeof(T);
  return sizeof(T);
}


template<typename T>
lulib::io::BinaryFileIO&
lulib::io::BinaryFileIO::operator>>(
  T& data
)
{
  std::size_t typeSize = sizeof(T);
  // reading may exceed the dataFile (no feedback is given)
  if (m_readPos + typeSize > m_dataFile.size())
  {
    return *this;
  }
  
  std::copy(m_dataFile.begin() + m_readPos, 
            m_dataFile.begin() + m_readPos + typeSize, 
            reinterpret_cast<char*>(&data) );
  
  m_readPos += typeSize;
  return *this;
}


template<typename T>
lulib::io::BinaryFileIO&
lulib::io::BinaryFileIO::operator<<(
  const T& data
)
{
  std::size_t typeSize = sizeof(T);
  const char* contentPtr = reinterpret_cast<const char*>(&data);
  m_dataFile.insert(m_dataFile.end(), contentPtr, contentPtr+typeSize);
  return *this;
}



#endif