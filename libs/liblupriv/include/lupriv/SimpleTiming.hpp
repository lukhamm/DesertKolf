/* 
 * This file is part of liblupriv ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

/*
 * lulib::utils
 * 
 * -- contains a class for measuring run times using chrono
 * 
 * Requirements:
 * -- std=c++11
 * 
 * Version
 * -- 1.0
 * 
 */

#ifndef LULIB_UTILS_SIMPLETIMING_HPP
#define LULIB_UTILS_SIMPLETIMING_HPP

#include <chrono>
#include <iostream>
#include <sstream>

namespace lulib {
  namespace utils {

    class SimpleTiming {
    public:
      
      // Start time measurement
      inline void 
      start(
      )
      {
        m_start = std::chrono::steady_clock::now();
      };
      
      // End time measurement
      inline void 
      end(
      ){
        m_end = std::chrono::steady_clock::now();
      };
      
      // Print time measurement to screen
      inline void 
      print(
      )
      {
        std::cout << "Elapsed time: " 
                  << std::chrono::duration<double> (m_end-m_start).count() 
                  << std::endl;
      };
      
      // operator<< overload prototype for string streaming
      friend std::ostream& 
      operator<<(
        std::ostream& os, const SimpleTiming& timing
      );

    private:
      std::chrono::time_point<std::chrono::steady_clock> m_start, m_end;
    };

  }
}


#endif