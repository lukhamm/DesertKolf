/* 
 * This file is part of liblupriv ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

/*
 * lulib::math::
 * 
 * -- contains functions for a matrix object (maybe a bit faster than std::vectors)
 * 
 * Requirements:
 * -- std=c++11
 * 
 * Version
 * -- 1.0
 * 
 */

#ifndef LULIB_MATH_MATRIX_HPP
#define LULIB_MATH_MATRIX_HPP

#include <iostream>
#include <stdexcept>
#include <initializer_list>
#include <string>
#include <cmath>
#include <vector>
#include <array>

namespace lulib {
  namespace math {

  enum class UNITS { DEG, RAD };
  enum class ROTAXIS { X_AXIS, Y_AXIS, Z_AXIS };
  
template <class T>
class Matrix {
public:
  /*
   * Construction
   * 
   * -- uninitialized
   * -- row, column, (init value)
   * -- row, column, matrix code ("ones"->fills with ones)
   * -- row (single no. for vectors)
   * -- Initializer list e.g. Matrix m = { 1 0 1 0 }
   *    Rows and columns need to be set manually in this case.
   *    Use e.g. reshape member function
   * 
   */
  Matrix(
  )
  {
    m = nullptr;
    m_columns = 0;
    m_rows = 0;
  }
  Matrix(
    const std::size_t in_rows, 
    const std::size_t in_columns, 
    const T value = 0
  )
  {
    m_rows = in_rows;
    m_columns = in_columns;
    m = new T[m_rows*m_columns];
    for (std::size_t k=0; k<m_rows*m_columns; ++k)
    {
      m[k] = value;
    }
  };
  Matrix(
    const std::size_t in_rows, 
    const std::size_t in_columns, 
    std::string matrix_code
  )
  {
    m_rows = in_rows;
    m_columns = in_columns;
    m = new T[m_rows*m_columns];
    if (matrix_code == "ones") createDiagonalMatrix(1);
  };
  Matrix(
    const std::size_t in_rows
  )
  {
    m_rows = in_rows;
    m_columns = 1;
    m = new T[m_rows];
    for (std::size_t k=0; k<m_rows*m_columns; ++k)
    {
      m[k] = 0;
    }
  };
  Matrix(
    std::initializer_list<T> l
  )
  {
    if (l.size() == 0) 
      throw std::logic_error("Initializer list is empty!");
    m = new T[l.size()];
    m_columns = 1;
    m_rows = l.size();
    std::size_t i = 0;
    for (typename std::initializer_list<T>::iterator it=l.begin(); it!=l.end(); it++)
    {
      m[i] = *it;
      i++;
    }
  };
  
  
  /*
   * Copy
   * 
   */
  Matrix(
    const Matrix<T> &other
  )
  {
    m_rows = other.rows_size();
    m_columns = other.columns_size();
    m = new T[other.size()];
    for (std::size_t i=0; i<m_rows*m_columns; ++i)
    {
      m[i] = other[i];
    }
  };

  Matrix(
    Matrix<T> &&other
  )
  {
    m_rows = other.rows_size();
    m_columns = other.columns_size();
    std::swap(m, other.m);
  }
  
  /*
   * Destruction
   * 
   */
  ~Matrix(
  )
  {
    delete [] m;
  };
  
  /*
   * Assignment operator
   * 
   * -- Matrix = Matrix
   * -- Matrix = Initializer list
   * -- Matrix = scalar (sets all elements equal to scalar)
   * -- Matrix = matrix_code (string e.g. ones, creates unity matrix)
   * -- Matrix = std::vector (1:1 conversion of elements to Matrix object)
   * 
   */
  Matrix<T>& 
  operator=(
    const Matrix<T> &other
  )
  {
    if (m_rows != other.m_rows or m_columns != other.m_columns) {
      if (m != nullptr) delete [] m;
      m = new T[other.size()];
      m_columns = other.m_columns;
      m_rows = other.m_rows;
    }
    for (std::size_t i=0; i<m_columns*m_rows; ++i){
      m[i] = other[i];
    }
    return *this;
  };
  Matrix<T>& 
  operator=(
    std::initializer_list<T> l
  )
  {
    if (l.size() == 0) throw std::logic_error("Initializer list is empty!");
    if (m != nullptr) delete [] m;
    m = new T[l.size()];
    m_columns = 1;
    m_rows = l.size();
    std::size_t i = 0;
    for (typename std::initializer_list<T>::iterator it=l.begin(); it!=l.end(); it++){
      m[i] = *it;
      i++;
    }
    return *this;
  }; 
  Matrix<T>& 
  operator=(
    const T scalar
  )
  {
    if (m == nullptr) throw std::logic_error("Cant assign scalar to an uninitialized (unsized) matrix");
    for (std::size_t i=0; i<m_columns*m_rows; ++i){
      m[i] = scalar;
    }
    return *this;
  }; 
  Matrix<T>& 
  operator=(
    const std::string matrix_code
  )
  {
    if (m == nullptr) throw std::logic_error("Cant assign with matrix code for an uninitialized (unsized) matrix");
    if (matrix_code == "ones")
    {
      createDiagonalMatrix(1);
    }
    return *this;
  }; 
  Matrix<T>& operator=(
    const std::vector<T> std_vector
  )
  {
    if (m != nullptr) delete [] m;
    m = new T[std_vector.size()];
    m_columns = 1;
    m_rows = std_vector.size();
    for (std::size_t i = 0; i < std_vector.size(); ++i){
      m[i] = std_vector[i];
    }
    return *this;
  }; 
  
  /*
   * operator[]
   * 
   * -- matrix is represented as 1D-array: operator[] grants
   *    access to array elements
   * 
   */
  T&
  operator[](
    const std::size_t i
  )
  {
    if (m == nullptr) throw std::domain_error("matrix used uninitialized (for operator[])");
    else if (i > this->size()) throw std::out_of_range("out of range for operator[]");
    else return m[i];
  }
  const T&
  operator[](
    const std::size_t i
  ) const 
  {
    if (m == nullptr) throw std::domain_error("matrix used uninitialized (for operator[])");
    if (i > this->size()) throw std::out_of_range("out of range for operator[]");
    else return m[i];
  };
  
  /*
   * operator()
   * 
   * -- access to matrix elements: rows, columns
   * 
   */ 
  T& 
  operator()(
    const std::size_t i, const std::size_t j
  )
  {
    if (m == nullptr) throw std::domain_error("matrix used uninitialized (for operator())");
    if (i > m_rows or j > m_columns) throw std::out_of_range("out of range for operator()");
    else return m[i*m_columns+j];
  };
  const T&
  operator()(
    const std::size_t i, const std::size_t j
  ) const 
  {
    if (m == nullptr) throw std::domain_error("matrix used uninitialized (for operator())");
    if (i > m_rows or j > m_columns) throw std::out_of_range("out of range for operator()");
    else return m[i*m_columns+j];
  };
  
  /*
   * operator+=
   * 
   * -- Matrix += Matrix addition
   * -- Matrix += scalar addition (adds a scalar to all elements)
   * 
   */
  Matrix<T>&
  operator+=(
    const Matrix<T>& other
  )
  {
    if (other.m == nullptr) throw std::runtime_error("cant add-assign uninitialized matrix!");
    if (m == nullptr){
      m = new T [other.m_columns*other.m_rows];
      m_rows = other.m_rows;
      m_columns = other.m_columns;
    }
    else if (m_rows != other.m_rows or m_columns != other.m_columns)
      throw std::runtime_error("wrong matrix dimensions for addition");
    for (std::size_t i = 0; i < m_rows*m_columns; i++)
      m[i] += other[i];
    return *this;
  };
  Matrix<T>& 
  operator+=(
    const T scalar
  )
  {
    if (m == nullptr) throw std::runtime_error("cant assign add scalar to uninitialized matrix!");
    for (std::size_t i = 0; i < m_rows*m_columns; i++) 
      m[i] += scalar;
    return *this;
  };
  
  /*
   * operator+
   * 
   * -- see above
   * 
   */
  Matrix<T> 
  operator+(
    const Matrix<T>& other
  ) const 
  {
    if (m == nullptr or other.m == nullptr) throw std::runtime_error("cant add uninitialized matrix!");
    if (m_rows != other.m_rows or m_columns != other.m_columns)
      throw std::runtime_error("wrong matrix dimensions for addition");
    Matrix<T> tmp(m_rows, m_columns);
    for (unsigned int i = 0; i < m_rows*m_columns; i++)
      tmp[i] = m[i] + other[i];
    return tmp;
  };
  Matrix<T> 
  operator+(
    const T scalar
  ) const 
  {
    if (m == nullptr) throw std::runtime_error("cant add scalar to uninitialized matrix!");
    Matrix<T> tmp_m(m_rows, m_columns);
    for (std::size_t i = 0; i < m_rows*m_columns; i++) 
      tmp_m[i] = m[i] + scalar;
    return tmp_m;
  };
  
  /*
   * operator-=
   * 
   * -- see above
   * 
   */
  Matrix<T>& 
  operator-=(
    const Matrix<T>& other
  )
  {
    if (other.m == nullptr) throw std::runtime_error("cant subtract-assign uninitialized matrix!");
    if (m == nullptr) {
      m = new T [other.m_columns*other.m_rows];
      m_rows = other.m_rows;
      m_columns = other.m_columns;
    }
    else if (m_rows != other.m_rows or m_columns != other.m_columns)
      throw std::range_error("wrong matrix dimensions for addition");
    for (std::size_t i = 0; i < m_rows*m_columns; i++)
      m[i] -= other[i];
    return *this;
  };
  Matrix<T>& 
  operator-=(
    const T scalar
  )
  {
    if (m == nullptr) throw std::runtime_error("cant add scalar to uninitialized matrix!");
    for (std::size_t i = 0; i < m_rows*m_columns; i++) 
      m[i] -= scalar;
    return *this;
  };
  
  /*
   * operator-
   * 
   * -- see above
   * 
   */
  Matrix<T> 
  operator-(
    const Matrix<T>& other
  ) const 
  {
    if (m == nullptr or other.m == nullptr) throw std::runtime_error("cant add uninitialized matrix!");
    if (m_rows != other.m_rows or m_columns != other.m_columns)
      throw std::runtime_error("wrong matrix dimensions for subtraction");
    Matrix<T> tmp(m_rows, m_columns);
    for (std::size_t i = 0; i < m_rows*m_columns; i++)
      tmp[i] = m[i] - other[i];
    return tmp;
  };
  Matrix<T> 
  operator-(
    const T scalar
  ) const 
  {
    Matrix<T> tmp_m(m_rows, m_columns);
    for (std::size_t i = 0; i < m_rows*m_columns; i++) 
      tmp_m[i] = m[i] - scalar;
    return tmp_m;
  };
  
  /*
   * operator*=
   * 
   * -- see above
   * 
   */
  Matrix<T>& 
  operator*=(
    const T scalar
  )
  {
    if (m == nullptr) throw std::runtime_error("cant multiply-assign scalar to uninitialized matrix!");
    for (std::size_t i = 0; i < m_columns*m_rows; i++) 
      m[i] *= scalar;
    return *this;
  };
  Matrix<T>& 
  operator*=(
    const Matrix<T>& other
  )
  {
    if (m == nullptr or other.m == nullptr) throw std::runtime_error("cant multiply-assign uninitialized matrix!");
    if (m_columns != other.m_rows) throw std::runtime_error("wrong matrix dimensions for matrix multiplication");
    Matrix<T> c(m_rows, other.m_columns);
    for (std::size_t i = 0; i < m_rows; i++)
      for (std::size_t j = 0; j < other.m_columns; j++)
        for (std::size_t k = 0; k < m_columns; k++)
          c(i,j) += m[i*m_columns+k] * other(k,j);
    
    this->operator=(c);
    return *this;
  };
  
  /*
   * operator*
   * 
   * -- see above
   * 
   */
  Matrix<T> 
  operator*(
    const Matrix<T>& other
  ) const 
  {
    if (m == nullptr or other.m == nullptr) throw std::runtime_error("cant multiply uninitialized matrix!");
    if (m_columns != other.m_rows)
      throw std::runtime_error("wrong matrix dimensions for matrix multiplication");
    Matrix<T> c(m_rows, other.m_columns);
    for (std::size_t i = 0; i < m_rows; i++)
      for (std::size_t j = 0; j < other.m_columns; j++)
        for (std::size_t k = 0; k < m_columns; k++)
          c(i,j) += m[i*m_columns+k] * other(k,j);
    return c;
  };
  Matrix<T> 
  operator*(
    const T scalar
  ) const 
  {
    if (m == nullptr) throw std::runtime_error("cant multiply uninitialized matrix by scalar!");
    Matrix<T> tmp_m(m_rows, m_columns);
    for (unsigned int i = 0; i < m_columns*m_rows; i++)
      tmp_m[i] =  m[i] * scalar;
    return tmp_m;
  };
  
  /*
   * operator/
   * 
   * -- matrix / scalar
   * 
   */
  Matrix<T> 
  operator/(
    const T scalar
  )
  {
    if (m == nullptr) throw std::runtime_error("cant devide uninitialized matrix by scalar!");
    Matrix<T> tmp_m(m_rows,m_columns);
    for (std::size_t i = 0; i < m_rows*m_columns; i++) 
      tmp_m[i] = m[i]/scalar;
    return tmp_m;
  };
  
  /*
   * operator/=
   * 
   * -- see above
   * 
   */
  Matrix<T>&
  operator/=(
    const T scalar
  )
  {
    if (m == nullptr) throw std::domain_error("cant devide-assign uninitialized matrix by scalar!");
    for (std::size_t i = 0; i < m_rows*m_columns; i++) 
      m[i] = m[i]/scalar;
    return *this;
  };
  
  /*
   * operator==
   * 
   * -- see above
   * 
   */
  bool 
  operator==(
    const Matrix<T>& other
  )
  {
    if (m_rows != other.m_rows or m_columns != other.m_columns) {
      return false;
    }
    bool returnVal = true;
    for (std::size_t i = 0; i < this->size(); ++i) {
      if (m[i] != other[i]){
        returnVal = false;
        break;
      }
    }
    return returnVal;
  }
    
  /*
   * determinant
   * 
   * -- only 2x2 and 3x3 matrixes are supported so far (TODO: implement others)
   * 
   */
  T determinant(
  ) const 
  {
    if (m == nullptr)
      throw std::runtime_error("calculate determinant from uninitialized matrix!");
    if (m_rows != m_columns) 
      throw std::runtime_error("matrix must be square to calculate determinant");
    if (m_rows > 3)        
      throw std::runtime_error("larger than 3x3 matrices not implemented yet");
    
    T det;
    if (m_rows == 2){
      det = ( m[0*m_columns+0]*m[1*m_columns+1] ) 
          - ( m[0*m_columns+1]*m[1*m_columns+0] );
    }
    else {
      det = ( m[0*m_columns+0]*m[1*m_columns+1]*m[2*m_columns+2] )
          + ( m[0*m_columns+1]*m[1*m_columns+2]*m[2*m_columns+0] )
          + ( m[0*m_columns+2]*m[1*m_columns+0]*m[2*m_columns+1] )
          - ( m[0*m_columns+2]*m[1*m_columns+1]*m[2*m_columns+0] )
          - ( m[0*m_columns+1]*m[1*m_columns+0]*m[2*m_columns+2] )
          - ( m[0*m_columns+0]*m[1*m_columns+2]*m[2*m_columns+1] );
    }
    return det;
  }
  
  /*
   * inverse
   * 
   * -- calculates the inverse matrix (only 2x2 and 3x3 matrices TODO)
   * 
   */
  Matrix<T> 
  inverse(
  ) const 
  {
    T det = this->determinant();
    if (abs(det) < 0.000001)
    {
      std::cout << "Determinant too small. Inverse matrix cannot be calculated." << std::endl;
      Matrix<T> inv(1,1);
      return inv;
    }
    else if (m_rows == 2)
    {
      Matrix<T> inv(2,2);
      inv(0,0) =  m[3] * (1.0/det);
      inv(0,1) = -m[1] * (1.0/det);
      inv(1,0) = -m[2] * (1.0/det);
      inv(1,1) =  m[0] * (1.0/det);
      return inv;
    }
    else if (m_rows == 3)
    {
      Matrix<T> inv(3,3);
      inv(0,0) = ( this->operator()(1,1)*this->operator()(2,2) ) 
               - ( this->operator()(1,2)*this->operator()(2,1) );
      inv(0,1) = -( ( this->operator()(0,1)*this->operator()(2,2) ) 
                 - ( this->operator()(0,2)*this->operator()(2,1) ) );
      inv(0,2) = ( this->operator()(0,1)*this->operator()(1,2) ) 
                - ( this->operator()(0,2)*this->operator()(1,1) );
      inv(1,0) = -( ( this->operator()(1,0)*this->operator()(2,2) ) 
                - ( this->operator()(2,0)*this->operator()(1,2) ) );
      inv(1,1) = ( this->operator()(0,0)*this->operator()(2,2) ) 
                - ( this->operator()(0,2)*this->operator()(2,0) );
      inv(1,2) = -( ( this->operator()(0,0)*this->operator()(1,2) ) 
                - ( this->operator()(0,2)*this->operator()(1,0) ) );
      inv(2,0) = ( this->operator()(1,0)*this->operator()(2,1) ) 
                - ( this->operator()(2,0)*this->operator()(1,1) );
      inv(2,1) = -( ( this->operator()(0,0)*this->operator()(2,1) ) 
                - ( this->operator()(2,0)*this->operator()(0,1) ) );
      inv(2,2) = ( this->operator()(0,0)*this->operator()(1,1) ) 
                - ( this->operator()(0,1)*this->operator()(1,0) );
      return inv*(1.0/det);
    }
    else {
      throw std::runtime_error("only 2x2 or 3x3 inverse matrices are implemented");
      Matrix<T> inv(1,1);
      return inv;
    }
  }
  
  /*
   * transpose
   * 
   * -- no check is done for vectors, yet (TODO)
   * 
   */
  Matrix<T> 
  transpose(
  ) const 
  {
    Matrix<T> trans(m_columns, m_rows);
    for (unsigned int i = 0; i < m_rows; i++) {
      for (unsigned int j = 0; j < m_columns; j++) {
        trans(j,i) = this->operator()(i,j);
      }
    }
    return trans;
  }
  
  /*
   * length
   * 
   * -- calculates the length of a vector (not defined for matrices)
   * -- throws runtime_error for matrices
   * 
   */
  T 
  length(
  ) const
  {
    if (m == nullptr) 
      throw std::runtime_error("cant calculate norm of uninitialized vector");
    T len = 0;
    if (m_rows == 1 or m_columns == 1) {
      for (std::size_t i=0; i<m_columns*m_rows; i++)
        len += pow(m[i],2);
    }
    else
      throw std::runtime_error("length only defined for vectors not for matrices");
    return sqrt(len);
  }
  
  /*
   * norm
   * 
   * -- returns a unit vector of itself
   * 
   */
  Matrix<T> 
  norm(
  ) const
  {
    if (m == nullptr) 
      throw std::runtime_error("cant calculate norm of uninitialized vector");
    if (m_rows != 1 and m_columns != 1) 
      throw std::logic_error("cannot calculate normalized vector of a matrix");
    Matrix<T> tmp_m = *this;
    T len = this->length();
    if (len > 0.000000001) 
      return tmp_m /= len;
    else 
      return tmp_m = 0;
//       throw std::runtime_error("length of vector is smaller than 0.000000001");
  }
  
  /*
   * Sizes
   * 
   * size():         returns number of total elements
   * rows_size():    returns number of row elements
   * columns_size(): returns number of column elements
   * 
   */
  inline std::size_t size() const {
    return m_rows*m_columns;
  };
  inline std::size_t rows_size() const {
    return m_rows;
  }
  inline std::size_t columns_size() const {
    return m_columns;
  }
  
  /*
   * resize()
   * 
   * -- reserves i*j (rows*columns) elements
   * 
   */
  void 
  resize(
    const std::size_t i, const std::size_t j
  )
  {
    m_columns = j;
    m_rows = i;
    if (m == nullptr){
      m = new T[m_columns*m_rows];
    }
    else {
      delete [] m;
      m = new T[m_columns*m_rows];
    }
  }
  
  /*
   * reshape()
   * 
   * -- set columns and rows manually to reshape a matrix 
   * 
   */
  inline void 
  reshape(
    const std::size_t new_rows, const std::size_t new_columns
  )
  {
    if (new_rows*new_columns != m_rows*m_columns) 
      throw std::logic_error("reshape: columns*rows != new_columns*new_rows");
    m_rows = new_rows;
    m_columns = new_columns;
  }
  
  /*
   * print()
   * 
   * -- prints the matrix / vector in a formatted way to the screen
   * 
   */
  void 
  print(
  ) const 
  {
    std::cout << "[ ";
    for (std::size_t i=0; i<m_rows; ++i){
      for (std::size_t j=0; j<m_columns; ++j){
        std::cout << m[i*m_columns+j] << " ";
      }
      if (i<m_rows-1){
        std::cout << std::endl;
        std::cout << "  ";
      }
    }
    std::cout << "]" << std::endl;
  }
  
  /*
   * clockwise rotation
   * 
   * -- all sorts of rotations (2-D, 3-D; n-D not defined)
   * -- 2D rotations defined seperately for performance
   * 
   */
  Matrix<T>&
  rotate_insitu(
    const T angle,       
    const UNITS unit = UNITS::DEG,          
    const ROTAXIS axis = ROTAXIS::Z_AXIS
  ) 
  {
    if (m == nullptr) 
      throw std::runtime_error("cant rotate uninitialized vector");
    if (m_columns > 1) 
      throw std::runtime_error("wrong vector column dimensions for rotation");
    if (m_rows < 2 or m_rows > 3) 
      throw std::runtime_error("wrong dimension for rotation of vector. Only 3D and 2D vectors supported");
    
    T theta = angle;
    if (unit == UNITS::DEG) theta = (M_PI/180.)*theta;
    
    if (m_rows == 2) {
      Matrix<T> rotm(2,2);
      rotm(0,0) = cos(theta); rotm(0,1) = sin(theta);
      rotm(1,0) = -sin(theta); rotm(1,1) = cos(theta);
      *this = rotm * (*this);
      return *this;
    }
    else if (m_rows == 3 and axis == ROTAXIS::X_AXIS) {
      Matrix<T> rotm(3,3);
      rotm(0,0) = 1; rotm(0,1) = 0;           rotm(0,2) = 0; 
      rotm(1,0) = 0; rotm(1,1) =  cos(theta); rotm(1,2) = sin(theta); 
      rotm(2,0) = 0; rotm(2,1) = -sin(theta); rotm(2,2) = cos(theta); 
      *this = rotm * (*this);
      return *this;
    }
    else if (m_rows == 3 and axis == ROTAXIS::Y_AXIS) {
      Matrix<T> rotm(3,3);
      rotm(0,0) = cos(theta); rotm(0,1) = 0; rotm(0,2) = -sin(theta); 
      rotm(1,0) = 0;          rotm(1,1) = 1; rotm(1,2) = 0; 
      rotm(2,0) = sin(theta); rotm(2,1) = 0; rotm(2,2) = cos(theta); 
      *this = rotm * (*this);
      return *this;
    }
    else if (m_rows == 3 and axis == ROTAXIS::Z_AXIS) {
      Matrix<T> rotm(3,3);
      rotm(0,0) =  cos(theta); rotm(0,1) = sin(theta); rotm(0,2) = 0; 
      rotm(1,0) = -sin(theta); rotm(1,1) = cos(theta); rotm(1,2) = 0; 
      rotm(2,0) = 0;           rotm(2,1) = 0;          rotm(2,2) = 1;
      *this = rotm * (*this);
      return *this;
    }
    else
      return *this;
  }
  Matrix<T> 
  rotate(
    const T angle, 
    const UNITS unit = UNITS::DEG,  
    const ROTAXIS axis = ROTAXIS::Z_AXIS
  ) 
  {
    Matrix<T> tmpM = *this;
    tmpM.rotate_insitu(angle, unit, axis);
    return tmpM;
  }
  Matrix<T>& 
  rotate90_2D_insitu(
  ) 
  {
    if (m == nullptr) throw std::runtime_error("cant rotate uninitialized vector");
    if (this->size() != 2)
      throw std::runtime_error("a 2D vector should be supplied if you want to do a 2D vector rotation");
    T tmp;
    tmp = m[0];
    m[0] = m[1];
    m[1] = -tmp;
    return *this;
  }
  Matrix<T>& 
  rotate180_2D_insitu(
  ) 
  {
    if (m == nullptr) throw std::runtime_error("cant rotate uninitialized vector");
    if (this->size() != 2)
      throw std::runtime_error("a 2D vector should be supplied if you want to do a 2D vector rotation");
    T tmp;
    tmp = m[0];
    m[0] = -m[1];
    m[1] = -tmp;
    return *this;
  }
  Matrix<T>& 
  rotate270_2D_insitu(
  ) 
  {
    if (m == nullptr) throw std::runtime_error("cant rotate uninitialized vector");
    if (this->size() != 2)
      throw std::runtime_error("a 2D vector should be supplied if you want to do a 2D vector rotation");
    T tmp;
    tmp = m[0];
    m[0] = -m[1];
    m[1] = tmp;
    return *this;
  }
  Matrix<T> 
  rotate90_2D(
  ) 
  {
    if (m == nullptr) throw std::runtime_error("cant rotate uninitialized vector");
    if (this->size() != 2)
      throw std::runtime_error("a 2D vector should be supplied if you want to do a 2D vector rotation");
    Matrix<T> tmp_m(2);
    tmp_m[0] =  m[1];
    tmp_m[1] = -m[0];
    return tmp_m;
  }
  Matrix<T> 
  rotate180_2D(
  ) 
  {
    if (m == nullptr) throw std::runtime_error("cant rotate uninitialized vector");
    if (this->size() != 2)
      throw std::runtime_error("a 2D vector should be supplied if you want to do a 2D vector rotation");
    Matrix<T> tmp_m(2);
    tmp_m[0] = -m[1];
    tmp_m[1] = -m[0];
    return tmp_m;
  }
  Matrix<T> 
  rotate270_2D(
  )
  {
    if (m == nullptr) throw std::runtime_error("cant rotate uninitialized vector");
    if (this->size() != 2)
      throw std::runtime_error("a 2D vector should be supplied if you want to do a 2D vector rotation");
    Matrix<T> tmp_m(2);
    tmp_m[0] = -m[1];
    tmp_m[1] =  m[0];
    return tmp_m;
  }
  
  /*
   * std_vector
   * 
   * -- returns all elements of a Matrix in an std::vector
   * 
   */
  std::vector<T>
  std_vector(
  )
  {
    std::vector<T> tmp(size());
    for (std::size_t i = 0; i < tmp.size(); ++i){
      tmp[i] = m[i];
    }
    return tmp;
  }
  
  /*
   * isInitialized()
   * 
   * -- returns false if not
   * 
   */
  bool 
  isInitialized(
  ) const 
  {
    return m == nullptr ? false : true;
  };
  
  /*
   * findEntry()
   * 
   * -- returns the index i of an entry if it exists
   * -- TODO: make template specific for ints (should not be used for floats)
   * 
   * Returns
   * -- Matrix element of certain value
   * 
   * Throws
   * -- runtime_error: if it cant find entry
   * -- runtime_error: if matrix is uninitialized
   * 
   */
  std::size_t 
  findEntry(
    const T& entry
  ) const
  {
    if (m == nullptr) throw std::runtime_error("can't find an entry in an uninitialized matrix");
    for (std::size_t i = 0; i<m_rows*m_columns; ++i){
      if (m[i] == entry) return i;
    }
    throw std::runtime_error("Couldn't find entry.");
    return 0;
  }
  
private:
  T *m;
  std::size_t m_rows, m_columns;
  
  /*
   * initializes a diagonal matrix with value 'value'
   * 
   */
  void 
  createDiagonalMatrix(
    const T value
  )
  {
    for (std::size_t i=0; i<m_rows; ++i){
      for (std::size_t j=0; j<m_columns; ++j){
        if (i==j) m[i*m_columns+j] = value;
        else  m[i*m_columns+j] = 0;
      }
    }
  };
};


  }
}


#endif