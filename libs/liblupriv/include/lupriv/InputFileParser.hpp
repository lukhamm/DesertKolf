/* 
 * This file is part of liblupriv ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef LUPRIV_INPUTFILEPARSER_HPP
#define LUPRIV_INPUTFILEPARSER_HPP

#include "ReadWriteFiles.hpp"

#include <string>
#include <unordered_map>

/*
 * InputParser
 * 
 * -- A class that parses an input for specific keywords as an unordered_map of 
 *    strings; these strings then have to be processed by employing string streams
 *    according to the users needs
 * 
 * Example Input:
 *              # - comments can be indicated by a hash sign at the beginning #
 *              # - keywords are case insensitive
 *              # - only use ASCII signs (not guaranteed to work without)
 *              # - keyword = value
 *              # - keyword = { value1 value2 value3 }
 *              myKeyword = 10
 *              myOtherKeyword = { 
 *              10 A
 *              20 B
 *              }
 * 
 */

namespace lulib {

  class InputFileParser {
  public:
    
    InputFileParser(
    ){};
    
    
    /*
    * registerKeyword()
    * 
    * -- keywords have to be registered to be found
    * 
    * Parameter
    * -- const std::string keyword       | holds keyword name as a string
    * -- const std::string value         | holds default value for keyword
    * 
    */
    void
    registerKeyword(
      const std::string& keyword
    );                            // registers essential keywords
    void registerKeyword(
      const std::string& keyword, 
      const std::string& value
    );                            // registers optional keywords
    
    
    /*
    * readFile()
    * 
    * -- reads the file's lines and deletes comments and transforms upper-case 
    *    characters to lower-case characters
    * 
    * Parameter
    * -- const std::string& filename     | path to input file
    * 
    * Throws
    * -- std::runtime_error
    * 
    */
    void
    readFile(
      const std::string& filename
    );                                    // after registering keywords -> readFile
    
    
    /*
    * GetParametersFromKeyword() const
    * 
    * -- returns the parameter of a keyword as a string
    * 
    */
    std::string
    getValues(
      const std::string& keyword
    ) const;
    
    /*
    * GetParametersFromKeyword() const
    * 
    * -- returns the parameter of a keyword as a stringstream; this makes it easy
    *    to assign them to variables
    * 
    */
    std::stringstream 
    getValuesStream(
      const std::string& keyword
    ) const;
    
    
  private:
    lulib::ReadWrite m_fileReader;
    std::unordered_map<std::string, std::string> m_keywordMap;
    
    /*
    * checkEssentialKeywords()
    * 
    * -- checks if the registered necessary keywords are given in the input
    * 
    * Parameter
    * -- const std::vector<std::string>& lines           | the important lines of the input
    * 
    * Throws
    * -- std::runtime_error
    */
    void 
    checkInput(
      const std::vector<std::string>& lines
    );                                            // should check for duplicates TODO
    void 
    checkEssentialKeywords(
      const std::vector<std::string>& lines
    );                                            // merge this one
    
    
    /*
    * readParameters()
    * 
    * -- after existance of keywords has been checked, they are read and saved in an
    *    std::unordered_map of strings
    * -- all values of the keywords are saved in a single std::string and needs to be
    *    read from the main (other) program (Maybe change later TODO)
    * 
    * Parameter
    * -- const std::vector<std::string>& lines           | the important lines of the input
    */
    void 
    readParameters(
      const std::vector<std::string>& lines
    );
    
    
    /*
    * isKeywordInLine() const
    * 
    * -- checks for an exact match of the keyword in a single std::string line
    * 
    * Parameter
    * -- const std::string& keyword      | holds the string to find (exact match) in a line
    * -- const std::string& line         | holds the string where to look for keyword
    * 
    * Comments
    * -- very slow (shouldn't be used repeatedly)
    * 
    */
    bool 
    isKeywordInLine(
      const std::string& keyword, 
      const std::string& line
    ) const;
    
    
    /*
    * raiseInputError()
    * 
    * -- throws an std::runtime_error and adds a lineNumber (for where in the input e.g.)
    * 
    */
    inline void 
    raiseInputError(
      const std::string& errorMessage
    ) const;
    inline void 
    raiseInputError(
      const std::string& errorMessage,
      const int lineNumber
    ) const;
    
  };

}


#endif