/* 
 * This file is part of liblupriv ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef ARGUMENTPARENT_HPP
#define ARGUMENTPARENT_HPP

#include <string>

namespace lulib {
namespace argutil {

class ArgumentParent {
public:
  enum class TYPE { INT, STR, BOOL, DOUBLE, FLOAT, POSITIONAL };
  
  virtual ~ArgumentParent(){};
  
  /*
   * Setter Functions for setting the argument properties
   * 
   * 
   */
  void
  setArgname(
    std::string argname1,
    std::string argname2 = ""
  );
  void
  setHelptext(
    std::string helpTxt
  );
  void
  setPositional(
    const bool positional
  );
  void
  setNPar(
    const unsigned int npar
  );
  void
  setType(
    const TYPE type
  );
  
  void
  setPosNo(
    const unsigned int n
  );
  
  void
  turnOn(
  );
  void
  turnOff(
  );

  
  /*
   * Getter functions for getting the argument properties
   * 
   * 
   */
  const std::string& 
  getArgumentId(
  ) const;
  
  const std::string&
  getArgnameShort(
  ) const;
  
  const std::string&
  getArgnameLong(
  ) const;
  
  const std::string&
  getArgname(
  ) const;
  
  /*
   * getAName()
   * 
   * -- tries to return an argument name, if set, in this order:
   *    positional name, short, longs
   * 
   */
  const std::string&
  getAName(
  ) const;
  
  const std::string&
  getHelpText(
  ) const;
  
  const TYPE&
  getType(
  ) const;
  
  unsigned int 
  getNPar(
  );
  
  unsigned int 
  getPosNo(
  );
  
  const bool&
  isUserInput(
  ) const;
  const bool&
  isPositional(
  ) const;
  
protected:
  void
  setArgumentId(
    std::string argumentId
  );
  
private:
  std::string m_id{""};                 // an id (currently: short name+long name)
  std::string m_argnameShort{""};       // short name of argument
  std::string m_argnameLong{""};        // long name of argument
  std::string m_argname{""};            // name for positional argument
  std::string m_helptxt{""};            // output text for the -h,--help information
  TYPE m_type{TYPE::BOOL};              // type of parameters (positional args are strings)
  bool m_on{false};                     // true -> argument given as input on command line
  bool m_positional{false};             // true -> argument is positional
  unsigned int m_nPar{0};               // number of parameters
  unsigned int m_posNo{0};                // position of a positional argument (starts with 0)
  
};


}
}

#endif