/* 
 * This file is part of liblupriv ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef ARGUMENT_HPP
#define ARGUMENT_HPP

#include <vector>
#include <stdexcept>

#include "ArgumentParent.hpp"

namespace lulib {
namespace argutil { 


template<typename T>
class Argument : public ArgumentParent {
public:
  
  virtual ~Argument(
  ){};
  
  void 
  setValues (
    const std::vector<T>& parameters
  )
  {
    m_parameters = parameters;
  }
  
  const std::vector<T>&
  getValues (
  ) const
  {
    return m_parameters;
  };
  
  void
  setPosValue(
    const T& posVal
  )
  {
    m_parameters.push_back(posVal);
  }
  
  const T&
  getPosValue(
  )
  {
    if (m_parameters.size() > 1)
      throw std::runtime_error("A positional argument has several entries."
                               "Something is severerly wrong. Please inform developer!");
    return m_parameters.at(0);
  }
  
protected:
  
private:
  std::vector<T> m_parameters;
};


}
}

#endif