/* 
 * This file is part of liblupriv ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

/*
 * lulib::
 * 
 * -- contains functions for Reading (and later on writing files)
 * 
 * Requirements:
 * -- std=c++11
 * 
 * Version
 * -- 0.1
 * 
 * TODO:
 * 1) Member: saveVector(vector )
 * 2) Member: readVector()
 * 3) the same for matrices and other types
 */

#ifndef LULIB_READWRITEFILES_HPP
#define LULIB_READWRITEFILES_HPP

#include <string>
#include <vector>
#include <iostream>
#include <fstream>

#include "BinaryFileIO.hpp"

namespace lulib {
  class ReadWrite {
  public:
    
   /*
    * readAllLines()
    * 
    * -- Reads all lines from a given File and puts them as strings 
    *    into a vector
    */
    std::vector<std::string>
    readAllLines(
      const std::string& filename
    ) const;
    
    
    io::BinaryFileIO
    readData(
      const std::string& filename
    ) const;
    
    
    void
    writeData(
      const std::string& filename,
      const std::vector<char>& data
    ) const;
    
    
    void
    writeData(
      const std::string& filename,
      const io::BinaryFileIO& binFile
    ) const;
    
    
    template <typename T>
    void writeMatrix(
      const std::string& filename,
      const std::vector<std::vector<T>>& matrix
    ) const;
    
    template <typename T>
    void writeVector(
      const std::string& filename,
      const std::vector<T>& vector
    ) const ;
    
  private:
    
  };
}





// ------------ Definitions -------------------

template <typename T>
void lulib::ReadWrite::writeMatrix (
  const std::string& filename,
  const std::vector<std::vector<T>>& matrix
) const
{
  std::ofstream filestream;
  filestream.open(filename, std::ios::out | std::ios::trunc );
  if (!filestream) {
    throw std::runtime_error("File open failed");
  }
  
  
  for (std::size_t i=0; i<matrix.size(); i++)
  {
    for (std::size_t j=0; j<matrix[i].size(); j++)
    {
      filestream << matrix[i][j];
      if (filestream.fail())
        throw std::runtime_error("Error writing to file!");
      
      if (j<matrix.size()-1)
        filestream << " ";
    }
    filestream << std::endl;
  }
}


template <typename T>
void lulib::ReadWrite::writeVector(
  const std::string& filename,
  const std::vector<T>& vector
) const
{
  std::ofstream filestream;
  filestream.open(filename, std::ios::out | std::ios::trunc );
  if (!filestream) {
    throw std::runtime_error("File open failed");
  }
  
  
  for (std::size_t i=0; i<vector.size(); i++)
  {
      filestream << vector[i] << std::endl;
      if (filestream.fail())
        throw std::runtime_error("Error writing to file!");
    
  }
}


#endif