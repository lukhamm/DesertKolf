# lugame library

This is my personal game library that makes my daily c++ and 
allegro live easier. As such, it is imagined as a project that
grows over time. At times it might therefore not be the most 
efficient. 

## Features
<!--* ArgHandler: an argument handler similar to pythons 
argparse (with (at the moment) less functionalities)

* StringManipulation: functions for handy string 
manipulations (trim, replace, etc.)

* SimpleTiming: a class for simple timing measurements
using chrono

* Matrix: a matrix object-->

## Getting Started

### Prerequisites
The following prerequisites are necessary to compile library
* Compiler with (at least) C++14
* CMake (version > 3.1)
* lupriv library

### Installing
The library is work in progress and comes with a simple CMake installer.
At the moment it is only tested for linux systems. 

#### Linux
Install globally if you have sudo-permission:
* Download library
* Go to installation folder (e.g. mypath/)
* Do the following steps:

``` bash
mkdir build
cd build/
cmake ..
make
sudo make install
```

### Compiling
If installing was successful the lugame library can simply be included
with the `-llugame` keyword.

### Using
...

## Test
No test cases are supplied at the moment. Will add them later, though.

## Versioning
I use [SemVer](http://semver.org/) for versioning.

## Authors
* Lukas Hammerschmidt - Initial work - [WebPage](https://lukhamm.gitlab.io/)

No contributers yet. If you want to contribute by adding your own
functionalities, speeding up the code, testing or another way,
please don't hesitate to contact me.

## License Note
This project is licensed under the GNU Lesser General Public
License - see the [LICENSE.md](https://gitlab.com/lukhamm/lupriv/blob/master/LICENSE.md)
file for details.

Copyright (C) 2017, Lukas Hammerschmidt.

## Acknowledgements
* Everyone!
