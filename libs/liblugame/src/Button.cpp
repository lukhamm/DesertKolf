/* 
 * This file is part of liblugame ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#include "lugame/gui/Button.hpp"

#include <allegro5/allegro.h>

#include "lugame/gui/styles/Primitive.hpp"
#include "lugame/utils/EventManager.hpp"
#include "lugame/utils/Event.hpp"
#include "lugame/engines2d/Collision.hpp"
#include "lugame/utils/EventCodes.hpp"

lugame::gui::Button::Button(
  const std::string& label,
  const std::string& text,
  const float width, 
  const float height,
  const style::Style* stylePtr
) 
: Widget(label) 
{
  m_type = WIDGET_TYPE::BUTTON;
  m_width = width; 
  m_height = height;
  m_text = text;
  m_stylePtr = stylePtr;
  
  m_buttonClickedLeftId = SDBMHash(m_label+"_clickedLeft");
  lugame::EventManager::getInstance().registerEvent(m_buttonClickedLeftId);
  m_buttonReleasedLeftId = SDBMHash(m_label+"_releasedLeft");
  lugame::EventManager::getInstance().registerEvent(m_buttonReleasedLeftId);
  
  lugame::EventManager::getInstance().attachEvent(lugame::eventid::mouseMove, *static_cast<EventHandler*>(this));
  lugame::EventManager::getInstance().attachEvent(lugame::eventid::mouseClickLeft, *static_cast<EventHandler*>(this));
  lugame::EventManager::getInstance().attachEvent(lugame::eventid::mouseReleaseLeft, *static_cast<EventHandler*>(this));
  
  m_name = "Button: " + label;
};

lugame::gui::Button::~Button()
{
  lugame::EventManager::getInstance().detachEvent(lugame::eventid::mouseMove, *static_cast<EventHandler*>(this));
  lugame::EventManager::getInstance().detachEvent(lugame::eventid::mouseClickLeft, *static_cast<EventHandler*>(this));
  lugame::EventManager::getInstance().detachEvent(lugame::eventid::mouseReleaseLeft, *static_cast<EventHandler*>(this));
}

// template <make sure that this is a style passed in here...
void 
lugame::gui::Button::draw(
) const
{
  m_stylePtr->draw(static_cast<const Widget*>(this));
};
  

const std::string& 
lugame::gui::Button::getText(
) const
{
  return m_text;
};


void 
lugame::gui::Button::handleEvent( 
  const lugame::Event* pEvent 
)
{
  if (pEvent->getId() == lugame::eventid::mouseMove)
    update();
  if (pEvent->getId() == lugame::eventid::mouseClickLeft)
    mouseClickUpdate();
  if (pEvent->getId() == lugame::eventid::mouseReleaseLeft)
    mouseReleaseUpdate();
}

void
lugame::gui::Button::update(
)
{
  ALLEGRO_MOUSE_STATE state;
  al_get_mouse_state(&state);
  if( lugame::collision::point_rectangle(state.x, state.y, getPosX(), getPosY(), getPosX()+getWidth(), getPosY()+getHeight()) )
    m_buttonState = BUTTON_STATE::MOUSE_OVER;
  else
    m_buttonState = BUTTON_STATE::DEFAULT;
}

void 
lugame::gui::Button::mouseClickUpdate(
)
{
//   ALLEGRO_MOUSE_STATE state;
//   al_get_mouse_state(&state);
  if (m_buttonState == BUTTON_STATE::MOUSE_OVER)
  {
    lugame::EventManager::getInstance().sendEvent(m_buttonClickedLeftId);
  }
}

void 
lugame::gui::Button::mouseReleaseUpdate(
)
{
  if (m_buttonState == BUTTON_STATE::MOUSE_OVER)
  {
    lugame::EventManager::getInstance().sendEvent(m_buttonReleasedLeftId);
  }
}

lugame::gui::Button::BUTTON_STATE
lugame::gui::Button::getState(
) const
{
  return m_buttonState;
}