/* 
 * This file is part of liblugame ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#include "../include/engines2d/SpriteAnimation.hpp"


SpriteAnimation::SpriteAnimation()
{
  active = false;
}

void 
SpriteAnimation::init(
  const int rows, 
  const int columns, 
  const int startRow, 
  const int startColumn, 
  const int direction, 
  const int delay, 
  const int frameWidth, 
  const int frameHeight)
{
  m_frameDelay          = delay;
  m_frameDir            = direction;
  m_currentColoumn      = startColumn;
  m_currentRow          = startRow;
  m_maxColumn           = columns;
  m_maxRow              = rows;
  m_frameWidth          = frameWidth;
  m_frameHeight         = frameHeight;
  m_frameCount          = 0;
}

void 
SpriteAnimation::update(
  ALLEGRO_EVENT ev
)
{
  if (ev.type == ALLEGRO_EVENT_TIMER)
  {
    if (m_active)
    {
      if (m_frameCount++ >= m_frameDelay)
      {
        m_frameCount = 0;
        m_currentColoumn+=m_frameDir;
      }
    }
  }
}

void SpriteAnimation::start(
  const bool activate, 
  const int frameDir, 
  const int currentRow
)
{
  m_active = activate;
  m_frameDir = frameDir;
  m_currentRow = currentRow;
}

void 
SpriteAnimation::stop(
  const bool activate, 
  const int stopColumn
)
{
  m_active = activate;
  m_currentColoumn = stopColumn;
}

int 
SpriteAnimation::s_x(
) const
{
  return (m_currentColoumn % m_maxColumn) * m_frameWidth;
}

int 
SpriteAnimation::s_y(
) const
{
  return m_currentRow * m_frameHeight;
}