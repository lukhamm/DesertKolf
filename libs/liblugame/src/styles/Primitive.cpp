/* 
 * This file is part of liblugame ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#include "lugame/gui/styles/Primitive.hpp"

#include "lugame/gui/Button.hpp"

#include <allegro5/allegro_primitives.h>


#include <exception>
#include <string>

lugame::gui::style::Primitive::Primitive(
)
: Primitive(20)
{
}

lugame::gui::style::Primitive::Primitive(
  const int fontSize
)
{
  m_buildPath = al_get_standard_path(ALLEGRO_RESOURCES_PATH);
  if (m_buildPath == nullptr)
    throw std::runtime_error(std::string(__func__) + " : Could not open standard path --> ALLEGRO_RESOURCES_PATH");
  
  al_drop_path_tail(m_buildPath);
  al_append_path_component(m_buildPath, "resources");
  al_append_path_component(m_buildPath, "Style_Primitive");
  al_append_path_component(m_buildPath, "fonts");
  al_append_path_component(m_buildPath, "bitstream_vera_sans_font_6016");
  al_set_path_filename(m_buildPath, "Vera.ttf");
  
  std::string filename = al_path_cstr(m_buildPath,ALLEGRO_NATIVE_PATH_SEP);
  
  if (not al_filename_exists(filename.c_str()))
    throw std::runtime_error(std::string(__func__) + " : Couldn't find resources in: " + filename);
  
  // TODO:: the font size needs to be calculated when the widget is created! 
  m_font = al_load_ttf_font(filename.c_str(), fontSize, 0);
}

lugame::gui::style::Primitive::~Primitive(
)
{
  if (m_buildPath != nullptr)
    al_destroy_path(m_buildPath);
  if (m_font)
    al_destroy_font(m_font);
}

void 
lugame::gui::style::Primitive::draw(
  const lugame::gui::Widget* widgetPtr
) const
{
  switch (widgetPtr->getType())
  {
    case (WIDGET_TYPE::BUTTON):
      drawButton(widgetPtr);
      break;
    default:
      break;
  }
}



bool 
lugame::gui::style::Primitive::cb_fun(
  int line_num, 
  const char* /*line*/, 
  int /*size*/,
  void* extra
)
{
  *static_cast<int*>(extra) = line_num;
  return true;
};


void 
lugame::gui::style::Primitive::drawButton(
  const lugame::gui::Widget* widgetPtr
) const 
{
  const lugame::gui::Button* buttonPtr = static_cast<const lugame::gui::Button*>(widgetPtr);
  
  if (buttonPtr->getState() == lugame::gui::Button::BUTTON_STATE::DEFAULT)
  {
  
    al_draw_filled_rectangle(buttonPtr->getPosX(), 
                            buttonPtr->getPosY(), 
                            buttonPtr->getPosX()+buttonPtr->getWidth(), 
                            buttonPtr->getPosY()+buttonPtr->getHeight(), 
                            al_map_rgb(255,255,255));
    
  //   std::string buttonText = "Hier muss noch Button Text hin...";

    int line_number = 0;
    
    al_do_multiline_text(m_font, 
                        buttonPtr->getWidth(), 
                        buttonPtr->getText().c_str(), 
                        cb_fun, 
                        &line_number);
    
    al_draw_multiline_text(m_font, 
                          al_map_rgb(0,0,0), 
                          buttonPtr->getPosX()+buttonPtr->getWidth()/2., 
                          buttonPtr->getPosY()+buttonPtr->getHeight()/2.-al_get_font_line_height(m_font)*(line_number+1)/2., 
                          buttonPtr->getWidth(), 
                          0, 
                          ALLEGRO_ALIGN_CENTRE, 
                          buttonPtr->getText().c_str());
  }
  else
  {
    al_draw_filled_rectangle(buttonPtr->getPosX(), 
                            buttonPtr->getPosY(), 
                            buttonPtr->getPosX()+buttonPtr->getWidth(), 
                            buttonPtr->getPosY()+buttonPtr->getHeight(), 
                            al_map_rgb(0,0,0));
    
  //   std::string buttonText = "Hier muss noch Button Text hin...";

    int line_number = 0;
    
    al_do_multiline_text(m_font, 
                        buttonPtr->getWidth(), 
                        buttonPtr->getText().c_str(), 
                        cb_fun, 
                        &line_number);
    
    al_draw_multiline_text(m_font, 
                          al_map_rgb(255,255,255), 
                          buttonPtr->getPosX()+buttonPtr->getWidth()/2., 
                          buttonPtr->getPosY()+buttonPtr->getHeight()/2.-al_get_font_line_height(m_font)*(line_number+1)/2., 
                          buttonPtr->getWidth(), 
                          0, 
                          ALLEGRO_ALIGN_CENTRE, 
                          buttonPtr->getText().c_str());
  }
  // TODO: Ändern per update... update manager
}

