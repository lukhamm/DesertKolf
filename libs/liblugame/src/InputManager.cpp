/* 
 * This file is part of liblugame ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#include "lugame/utils/InputManager.hpp"


namespace lugame {
  
  bool 
  InputManager::isKeyPressed(
    ALLEGRO_EVENT ev, 
    const int key
  ) const
  {
    if (ev.type == ALLEGRO_EVENT_KEY_DOWN)
    {
      if (ev.keyboard.keycode == key)
        return true;
    }
    return false;
  }
  
  bool 
  InputManager::isKeyPressed(
    ALLEGRO_EVENT ev, 
    const std::vector<int>& keys
  ) const
  {
    if (ev.type == ALLEGRO_EVENT_KEY_DOWN)
    {
      for (std::size_t i = 0; i < keys.size(); i++)
      {
        if (ev.keyboard.keycode == keys[i])
          return true;
      }
    }
    return false;
  };
  
  bool 
  InputManager::isKeyReleased(
    ALLEGRO_EVENT ev, 
    const int key
  ) const
  {
    if (ev.type == ALLEGRO_EVENT_KEY_UP)
    {
      if (ev.keyboard.keycode == key)
        return true;
    }
    return false;
  }
  
  bool 
  InputManager::isKeyReleased(
    ALLEGRO_EVENT ev, 
    const std::vector<int>& keys
  ) const
  {
    if (ev.type == ALLEGRO_EVENT_KEY_UP)
    {
      for (std::size_t i = 0; i < keys.size(); i++)
      {
        if (ev.keyboard.keycode == keys[i])
          return true;
      }
    }
    return false;
  }
  
  bool 
  InputManager::isMousePressed(
    ALLEGRO_EVENT ev, 
    const std::vector<unsigned int>& mouseKeys
  ) const
  {
    if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
    {
      for (std::size_t i = 0; i < mouseKeys.size(); i++)
      {
        if (ev.mouse.button == mouseKeys[i])
          return true;
      }
    }
    return false;
  }
  
  
  bool 
  InputManager::isMousePressed(
    ALLEGRO_EVENT ev, 
    const unsigned int mouseKey
  ) const
  {
    if(ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
    {
      if (ev.mouse.button == mouseKey)
        return true;
    }
    return false;
  }
  
  
  bool 
  InputManager::isMouseReleased(
    ALLEGRO_EVENT ev, 
    const std::vector<unsigned int>& mouseKeys
  ) const
  {
    if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP)
    {
      for (std::size_t i = 0; i < mouseKeys.size(); i++)
      {
        if (ev.mouse.button == mouseKeys[i])
          return true;
      }
    }
    return false;
  }
  
  
  bool 
  InputManager::isMouseReleased(
    ALLEGRO_EVENT ev, 
    const unsigned int mouseKey
  ) const
  {
    if(ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP)
    {
      if (ev.mouse.button == mouseKey)
        return true;
    }
    return false;
  }
  
  
  void 
  InputManager::mousePosition(
    ALLEGRO_EVENT ev, 
    std::array<float,3>& mousePosOut                    // maybe change to float mousePosOut[3]
  ) const
  {
    if (ev.type == ALLEGRO_EVENT_MOUSE_AXES)
    {
      mousePosOut[0] = ev.mouse.x;
      mousePosOut[1] = ev.mouse.y;
      mousePosOut[2] = ev.mouse.z;
    }
  }
}
