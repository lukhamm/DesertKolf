/* 
 * This file is part of liblugame ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#include "lugame/utils/Event.hpp"
#include "lugame/utils/EventHandler.hpp"

#include <algorithm>
#include <iostream>


namespace lugame {
  
  
  Event::Event(
    const uint32_t eventID,
    void* data
  )
  : m_eventID(eventID)
  , m_dataPtr(data)
  {
  }
  
  
  void Event::attachListener(EventHandler& eventHandler)
  {
    m_listeners.push_back(&eventHandler);
  }
  
  
  void Event::detachListener(EventHandler& eventHandler)
  {
    for (auto iter = m_listeners.begin(); iter != m_listeners.end(); ++iter)
    {
      if (&eventHandler == *iter)
      {
        m_listeners.erase(iter);
        break;
      }
    }
  }
  
  
  void 
  Event::send(
  ) const
  {
    for (auto& listener : m_listeners)
    {
      if (listener != nullptr)
      {
        listener->handleEvent(this);
      }
    }
  }
  
  void 
  Event::send(
    void* dataPtr
  )
  {
    m_dataPtr = dataPtr;
    send();
  }
  
  
  void 
  Event::sendToHandler(
    EventHandler& eventHandler
  ) const
  {
    auto found = std::find(m_listeners.begin(), m_listeners.end(), &eventHandler);
    if (found != m_listeners.end())
    {
      (*found)->handleEvent(this);
    }
  }
  
}