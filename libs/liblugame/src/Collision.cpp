/* 
 * This file is part of liblugame ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#include "lugame/engines2d/Collision.hpp"

#include <lupriv/Matrix.hpp>

namespace lugame {
  namespace collision {

    bool 
    circle_rectangle(
      const float& circle_x, 
      const float& circle_y, 
      const float& radius,
      const float& rectangle_x1, 
      const float& rectangle_y1, 
      const float& rectangle_x2, 
      const float& rectangle_y2
    )
    {
      lulib::math::Matrix<float> closestPointOnRec(2), circleCenter(2);
      circleCenter[0] = circle_x;
      circleCenter[1] = circle_y;
      closestPointOnRec[0] = clamp<float>(circle_x, rectangle_x1, rectangle_x2);
      closestPointOnRec[1] = clamp<float>(circle_y, rectangle_y1, rectangle_y2);
      
      if ( (closestPointOnRec-circleCenter).length() <= radius )
        return true;
      else
        return false;
    }
    
    
    
    bool 
    lineSegCircleOffset(
      const float line_x1, 
      const float line_y1, 
      const float line_x2,
      const float line_y2, 
      const float circle_x, 
      const float circle_y, 
      const float radius,
      lulib::math::Matrix<float>& offset
    )
    {
      lulib::math::Matrix<float> lvec(2), kvec(2), CP(2), dvec(2);
      float lvecl, p, d;
      
      lvec[0] = line_x2 - line_x1;
      lvec[1] = line_y2 - line_y1;
      kvec[0] = circle_x - line_x1;
      kvec[1] = circle_y - line_y1;
      
      lvecl = lvec.length();

      // get the projection p onto lvec:
      p = (lvec.transpose()*kvec)[0] / lvecl;// f_std_sprod(lvec, kvec)/lvecl;
      
      if ( p < 0 )      // the circle is left of x1,y1
      {
        CP[0] = line_x1;        // CP = collision point (possibly left corner)
        CP[1] = line_y1;
      }
      else if ( p > lvecl )
      {
        CP[0] = line_x2;        // CP = collision point (possibly right corner)
        CP[1] = line_y2;
      }
      else
      {
        // the projection is on the line, such that the collision point is on the line
        CP[0] = (lvec[0]/lvecl)*p + line_x1;
        CP[1] = (lvec[1]/lvecl)*p + line_y1;
      }
      
      // vector pointing from the potential collision point to the circle
      dvec[0] = CP[0] - circle_x;
      dvec[1] = CP[1] - circle_y;
      d = dvec.length();
      
//       d = f_2d_len_vec(CP[0]-circle_x, CP[1]-circle_y);//sqrt(pow(cpvec[0]-circle_x,2) + pow(cpvec[1]-circle_y,2));
//       dvec[0] = CP[0] - circle_x;
//       dvec[1] = CP[1] - circle_y;

      if ( d <= radius )
      {
         offset = ((dvec*-1)/d)*radius + CP;
//         offset[0] = (-dvec[0]/d)*radius+CP[0];
//         offset[1] = (-dvec[1]/d)*radius+CP[1];
        return true;
      }
      else
        return false;
    }
    
    
    bool 
    circle_lineSegment(
      const float line_x1,
      const float line_y1,
      const float line_x2,
      const float line_y2,
      const float circle_x, 
      const float circle_y, 
      const float radius
    )
    {
      lulib::math::Matrix<float> closestPointOnLine(2), circleCenter(2);
      circleCenter[0] = circle_x;
      circleCenter[1] = circle_y;
      closestPointOnLine[0] = clamp<float>(circle_x, line_x1, line_x2);
      closestPointOnLine[1] = clamp<float>(circle_y, line_y1, line_y2);
      
      if ( (closestPointOnLine-circleCenter).length() < radius )
        return true;
      else
        return false;
    }
    
  }
}



