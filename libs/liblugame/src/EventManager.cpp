/* 
 * This file is part of liblugame ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#include "lugame/utils/EventManager.hpp"

#include <utility>

namespace lugame {
  
  EventManager::EventManager(
  )
  {
  }
  
  EventManager::~EventManager(
  )
  {
    for (auto& event : m_events)
    {
      Event* pEvent = event.second;
      if (pEvent)
      {
        delete pEvent;
        event.second = nullptr;
      }
    }
    m_events.clear();
  }
  
  EventManager&
  EventManager::getInstance(
  )
  {
    static EventManager instance;
    return instance;
  }
  
  bool 
  EventManager::registerEvent(
    uint32_t eventId
  )
  {
    bool added = false;
    
    auto result = m_events.find(eventId);
    if (result == m_events.end())
    {
      Event* pNewEvent = new Event(eventId);
      
      if (pNewEvent)
      {
        std::pair<uint32_t, Event*> newEvent(eventId, pNewEvent);
        auto addedIter = m_events.insert(newEvent);
        added = addedIter.second;
      }
    }
    else
    {
      added = result->second;
    }
    assert(added);
    return added;
  }
  
  
  void
  EventManager::attachEvent(
    uint32_t eventId, 
    EventHandler& eventHandler
  )
  {
    auto result = m_events.find(eventId);
    assert(result != m_events.end());
    
    if (result != m_events.end())
    {
      assert(result->second);
      result->second->attachListener(eventHandler);
    }
  }
  
  
  void
  EventManager::detachEvent(
    uint32_t eventId,
    EventHandler& eventHandler
  )
  {
    auto result = m_events.find(eventId);
    assert(result != m_events.end());
    if (result != m_events.end())
    {
      assert(result->second);
      result->second->detachListener(eventHandler);
    }
  }
  
  
  void
  EventManager::sendEvent(
    uint32_t eventId
  ) const
  {
    auto result = m_events.find(eventId);
    if (result != m_events.end())
    {
      assert(result->second);
      if (result->second)
      {
        result->second->send();
      }
    }
  }
  
  
  void
  EventManager::sendEvent(
    uint32_t eventId,
    void* dataPtr
  ) 
  {
    auto result = m_events.find(eventId);
    if (result != m_events.end())
    {
      assert(result->second);
      if (result->second)
      {
        result->second->send(dataPtr);
      }
    }
  }
  
  
  void
  EventManager::sendEventToHandler(
    uint32_t eventId,
    EventHandler& eventHandler
  ) const
  {
    auto result = m_events.find(eventId);
    if (result != m_events.end())
    {
      assert(result->second);
      if (result->second)
      {
        result->second->sendToHandler(eventHandler);
      }
    }
  }
  
}