/* 
 * This file is part of liblugame ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#include "lugame/gui/Box.hpp"

#include <iostream>

#include <allegro5/allegro.h>

#include "lugame/utils/Event.hpp"
#include "lugame/utils/EventManager.hpp"
#include "lugame/engines2d/Collision.hpp"
#include "lugame/utils/EventCodes.hpp"


lugame::gui::Box::Box(
  const std::string& label,
  const float& posX,
  const float& posY,
  const ORIENTATION& orientation, 
  const float& separation
) 
: Widget(label)
{
  m_type = WIDGET_TYPE::BOX;
  m_posX = posX; 
  m_posY = posY;
  
  if (orientation == ORIENTATION::VERTICAL)
    m_lastPos = posY;
  else
    m_lastPos = posX;
  
  m_separation = separation;
  m_orientation = orientation;
  
  lugame::EventManager::getInstance().attachEvent(lugame::eventid::mouseMove, 
                                                  *static_cast<EventHandler*>(this));
  
//   init(posX, posY, orientation, separation);
  
  m_name = "Box: " + label;
}
// lugame::gui::Box::Box(
//   const uint32_t& id,
//   const float& posX, 
//   const float& posY,
//   const ORIENTATION& orientation, 
//   const float& separation
// ) 
// : Widget(id)
// {
//   init(posX, posY, orientation, separation);
// };

// void 
// lugame::gui::Box::init(
//   const float& posX,
//   const float& posY,
//   const ORIENTATION& orientation, 
//   const float& separation
// )
// {
//   
//   lugame::attachEvent(lugame::eventid::mouseMove, this);
// };


lugame::gui::Box::~Box(
)
{
  lugame::EventManager::getInstance().detachEvent(lugame::eventid::mouseMove, 
                                                  *static_cast<EventHandler*>(this));
  destroy();
};


void 
lugame::gui::Box::destroy(
)
{
}
  

void 
lugame::gui::Box::draw(
) const
{
  for (auto pair : content)
  {
    pair.second->draw();
  }
};


bool 
lugame::gui::Box::erase(
  const std::string& idName
)
{
  uint32_t id = SDBMHash(idName);
  erase(id);
  return true;
}
bool 
lugame::gui::Box::erase(
  const uint32_t id
)
{
  bool erased = false;
  float eraseSize = 0;
  
  auto mapIter = content.find(id);
  if (mapIter != content.end())
  {
    if (m_orientation == ORIENTATION::VERTICAL)
      eraseSize = mapIter->second->getHeight() + m_separation;
    else
      eraseSize = mapIter->second->getWidth() + m_separation;
    
    m_lastPos -= eraseSize;
    mapIter->second->destroy();
    content.erase(mapIter);
    erased = true;
  }
  assert(erased && "Couldn't erase an element in box.");
  
  // shift of all elements after the erased one
  for (auto& pair : content)
  {
    if (m_orientation == ORIENTATION::HORIZONTAL)
    {
      if (pair.second->getPosX()+pair.second->getHeight() > m_lastPos)
        pair.second->setPosX(pair.second->getPosX() - eraseSize);
    }
    else if(m_orientation == ORIENTATION::VERTICAL)
    {
      if (pair.second->getPosY() > m_lastPos)
        pair.second->setPosY(pair.second->getPosY() - eraseSize);
    }
  }
  return true;
}


void 
lugame::gui::Box::update()
{
  ALLEGRO_MOUSE_STATE state;
  al_get_mouse_state(&state);
  m_is_inside_box = lugame::collision::point_rectangle(state.x, state.y, getPosX(), getPosY(), getPosX()+getWidth(), getPosY()+getHeight());
//   if (m_is_inside_box)
//     std::cout << "Inside box!!" << state.x << "  " << state.y << std::endl;
}


void 
lugame::gui::Box::handleEvent( 
  const lugame::Event* pEvent 
)
{
  if (pEvent->getId() == lugame::eventid::mouseMove)
    update();
}

