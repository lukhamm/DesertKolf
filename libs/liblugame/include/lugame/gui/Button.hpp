/* 
 * This file is part of liblugame ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef LUGAME_GUI_BUTTON_HPP
#define LUGAME_GUI_BUTTON_HPP

#include "Widget.hpp"

#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>

#include <string>
#include <iostream> // for testing delete later

#include "styles/Style.hpp"

// class Style;

namespace lugame {
  class Event;
  class EventManager;
}

namespace lugame {
  namespace gui {
    class Button;
  }
}


class lugame::gui::Button : public lugame::gui::Widget {
public:
  enum class BUTTON_STATE { DEFAULT, MOUSE_OVER };
  
  Button(
    const std::string& label,
    const std::string& text,
    const float width, 
    const float height,
    const style::Style* stylePtr
  );

  virtual ~Button();
  
  virtual void 
  draw(
  ) const override;
  
  const std::string&
  getText(
  ) const;
  
  virtual void 
  handleEvent( 
    const lugame::Event* pEvent 
  ) override;
  
  void 
  update(
  );
  
  void 
  mouseClickUpdate(
  );
  void 
  mouseReleaseUpdate(
  );
  
  BUTTON_STATE 
  getState(
  ) const;

private:

  const style::Style* m_stylePtr{nullptr};
  
  std::string m_text;
  
  BUTTON_STATE m_buttonState{BUTTON_STATE::DEFAULT};
  int32_t m_buttonClickedLeftId;
  int32_t m_buttonReleasedLeftId;
};

#endif