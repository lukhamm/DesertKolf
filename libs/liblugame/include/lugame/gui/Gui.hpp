/* 
 * This file is part of liblugame ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef LIBLUGAME_GUI_HPP
#define LIBLUGAME_GUI_HPP

#include "../utils/EventManager.hpp"
#include "../utils/EventCodes.hpp"
#include "../utils/InputManager.hpp"

#include <allegro5/allegro.h>

namespace lugame {
  namespace gui {
    class Gui;
  }
}



class lugame::gui::Gui {
public:

  Gui(
  )
  {
    lugame::EventManager::getInstance().registerEvent(lugame::eventid::mouseMove);
    lugame::EventManager::getInstance().registerEvent(lugame::eventid::mouseClickLeft);
    lugame::EventManager::getInstance().registerEvent(lugame::eventid::mouseClickRight);
    lugame::EventManager::getInstance().registerEvent(lugame::eventid::mouseReleaseLeft);
    lugame::EventManager::getInstance().registerEvent(lugame::eventid::mouseReleaseRight);
  };
  
  /*
   * Needs to be called in EventQueue in Main
   * 
   */
  void
  update(
    ALLEGRO_EVENT ev
  )
  {
    if (inputManager.isMousePressed(ev,1))
      lugame::EventManager::getInstance().sendEvent(lugame::eventid::mouseClickLeft);
    
    if (inputManager.isMouseReleased(ev,1))
      lugame::EventManager::getInstance().sendEvent(lugame::eventid::mouseReleaseLeft);
    
    if (inputManager.isMousePressed(ev,2))
      lugame::EventManager::getInstance().sendEvent(lugame::eventid::mouseClickRight);
    
    if (inputManager.isMouseReleased(ev,2))
      lugame::EventManager::getInstance().sendEvent(lugame::eventid::mouseReleaseRight);
    
    switch (ev.type)
    {
      case ALLEGRO_EVENT_MOUSE_AXES:
        lugame::EventManager::getInstance().sendEvent(lugame::eventid::mouseMove);
    }
  };
  
  
private:
  InputManager inputManager;
};



#endif