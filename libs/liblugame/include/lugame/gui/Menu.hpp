/* 
 * This file is part of liblugame ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef LIBLUGAME_GUI_MENU_HPP
#define LIBLUGAME_GUI_MENU_HPP

#include <vector>
#include <string>

#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>

#include "../engines2d/Collision.hpp"
#include "../utils/InputManager.hpp"


namespace lugame {

  class Menu {
  public:
    
    Menu();
    
    void 
    load(
      const float x, const float y, 
      const std::vector< std::string >& menu_text,
      const std::vector< std::string >& menu_link,
      const std::string& input_font, 
      const unsigned int font_size = 10, 
      std::vector<int> input_colors = { 255, 255, 255, 200, 200, 200, 140, 140, 140 }
    );
    
    void
    unload();
    
    void
    draw();
    
    void
    update(
      ALLEGRO_EVENT ev, 
      const int alpha
    );
    
    void
    position(
      const float x, 
      const float y
    );
    
    bool
    isMouseInsideMenu(
    ) const;
    
    bool linkEvent;
    std::string link;

  private:
    InputManager inputManager;
    
    ALLEGRO_FONT* font;
    
    float m_menuDimensions[2];
    float m_posX, m_poxY;
    float m_mouseX, m_mouseY;
    int m_fontHeight, m_fontWidth;
    int m_alpha;
    std::vector<int> rgb;
    
    struct MenuPointsStruct {
      int m_fontWidth;
      std::string m_text, m_link;
      bool m_active;
    };
    std::vector<MenuPointsStruct> menuPoints;
    
    
  };

}


#endif