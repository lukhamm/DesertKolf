/* 
 * This file is part of liblugame ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef LUGAME_GUI_STYLE_PRIMITIVE_HPP
#define LUGAME_GUI_STYLE_PRIMITIVE_HPP

#include "Style.hpp"

#include <iostream>                     // TODO delete
#include <type_traits>

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>

namespace lugame {
  namespace gui {
    class Widget;
  }
}

namespace lugame {
  namespace gui {
    namespace style {
      class Primitive;
    }
  }
}

// enum class WIDGET_TYPE;
// class lugame::gui::Widget;

class lugame::gui::style::Primitive : public lugame::gui::style::Style {
public:
  Primitive();
  Primitive(const int fontSize);
  virtual ~Primitive();
  
  virtual void 
  draw(
    const lugame::gui::Widget* widgetPtr
  ) const override;

private:
  virtual void 
  drawButton(
    const lugame::gui::Widget* widgetPtr
  ) const override;
  
  ALLEGRO_PATH* m_buildPath{nullptr};
  ALLEGRO_FONT* m_font{nullptr};
  
  static bool 
  cb_fun(
    int line_num, 
    const char* line, 
    int size,
    void* extra
  );
};





#endif