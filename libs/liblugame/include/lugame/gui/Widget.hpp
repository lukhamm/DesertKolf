/* 
 * This file is part of liblugame ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef LIBLUGAME_GUI_WIDGET_HPP
#define LIBLUGAME_GUI_WIDGET_HPP

#include <string>
#include <iostream>

#include "../utils/EventHandler.hpp"
#include "../utils/SDBMHash.hpp"

namespace lugame {
  namespace gui {
    namespace style {
      class Style;
    }
  }
}

namespace lugame {
  namespace gui {
    class Widget;
  }
}

namespace lugame {
    class Event;
    class EventManager;
}


enum class WIDGET_TYPE { 
  BUTTON, 
  BOX
};


class lugame::gui::Widget : public lugame::EventHandler {
public:
  
  Widget(
    const std::string& name
  )
  {
    m_label = name;
    m_id = SDBMHash(name);
  };

  virtual ~Widget(
  )
  {};
  
  inline bool 
  makeClickable(
  )
  {m_clickable = true; return true;}
  
  inline bool 
  isClickable(
  )
  {return m_clickable;}
  
  inline float 
  getPosX(
  ) const
  {return m_posX;}
  
  inline float 
  getPosY(
  ) const
  {return m_posY;}
  inline float 
  getWidth(
  ) const
  { return m_width; }
  inline float 
  getHeight(
  ) const
  { return m_height; }
  inline uint32_t 
  getId(
  ) const
  { return m_id; }
  
  inline void 
  setPosX(
    const float& posX
  )
  {m_posX = posX;}
  
  inline void 
  setPosY(
    const float& posY
  )
  {m_posY = posY;}
  
  inline WIDGET_TYPE 
  getType(
  ) const
  {return m_type;};
  
  virtual void 
  draw(
  ) const
  {};
  
  virtual void 
  destroy(
  )
  {};
  
  virtual void 
  handleEvent(
    const lugame::Event* /*pEvent */
  )
  {};
  
protected:
  
  Widget(
    const uint32_t& id
  )
  {
    m_id = id;
  };
  
  float m_posX{0}, m_posY{0};
  float m_width{0}, m_height{0};
  bool m_clickable{false};                        // can be clicked
  bool m_visible{true};                           // will be drawn
  
  std::string m_label;                            // unique name of the widget TODO add a static map that keeps track of that
  
  uint32_t m_id{0};
  WIDGET_TYPE m_type;                             // needs to be initialized
  
  
private:
};


#endif