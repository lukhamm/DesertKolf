/* 
 * This file is part of liblugame ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef LIBLUGAME_GUI_BOX_HPP
#define LIBLUGAME_GUI_BOX_HPP

#include <iostream>
#include <unordered_map>
#include <cassert>
#include <utility>
#include <type_traits>

#include "Widget.hpp"
#include "../utils/EventHandler.hpp"


namespace lugame {
  class Event;
  class EventManager;
}

namespace lugame {
  namespace gui {
    class Box;
    
  }
}

class lugame::gui::Box : public lugame::gui::Widget {
public:
  
  enum class ORIENTATION {
    HORIZONTAL,
    VERTICAL
  };
  
  /*
   * Constructor
   */
  Box(
    const std::string& label, 
    const float& posX,
    const float& posY,
    const ORIENTATION& orientation = ORIENTATION::VERTICAL,
    const float& separation = 0
  );

  /*
   * Destructor
   */
  virtual ~Box(
  );
  
  
  /*
   * Destroy
   */
  virtual void 
  destroy(
  ) override;
  
  
  /*
   * Calls draw of all Widget in the map
   */
  virtual void 
  draw(
  ) const override;
  
  
  /*
   * Insert a Widget into the box
   */
  template <typename T>
  bool 
  insert(
    T* pWidget
  );
  
  /*
   * Erase
   */
  bool 
  erase(
    const std::string& id_name
  );
  bool 
  erase(
    const uint32_t id
  );
  
  /*
   * Update
   */
  void 
  update();
  
  virtual void 
  handleEvent( 
    const lugame::Event* pEvent 
  ) override;
  
  
protected:
  
private:
  
  std::unordered_map<uint32_t, Widget*> content;
  float m_separation{0};
  float m_lastPos{m_posY};
  ORIENTATION m_orientation;
  bool m_is_inside_box{false};
};





template <typename T>
bool 
lugame::gui::Box::insert(
  T* pWidget
)
{
  static_assert( std::is_base_of<Widget, T>::value, "Insert to box has to be of base Widget." );
  
  // update widget position
  if (m_orientation == ORIENTATION::VERTICAL)
  {
    pWidget->setPosX(m_posX);
    pWidget->setPosY(m_lastPos);
    
    // update size of box
    m_lastPos += (m_separation + pWidget->getHeight());
    m_height = m_lastPos - m_posY;
    
    if (pWidget->getWidth() > m_width)
      m_width = pWidget->getWidth();
  }
  else if (m_orientation == ORIENTATION::HORIZONTAL)
  {
    pWidget->setPosX(m_lastPos);
    pWidget->setPosY(m_posY);
    
    // update size of box
    m_lastPos += m_separation + pWidget->getWidth();
    m_width = m_lastPos - m_posX;
    
    if (pWidget->getHeight() > m_height)
      m_height = pWidget->getHeight();
  }
  
  // push into map
  Widget* widgetPtr = nullptr;
  widgetPtr = static_cast<Widget*>(pWidget);
  
  bool added = false;
  auto mapIter = content.find(widgetPtr->getId());
  if (mapIter == content.end())
  {
    content.insert(std::pair<uint32_t, Widget*>(widgetPtr->getId(), widgetPtr));
    added = true;
  }
  assert(added);
  return true;
}




#endif