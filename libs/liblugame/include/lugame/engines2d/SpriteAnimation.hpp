/* 
 * This file is part of liblugame ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef LIBLUGAME_ENGINE_SPRITEANIMATION_HPP
#define LIBLUGAME_ENGINE_SPRITEANIMATION_HPP

#include <utility>
#include <allegro5/allegro.h>

class SpriteAnimation {
  
public:
  
  SpriteAnimation(
  );
  
  void 
  init(
    const int rows, 
    const int columns, 
    const int startRow, 
    const int startColumn, 
    const int direction, 
    const int delay, 
    const int frameWidth, 
    const int frameHeight
  );
  
  void 
  unloadContent(
  ){};
  
  void
  draw(
  ){};
  
  void 
  update(
    ALLEGRO_EVENT ev
  );
  
  void
  anim(
    const bool activate, 
    const int frameDir, 
    const int currentRow
  );
  
  void
  anim(
    const bool activate, 
    const int breakColumn
  );
  
  int s_x(
  ) const;
  int s_y(
  ) const;
  
  std::pair<int,int> sprite();
  
  int 
  getStatus(
  )
  {return m_currentColoumn;};
  
private:
  bool m_active;
  
  int m_maxColumn;
  int m_maxRow;
  int m_currentRow;
  int m_currentColoumn;
  int m_frameDir;
  int m_frameDelay;
  int m_frameSpeed;
  int m_frameCount;
  int m_frameWidth;
  int m_frameHeight;
  
  int m_sx, m_sy;
};



#endif