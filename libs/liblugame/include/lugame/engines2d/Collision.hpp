/* 
 * This file is part of liblugame ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef LIBLUGAME_ENGINES2D_COLLISION_HPP
#define LIBLUGAME_ENGINES2D_COLLISION_HPP

#include <lupriv/Matrix.hpp>

#include <cmath>
#include <vector>

namespace lugame {
  namespace collision {
    
    /*
     * clamp
     *
     * -- find closest value for n between lower and upper
     * 
     * 
     */
    template <class T>
    T
    clamp(
      const T& n, 
      const T& lower, 
      const T& upper
    )
    {
      return std::max(lower, std::min(n, upper));
    }
    // or use std::clamp from c++17
    
    /*
     * rectangle - rectanlge :: axis aligned
     * 
     */
    inline bool 
    rect_rect_axis_aligned(
      const float r1x1, 
      const float r1y1,
      const float r1x2, 
      const float r1y2, 
      const float r2x1, 
      const float r2y1,           
      const float r2x2, 
      const float r2y2
    );
    
    
    bool 
    circle_rectangle(
      const float circle_x, 
      const float circle_y, 
      const float radius,
      const float rectangle_x1, 
      const float rectangle_y1, 
      const float rectangle_x2, 
      const float rectangle_y2
    );
    
    
    inline bool
    circle_circle(
      const float c1_x, 
      const float c1_y, 
      const float c1_radius, 
      const float c2_x, 
      const float c2_y, 
      const float c2_radius
    );

    
    inline bool 
    circle_border_rectangle(
      const float border_x1, 
      const float border_y1, 
      const float border_x2, const float& border_y2, 
      const float circle_x, const float& circle_y,   
      const float radius
    );
    
    
    inline bool 
    point_rectangle(
      const float punkt_x, 
      const float punkt_y,
      const float rectangle_x1, 
      const float rectangle_y1, 
      const float rectangle_x2, 
      const float rectangle_y2
    );
    
    
    
    inline bool 
    point_circle(
      const float punkt_x, 
      const float punkt_y,
      const float circle_x, 
      const float circle_y, 
      const float radius
    );
    
    
    inline float 
    angle_axis(
      const float pos_vec_x, 
      const float pos_vec_y
    );
    
    
    bool 
    lineSegCircleOffset(
      const float line_x1, 
      const float line_y1, 
      const float line_x2,
      const float line_y2, 
      const float circle_x, 
      const float circle_y, 
      const float radius,
      lulib::math::Matrix<float>& offset
    );
    
    
    bool 
    circle_lineSegment(
      const float line_x1,
      const float line_y1,
      const float line_x2,
      const float line_y2,
      const float circle_x, 
      const float circle_y, 
      const float radius
    );
    
  }
}



// ----------------------------------------------------------------------------
// Function Definition
// ----------------------------------------------------------------------------


inline bool 
lugame::collision::rect_rect_axis_aligned(
  const float r1x1, 
  const float r1y1,
  const float r1x2, 
  const float r1y2, 
  const float r2x1, 
  const float r2y1,           
  const float r2x2, 
  const float r2y2
)
{
  if (r1x2 > r2x1 && r1x1 < r2x2 && r1y2 > r2y1 && r1y1 < r2y2)
    return true;
  else
    return false;
};


inline bool
lugame::collision::circle_circle(
  const float c1_x, 
  const float c1_y, 
  const float c1_radius, 
  const float c2_x, 
  const float c2_y, 
  const float c2_radius
)
{
  if (sqrt(pow(c1_x - c2_x,2) + pow(c1_y - c2_y,2)) < c1_radius + c2_radius )
    return true;
  else
    return false;
};



inline bool 
lugame::collision::circle_border_rectangle(
  const float border_x1, 
  const float border_y1, 
  const float border_x2, const float& border_y2, 
  const float circle_x, const float& circle_y,   
  const float radius
) 
{
  if (circle_x+radius >= border_x2 || circle_x-radius <= border_x1 ||
      circle_y+radius >= border_y2 || circle_y-radius <= border_y1)
    return true;
  else
    return false;
};


inline bool 
lugame::collision::point_rectangle(
  const float punkt_x, 
  const float punkt_y,
  const float rectangle_x1, 
  const float rectangle_y1, 
  const float rectangle_x2, 
  const float rectangle_y2
)
{
  if (punkt_x > rectangle_x1 && punkt_x < rectangle_x2 && punkt_y > rectangle_y1 && punkt_y < rectangle_y2)
    return true;
  else
    return false;
}


inline bool 
lugame::collision::point_circle(
  const float punkt_x, 
  const float punkt_y,
  const float circle_x, 
  const float circle_y, 
  const float radius
)
{
  if (pow(punkt_x-circle_x,2.0) + pow(punkt_y-circle_y,2.0) <= pow(radius,2.0))
    return true;
  else
    return false;
};


inline float 
lugame::collision::angle_axis(
  const float pos_vec_x, 
  const float pos_vec_y
) 
{
  float winkel = atan2(pos_vec_x, pos_vec_y);
  return (M_PI-winkel);
}




#endif