/* 
 * This file is part of liblugame ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef LUBLIGAME_UTILS_RANDOMNUMBERS_HPP
#define LUBLIGAME_UTILS_RANDOMNUMBERS_HPP

#include <random>

namespace lugame {
  // General template class for random values
  template <class T>
  class RandomNumbers {
  };


  // Specialized template functor for random double values
  template <>
  class RandomNumbers<double> {
  public:
    RandomNumbers(
    ) 
    : mt(dr())
    {
    };
    
    RandomNumbers(
      const double a, 
      const double b
    ) 
    : mt(dr())
    , dist(a,b) 
    {
    };
    
    double 
    operator()(
    )
    {
      return dist(mt);
    };
    
    double 
    operator()(
      const double a, 
      const double b
    )
    {
      std::uniform_real_distribution<double> dist(a,b); 
      return dist(mt);
    }
    
  private:
    std::random_device dr;
    std::mt19937 mt;
    std::uniform_real_distribution<double> dist;
  };


  // Specialized template functor for random real values
  template <>
  class RandomNumbers<float> {
  public:
    RandomNumbers(
    ) 
    : mt(dr())
    {
    };
    
    RandomNumbers(
      const float a, 
      const float b
    ) 
    : mt(dr())
    , dist(a,b)
    {
    };
    
    float
    operator()(
    )
    {
      return dist(mt);
    }
    
    float
    operator()(
      const float a, 
      const float b
    )
    {
      std::uniform_real_distribution<float> dist(a,b); 
      return dist(mt);
    }
    
  private:
    std::random_device dr;
    std::mt19937 mt;
    std::uniform_real_distribution<float> dist;
  };


  // Specialized template functor for random integer values
  template <>
  class RandomNumbers<int> {
    public:
    RandomNumbers(
    ) 
    : mt(dr())
    {
    };
    
    RandomNumbers<int>(
      const int a,
      const int b
    ) 
    : mt(dr())
    , dist(a,b)
    {
    };
    
    int 
    operator()(
    )
    {
      return dist(mt);
    }
    
    int 
    operator()(
      const int a, 
      const int b
    )
    {
      std::uniform_int_distribution<int> dist(a,b); 
      return dist(mt);
    }
    
  private:
    std::random_device dr;
    std::mt19937 mt;
    std::uniform_int_distribution<int> dist;
  };
}

#endif