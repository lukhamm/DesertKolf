/* 
 * This file is part of liblugame ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

/*
 * Singleton Template
 * 
 * -- turns a deriving class into a singleton
 * -- probably a bad pattern ??
 * 
 * Use:
 * -- e.g.   #include "Singleton.hpp"
 *           class MySingClass : public Singleton<MyClass>
 *           {
 *           }
 * 
 *           [...] then MySingClass is called in some code by e.g.:
 *           
 *           void UseSingletonFunction()
 *           {
 *             new MySingClass();
 *             delete MySingClass::getSingletonPtr();
 *           }
 * 
 *           [...] or alternatively as:
 *           void UseSingletonFunction()
 *           {
 *             MySingClass mySingClass;                           // needs to be instantiated once
 *             MySingClass another = MySingClass::getInstance(); 
 *           }
 * 
 * 
 */

#ifndef LIBLUGAME_SINGLETON_HPP
#define LIBLUGAME_SINGLETON_HPP

#include <cassert>

namespace lugame {

  template <typename T>
  class Singleton {
  public:
    // allows for only one instantiation and stops otherwise. <-- does it though?
    
    static T& getInstance()
    {
      assert(m_instance != nullptr);              // needs to be instantiated once
      return *m_instance;
    }
    
    static T* getInstancePtr()
    {
      assert(m_instance != nullptr);              // needs to be instantiated once
      return m_instance;
    }
  protected:
    inline explicit Singleton()
    {
      assert(m_instance == nullptr);
      if (m_instance == nullptr)
      {
        m_instance = static_cast<T*>(this);
      }
    }
    
    virtual ~Singleton()
    {
      m_instance = nullptr;
    }
    
  private:
    
    static T* m_instance;
  };


  // First definition of static member variable
  template <typename T>
  T* Singleton<T>::m_instance = nullptr;

}

#endif