/* 
 * This file is part of liblugame ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef LIBLUGAME_UTILS_INPUTMANAGER_HPP
#define LIBLUGAME_UTILS_INPUTMANAGER_HPP

#include <allegro5/allegro.h>

#include <vector>
#include <array>

namespace lugame {
  
  class InputManager {
  public:
    // mouseKeys :: 1 = left mouse key, 2 = right mouse key, 3 = mouse wheel
    // key       :: ALLEGRO_KEY_XXX
    bool 
    isKeyPressed(
      ALLEGRO_EVENT ev, 
      const int key
    ) const;
    
    bool 
    isKeyPressed(
      ALLEGRO_EVENT ev, 
      const std::vector< int >& keys
    ) const;
    
    bool 
    isKeyReleased(
      ALLEGRO_EVENT ev, 
      const int key
    ) const;
    
    bool 
    isKeyReleased(
      ALLEGRO_EVENT ev, 
      const std::vector< int >& keys
    ) const;
    
    /*
     * Parameter:
     * -- ALLEGRO_EVENT
     * -- mouseKeys / mouseKey [0,1,2] 
     * 
     */
    bool 
    isMousePressed(
      ALLEGRO_EVENT ev, 
      const std::vector<unsigned int>& mouseKeys
    ) const;
    
    bool 
    isMousePressed(
      ALLEGRO_EVENT ev, 
      const unsigned int mouseKey
    ) const;
    
    bool 
    isMouseReleased(
      ALLEGRO_EVENT ev, 
      const std::vector< unsigned int >& mouseKeys
    ) const;
    
    bool 
    isMouseReleased(
      ALLEGRO_EVENT ev, 
      const unsigned int mouseKey
    ) const;
    
    void 
    mousePosition(
      ALLEGRO_EVENT ev, 
      std::array<float,3>& mousePosOut
    ) const;

  private:
//     ALLEGRO_KEYBOARD_STATE keyState;            // braucht es das?
//     std::array<float,3> m;
    
  };
    
}

#endif
