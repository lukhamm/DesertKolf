/* 
 * This file is part of liblugame ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef LIBLUGAME_EVENT_HPP
#define LIBLUGAME_EVENT_HPP

#include <vector>
#include <cstdint>

namespace lugame {

  class EventHandler;

  class Event {
  public:
    explicit Event(
      const uint32_t eventID, 
      void* data = nullptr
    );

    void 
    send(
    ) const;
    
    void 
    send(
      void* dataPtr
    );
    
    void 
    sendToHandler(
      EventHandler& eventHandler
    ) const;
    
    void 
    attachListener(
      EventHandler& eventHandler
    );
    
    void 
    detachListener(
      EventHandler& eventHandler
    );
    
    inline uint32_t 
    getId(
    ) const
    {return m_eventID;};
    
    void* 
    getDataPtr() const
    {
      return m_dataPtr;
    }
    
  private:
    std::vector<EventHandler*> m_listeners;
    const uint32_t m_eventID;
    void* m_dataPtr;
  };

}

#endif