/* 
 * This file is part of liblugame ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef LIBLUGAME_SDBMHASH_HPP
#define LIBLUGAME_SDBMHASH_HPP

#include <cstdint>
#include <string>

namespace lugame {

  template <uint32_t stringLength>
  struct SDBMCalculator
  {
    constexpr static inline uint32_t 
    Calculate(
      const char* const stringToHash, 
      uint32_t& value
    )
    {
      uint32_t character = SDBMCalculator<stringLength - 1>::Calculate(stringToHash, value);
      value = character + (value << 6) + (value << 16) - value;
      return stringToHash[stringLength - 1];
    }

    constexpr static inline uint32_t 
    CalculateValue(
      const char* const stringToHash
    )
    {
      uint32_t value = 0;
      uint32_t character = SDBMCalculator<stringLength>::Calculate(stringToHash, value);
      value = character + (value << 6) + (value << 16) - value;
      return value;
    }
  };

  template <>
  struct SDBMCalculator<1>
  {
    constexpr static inline uint32_t 
    Calculate(
      const char* const stringToHash, 
      uint32_t& /*value*/
    )
    {
      return stringToHash[0];
    }
  };

  
  inline uint32_t SDBMHash(
    const std::string& key
  )
  {
    uint32_t result = 0;
    for (uint32_t i = 0; i < key.length(); i++)
    {
      uint32_t c = key[i];
      result = c + (result << 6) + (result << 16) - result;
    }
    return result;
  }
  
}


#endif