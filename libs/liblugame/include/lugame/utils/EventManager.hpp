/* 
 * This file is part of liblugame ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef LIBLUGAME_EVENT_MANAGER_HPP
#define LIBLUGAME_EVENT_MANAGER_HPP

#include <unordered_map>
#include <type_traits>
#include "Singleton.hpp"
#include "Event.hpp"



#include <iostream>

namespace lugame {

  class EventHandler;

  class EventManager {
  public:
    
    EventManager(
      EventManager const&
    ) = delete;
    
    ~EventManager(
    );
    
    void
    operator=(
      EventManager const&
    ) = delete;
    
    static EventManager&
    getInstance(
    );
    
    void
    sendEvent(
      uint32_t eventId
    ) const;
    
    void 
    sendEvent(
      uint32_t eventId,
      void* dataPtr
    );
    
    void 
    sendEventToHandler(
      uint32_t eventId, 
      EventHandler& eventHandler
    ) const;
    
    bool 
    registerEvent(
      uint32_t eventId
    );
    
    void 
    attachEvent(
      uint32_t eventId,
      EventHandler& eventHandler
    );
    
    void
    detachEvent(
      uint32_t eventId,
      EventHandler& eventHandler
    );
    
  private:
    
    EventManager(
    );
    
    std::unordered_map<uint32_t, Event*> m_events;
    
  };
}

#endif