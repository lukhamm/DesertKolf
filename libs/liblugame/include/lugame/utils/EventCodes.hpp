/* 
 * This file is part of liblugame ("this code" in the following).
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright 2019 by L. Hammerschmidt
 * https://lukhamm.gitlab.io/
 */

#ifndef LIBLUGAME_UTILS_EVENTDEFS_HPP
#define LIBLUGAME_UTILS_EVENTDEFS_HPP

#include <lugame/utils/SDBMHash.hpp>

namespace lugame {
  namespace eventid {
    constexpr uint32_t mouseMove          = lugame::SDBMCalculator<13>::CalculateValue("MouseMoveEvent");
    constexpr uint32_t mouseClickLeft     = lugame::SDBMCalculator<19>::CalculateValue("MouseClickLeftEvent");
    constexpr uint32_t mouseClickRight    = lugame::SDBMCalculator<19>::CalculateValue("MouseClickRightEvent");
    constexpr uint32_t mouseReleaseLeft   = lugame::SDBMCalculator<19>::CalculateValue("MouseReleaseLeftEvent");
    constexpr uint32_t mouseReleaseRight  = lugame::SDBMCalculator<19>::CalculateValue("MouseReleaseRightEvent");
    constexpr uint32_t buttonClickLeft    = lugame::SDBMCalculator<19>::CalculateValue("ButtonClickLeftEvent");
  }
}

#endif