# Information on Creative Work

## License Note
This work (image, sound, and similar work contained in the underlying project) is licensed under a Creative Commons Attribute-NonCommercial-ShareAlike 4.0 International License.

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.

Copyright (C) 2019, Lukas Hammerschmidt.
