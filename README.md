# DesertKolf

This is DesertKolf. This game was a fun little side project and
does not claim to be complete and/or even fun to play. For me
this game is finished as it is and I am not planning on continuing
much on the development. You are very welcome to use the code in
whichever way the license allows you to.

## Features
![Bild des Models](./resources/ScreenShots/03.png)
![Bild des Models](./resources/ScreenShots/01.png)
![Bild des Models](./resources/ScreenShots/02.png)

Create your own (random) levels
![Bild des Models](./resources/ScreenShots/04.png)

## Getting Started

### Prerequisites
The following prerequisites are necessary to compile the game
* Compiler with (at least) C++14
* CMake (version > 3.1)
* allegro5 (Debian/Ubuntu: `sudo apt install allegro`; MacOS: `brew install allegro`; [Allegro 5 Wiki](https://github.com/liballeg/allegro_wiki/wiki))

### Installing
The game includes all necessary parts of my private libraries as
they are needed in the game. So far, this game was only tested on
Linux and MacOS systems, but should in principle work on other OS
as well.

#### Linux / Mac
Install:
* Download code to your folder of choice (e.g. `/home/user/deserKolf`)
* Go to the download folder (e.g. `cd /home/user/deserKolf`)
* Commence installation by following the next steps:

``` bash
mkdir build
cd build/
cmake ..
make
```

### Using
Simply start executable "desertKolf" created in your build/ directory.

## Authors
* Lukas Hammerschmidt - Initial work - [WebPage](https://lukhamm.gitlab.io/)

No contributers yet. If you want to contribute to the project,
please don't hesitate to contact me.

## License Note
If not stated otherwise, this work is licensed under the GNU General Public
License Version 3 - see the 
[LICENSE.md](https://gitlab.com/lukhamm/DesertKolf/blob/master/LICENSE.md) 
file for details.

Copyright (C) 2019, Lukas Hammerschmidt.

## Acknowledgements
* Everyone!
